#version 400

in vec3 pos0; // Position in camera space.
in vec3 normal0; // Normal in camera space.
in vec2 texCoord0;
in vec4 lightSpacePos;
in vec3 reflectDir;

layout (location = 0) out vec4 color;
layout (location = 1) out vec3 PositionData;

layout(std140) uniform GlobalMatrices
{
  mat4 perspective;
  mat4 view;
  mat4 world;
} transform;

struct Light
{
  vec3 color;
  float ambientK;
  vec4 position; // Light position in world
  float diffuseK;
};

struct Material
{
  vec3 diffuseK;
  vec3 specularK;
  float shininess;
};

uniform Light gLight;
uniform Material material = { vec3(1.0f), vec3(0.7f), 50.0f };
uniform sampler2D gShadowMap;
uniform sampler2D gCausticMap;
uniform sampler2D gTexture;
uniform samplerCube gEnvironmentMap;
uniform bool isTextured;
uniform bool isCausticReceiver = false;
uniform bool isReflective;
uniform bool isRefractive = false;
uniform float reflectFactor = 0.1f;

const float kGamma = 2.2f;
const float bias = 0.0005f;

float calcShadow(vec4 pos)
{
  vec3 proj = pos.xyz / pos.w;
  vec2 uv;
  uv.x = 0.5f * proj.x + 0.5f;
  uv.y = 0.5f * proj.y + 0.5f;
  float z = 0.5f * proj.z + 0.5f;
  float depth = texture(gShadowMap, uv).x;
  if (depth < z - bias) {
    return 0.1f;
  } else {
    return 1.0f;
  }
}

vec4 calcCaustic(vec4 pos)
{
  vec3 proj = pos.xyz / pos.w;
  vec2 uv;
  uv.x = 0.5f * proj.x + 0.5f;
  uv.y = 0.5f * proj.y + 0.5f;
  vec4 caustic = texture(gCausticMap, uv).rgba;
  caustic = caustic * caustic * vec4(gLight.color, 1.0f);
  return vec4(caustic.rgb * 100, 1.0f);
}

vec4 phong()
{
  vec3 l;
  if (gLight.position.w == 0.0f) {
    l = normalize((transform.view * gLight.position).xyz);
  } else {
    l = normalize((transform.view * gLight.position).xyz - pos0);
  }
  vec3 norm = normalize(normal0);
  vec3 cameraDir = normalize(-pos0);
  //vec3 r = reflect(-l, norm);
  vec3 h = normalize(l + cameraDir);
  vec3 ambientColor = gLight.color * gLight.ambientK;
  float ndotl = max(dot(norm, l), 0.0f);
  vec3 diffuseColor = gLight.color * ndotl * gLight.diffuseK;
  float shadow = isCausticReceiver ? calcShadow(lightSpacePos) : 1.0f;
  vec3 specColor = gLight.color * material.specularK *
                   pow(max(dot(h,norm), 0.0f), material.shininess);
  vec3 matColor = material.diffuseK;
  if (isTextured) {
    matColor *= texture(gTexture, texCoord0).rgb;
  }
  vec3 phong = shadow * ambientColor * matColor +
               shadow * diffuseColor * matColor;
  phong += specColor * diffuseColor * shadow;
  if (isCausticReceiver) {
    phong += calcCaustic(lightSpacePos).rgb;
  }
  return vec4(phong, 1.0f);
}

void main()
{
  color = phong();
  if (isReflective) {
    vec4 envColor = texture(gEnvironmentMap, reflectDir);
    color = mix(color, envColor, reflectFactor);
    // color = mix(color, vec4(reflectDir, 1.0f), 1.0f);
  }
  vec4 gamma = vec4(1.0f / kGamma);
  gamma.w = 1.0f;
  color = pow(color, gamma);
  PositionData = vec3(lightSpacePos.xyz);
}

