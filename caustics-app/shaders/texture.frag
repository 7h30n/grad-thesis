#version 400

uniform sampler2D gTexture;

in vec3 pos0;
in vec3 normal0;
in vec2 texCoord0;

out vec4 FragColor;

vec4 colorDepth()
{
  float depth = texture2D(gTexture, texCoord0).x;
  depth = 1.0 - (1.0 - depth) * 10.0;
  return vec4(depth);
}

vec4 color()
{
  return vec4(texture2D(gTexture, texCoord0).rgb, 1.0f);
}

void main()
{
  //FragColor = colorDepth();
  FragColor = color();
}
