#version 400

in vec3 pos0;
in vec3 normal0;
in vec2 texCoord0;

uniform sampler2D texture0;
uniform int width = 512;
uniform int height = 512;

layout (location = 0) out vec4 color;

uniform float pixOffset[5] = float[](0.0, 1.0, 2.0, 3.0, 4.0);
uniform float weights[5];
uniform int pass = 1;

vec4 pass1()
{
  float dy = 1.0 / float(height);
  vec4 sum = weights[0] * texture(texture0, texCoord0);
  for (int i = 1; i < 5; ++i) {
    sum += weights[i] * texture(texture0, texCoord0 + dy * vec2(0.0, pixOffset[i]));
    sum += weights[i] * texture(texture0, texCoord0 - dy * vec2(0.0, pixOffset[i]));
  }
  return sum;
}

vec4 pass2()
{
  float dx = 1.0 / float(width);
  vec4 sum = weights[0] * texture(texture0, texCoord0);
  for (int i = 1; i < 5; ++i) {
    sum += weights[i] * texture(texture0, texCoord0 + dx * vec2(pixOffset[i], 0.0));
    sum += weights[i] * texture(texture0, texCoord0 - dx * vec2(pixOffset[i], 0.0));
  }
  return sum;
}

void main()
{
  if (pass == 1) {
    color = pass1();
  } else if (pass == 2) {
    color = pass2();
  } else {
    color = vec4(1.0f, 0.0f, 0.0f, 1.0f);
  }
}
