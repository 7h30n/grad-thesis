#version 400

in vec3 pos0;
in vec3 normal0;
in vec2 texCoord0;

out vec4 FragColor;

void main()
{
  FragColor = vec4(pos0, 1.0f);
}

