#version 330

layout (location = 0) in vec3 Position;
layout (location = 1) in vec3 Normal;

// uniform mat4 gWVP;
// uniform mat4 gWorld;

layout(std140) uniform GlobalMatrices
{
	mat4 perspectiveMatrix;
	mat4 worldToCameraMatrix;
  mat4 transformMatrix;
};

out vec4 fragmentColor;

struct Light
{
	vec3 color;
	float ambientK;
	float diffuseK;
	vec3 direction;
};

uniform Light gLight;

void main()
{
	mat4 gWorld = worldToCameraMatrix;
	mat4 gWVP = perspectiveMatrix * worldToCameraMatrix;
	vec4 ambientColor = vec4(gLight.color, 1.0f) * gLight.ambientK;
	vec3 l = worldToCameraMatrix * vec4(gLight.direction, 0.0f);
	float diffuseK = dot(normalize(gWorld * vec4(Normal, 0.0f)), l);
	vec4 diffuseColor;
	if (diffuseK > 0) {
		diffuseColor = vec4(gLight.color, 1.0f) * diffuseK * gLight.diffuseK;
	} else {
		diffuseColor = vec4(0.0f, 0.0f, 0.0f, 0.0f);
	}
	fragmentColor = ambientColor + diffuseColor;
	gl_Position = gWVP * vec4(Position, 1.0f);
}
