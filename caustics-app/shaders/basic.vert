#version 400

layout (location = 0) in vec3 Position;

layout(std140) uniform GlobalMatrices
{
  mat4 perspectiveMatrix;
  mat4 worldToCameraMatrix;
  mat4 transformMatrix;
};

void main()
{
  vec4 worldPos = transformMatrix * vec4(Position, 1.0f);
  gl_Position = worldPos;
  //gl_Position = perspectiveMatrix * worldToCameraMatrix * worldPos;
}

