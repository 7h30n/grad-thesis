#version 400

layout(points) in;
layout(points) out;
layout(max_vertices = 40) out;

in float Type0[];
in vec3 Position0[];
in vec3 Velocity0[];
in float Age0[];

out float Type1;
out vec3 Position1;
out vec3 Velocity1;
out float Age1;

uniform float gParticleLifetime;
uniform float gDeltaTime;
uniform float gTime;
uniform vec3 gRandDir;

#define PARTICLE_TYPE_LAUNCHER 0.0f
#define PARTICLE_TYPE_EFFECT 1.0f

void spawnParticles(vec3 Pos);

void main()
{
  float test = gTime;
  float Age = Age0[0] + gDeltaTime;
  
  if (Type0[0] == PARTICLE_TYPE_LAUNCHER) {
    if (Age >= 200.0f) {
      spawnParticles(Position0[0]);
      Age = 0.0f;
    }
    Type1 = PARTICLE_TYPE_LAUNCHER;
    Position1 = Position0[0];
    Velocity1 = Velocity0[0];
    Age1 = Age;
    EmitVertex();
    EndPrimitive();
  } else {
    if (Age < gParticleLifetime) {
      float dts = gDeltaTime / 1000.0f;
      vec3 dPos = dts * Velocity0[0];
      vec3 dVel = vec3(dts) * vec3(0.0, 0.0, -10.0);
      Type1 = PARTICLE_TYPE_EFFECT;
      Position1 = Position0[0] + dPos;
      Velocity1 = Velocity0[0] + dVel;
      Age1 = Age;
      EmitVertex();
      EndPrimitive();
    }
  }
}

void spawnParticles(vec3 Pos)
{
  Type1 = PARTICLE_TYPE_EFFECT;
  Position1 = Pos + gRandDir;
  Velocity1 = vec3(0.0);
  Age1 = 0.0f;
  EmitVertex();
  EndPrimitive();
}
