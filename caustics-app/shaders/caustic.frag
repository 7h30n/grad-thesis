#version 400

in vec3 pos0; // Position in camera space.
in vec3 normal0; // Normal in camera space.

layout (location = 0) out vec4 PositionData;
layout (location = 1) out vec3 NormalData;

layout(std140) uniform GlobalMatrices
{
  mat4 perspective;
  mat4 view;
  mat4 world;
} transform;

uniform bool isRefractive = false;

vec4 colorCausticMap()
{
  float v = 64;//1024;
  vec3 n = normalize(normal0);
  vec3 l = normalize(pos0);
  float flux = dot(n, l) / v;
  return vec4(flux / exp(2.0f * vec3(0.1f, 0.2f, 0.9f)), flux);
}

void main()
{
  if (isRefractive) {
    PositionData = colorCausticMap();
  } else {
    PositionData = vec4(pos0, 1.0f);
    NormalData = normal0;
  }
}

