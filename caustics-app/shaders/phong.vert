#version 330

layout (location = 0) in vec3 Position;
layout (location = 1) in vec3 Normal;
layout (location = 2) in vec2 TexCoord;

// uniform mat4 gWVP;
// uniform mat4 gWorld;

layout(std140) uniform GlobalMatrices
{
  mat4 perspective;
  mat4 view;
  mat4 world;
} transform;

uniform vec3 gCameraWorldPos;
uniform mat4 gLightPersp;
uniform bool isReflective;
uniform bool isRefractive = false;

out vec3 pos0;
out vec3 normal0;
out vec2 texCoord0;
out vec4 lightSpacePos;
out vec3 reflectDir;

void main()
{
  vec4 worldPos = transform.world * vec4(Position, 1.0f);
  gl_Position = transform.perspective * transform.view * worldPos;
  normal0 = (transform.view * transform.world * vec4(Normal, 0.0f)).xyz;
  lightSpacePos = gLightPersp * worldPos;
  //gl_Position = lightSpacePos;
  //lightSpacePos = vec4(Position, 1.0f);
  texCoord0 = TexCoord;
  pos0 = (transform.view * worldPos).xyz;
  if (isReflective) {
    vec3 worldNorm = vec3(transform.world * vec4(Normal, 0.0f));
    vec3 worldView = normalize(gCameraWorldPos - worldPos.xyz);
    reflectDir = reflect(normalize(-worldView), worldNorm);
    reflectDir.y = -reflectDir.y;
  }
}
