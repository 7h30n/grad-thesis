#version 400

layout (points) in;
layout (triangle_strip) out;
layout (max_vertices = 4) out;

layout(std140) uniform GlobalMatrices
{
  mat4 perspectiveMatrix;
  mat4 worldToCameraMatrix;
  mat4 transformMatrix;
};

out vec2 TexCoord;
//out vec4 Color;

uniform vec3 gCameraPos;

void billboard(vec3 Pos, vec3 up, vec3 right);

void main()
{
  vec3 Pos = gl_in[0].gl_Position.xyz;
  vec3 toCamera = normalize(gCameraPos - Pos);
  vec3 up = vec3(0.0, 0.0, 1.0);
  //vec3 v = up - (toCamera * up) * toCamera;
  vec3 right = normalize(cross(toCamera, up));
  up = cross(right, toCamera);
  
  billboard(Pos, up, right);
}

void billboard(vec3 Pos, vec3 up, vec3 right)
{
  Pos -= right * 0.5;
  Pos -= up * 0.5;
  gl_Position = perspectiveMatrix * worldToCameraMatrix * vec4(Pos, 1.0);
  TexCoord = vec2(0.0, 0.0);
  //Color = vec4(1.0, 0.0, 0.0, 0.0);
  EmitVertex();

  Pos += up;
  gl_Position = perspectiveMatrix * worldToCameraMatrix * vec4(Pos, 1.0);
  TexCoord = vec2(0.0, 1.0);
  //Color = vec4(0.0, 1.0, 0.0, 0.0);
  EmitVertex();

  Pos -= up;
  Pos += right;
  gl_Position = perspectiveMatrix * worldToCameraMatrix * vec4(Pos, 1.0);
  TexCoord = vec2(1.0, 0.0);
  //Color = vec4(0.0, 0.0, 1.0, 0.0);
  EmitVertex();

  Pos += up;
  gl_Position = perspectiveMatrix * worldToCameraMatrix * vec4(Pos, 1.0);
  TexCoord = vec2(1.0, 1.0);
  //Color = vec4(0.0, 0.0, 0.0, 0.0);
  EmitVertex();

  EndPrimitive();
}
