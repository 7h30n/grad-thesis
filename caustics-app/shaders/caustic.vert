#version 330

layout (location = 0) in vec3 Position;
layout (location = 1) in vec3 Normal;
layout (location = 2) in vec2 TexCoord;

layout(std140) uniform GlobalMatrices
{
  mat4 perspective;
  mat4 view;
  mat4 world;
} transform;

uniform bool isRefractive = false;
uniform float eta = 1/1.5f;
uniform sampler2D positionMap;

out vec3 pos0;
out vec3 normal0;

vec2 calcTC(vec3 pos)
{
  vec4 texPt = transform.perspective * vec4(pos, 1.0f);
  vec2 tc = vec2(0.5 * (texPt.xy / texPt.w) + vec2(0.5, 0.5));
  tc.y = 1.0 - tc.y;
  return tc;
}

// Returns distance on refracted ray
float intersectionNP(vec3 pos, vec3 dir, int iters)
{
  float eps = 0.001;
  vec2 tc = vec2(0.0, 0.0);
  // initial guess
  float x_k = 0.1;
  for (int i = 0; i < iters; ++i)
  {
    vec3 pos_p = pos + x_k * dir;
    tc = calcTC(pos_p);
    vec3 new_pos = texture(positionMap, tc).xyz;
    float f_x_k = distance(new_pos, pos_p);

    //f(x_k + eps)
    vec3 pos_q = pos + (x_k + eps) * dir;
    tc = calcTC(pos_q);
    vec3 new_pos2 = texture(positionMap, tc).xyz;
    float f_x_k_eps = distance(new_pos2, pos_q);
    float deriv = (f_x_k_eps - f_x_k) / eps;
    x_k = x_k - (f_x_k / deriv);
  }
  return x_k;
}

vec3 estimateIntersection(vec3 v, vec3 r, int iters)
{
  vec3 p1 = v + r;
  vec2 tc = calcTC(p1);
  for (int i = 0; i < iters; ++i) {
    vec4 recPos = texture(positionMap, tc);
    vec3 p2 = v + distance(v, recPos.xyz) * r;
    tc = calcTC(p2);
  }
  return texture(positionMap, tc).rgb;
}

void displaceRefractive()
{
  // vec4 inLightPos = transform.view * transform.world * vec4(Position, 1.0f);
  // vec4 inLightNormal = transform.view * transform.world * vec4(Normal, 0.0f);
  vec4 inLightPos = vec4(Position, 1.0f);
  vec4 inLightNormal = vec4(Normal, 0.0f);
  inLightNormal = normalize(inLightNormal);
  vec3 lightDir = normalize(Position);
  vec3 r = refract(lightDir, inLightNormal.xyz, eta);
  //vec3 r = reflect(lightDir, inLightNormal.xyz);
  r = normalize(r);
  vec3 intersection = estimateIntersection(inLightPos.xyz, r, 1);
  // float dist = intersectionNP(inLightPos.xyz, r, 5);
  // vec3 intersection = inLightPos.xyz + dist * r;
  gl_Position = transform.perspective * vec4(intersection, 1.0f);
  pos0 = inLightPos.xyz;
  normal0 = inLightNormal.xyz;
}

void main()
{
  if (isRefractive) {
    displaceRefractive();
  } else {
    vec4 worldPos = transform.world * vec4(Position, 1.0f);
    gl_Position = transform.perspective * transform.view * worldPos;
    normal0 = (transform.view * transform.world * vec4(Normal, 0.0f)).xyz;
    pos0 = (transform.view * worldPos).xyz;
  }
}

