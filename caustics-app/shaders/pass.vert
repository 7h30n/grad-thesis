#version 400

layout (location = 0) in vec3 Position;
layout (location = 1) in vec3 Normal;
layout (location = 2) in vec2 TexCoord;

out vec3 pos0;
out vec3 normal0;
out vec2 texCoord0;

void main()
{
  gl_Position = vec4(Position, 1.0f);
  pos0 = Position;
  normal0 = Normal;
  texCoord0 = TexCoord;
}
