#version 400

uniform sampler2D gColorMap;
uniform sampler2D gPosMap; // Positions in light space
uniform sampler2D gShadowMap;
uniform vec3 gCameraLightSpacePos; // Camera position in light space

in vec3 pos0;
in vec2 texCoord0;

out vec4 FragColor;

uniform float tau = 1.0f;
uniform vec3 albedo = {3.0f, 3.0f, 3.0f };
uniform int samples = 100;
uniform vec3 phi = { 80.0f, 50.0f, 20.0f };
const float bias = 0.0005f;
const float g = 0.0f; // -1 (backscattering) to 1 (forwardscattering)

#define PI 3.14159f

float visible(vec3 pos)
{
  vec3 proj = pos;
  vec2 uv;
  uv.x = 0.5f * proj.x + 0.5f;
  uv.y = 0.5f * proj.y + 0.5f;
  if (uv.x > 1.0f || uv.y > 1.0f) {
    return 0.0f;
  }
  float z = 0.5f * proj.z + 0.5f;
  float depth = texture(gShadowMap, uv).x;
  if (depth < z - bias) {
    return 0.0f;
  } else {
    return 1.0f;
  }
}

float phase(vec3 pos, vec3 viewDir)
{
  //return 1.0f / 4.0f / PI;
  float cosP = dot(normalize(pos), viewDir);
  float denom = 1.0f - g * g;
  float nom = 1.0f + g*g - 2*g*cosP;
  return denom / pow(nom, 1.5f) / 4.0f / PI;
}

void main()
{
  vec3 rad0 = texture(gColorMap, texCoord0).xyz;
  vec3 pos = texture(gPosMap, texCoord0).xyz;
  vec3 viewDir = gCameraLightSpacePos - pos;
  float rayLength = length(viewDir);
  viewDir = normalize(viewDir);

  float dl = rayLength / samples;

  vec3 rad = rad0 * exp(-rayLength * tau);
  for (float l = rayLength - dl; l >= 0; l -= dl) {
    pos += viewDir * dl;
    float v = visible(pos);
    float d = length(pos);
    vec3 rad_in = exp(-d * tau) * v * phi / 4.0f / PI / d / d;
    vec3 rad_i = rad_in * tau * albedo * phase(-pos, viewDir);
    rad += rad_i * exp(-l * tau) * dl;
  }

  FragColor = vec4(rad, 1.0f);
  // FragColor = vec4(texture(gPosMap, texCoord0).xyz, 1.0f);
  // FragColor = vec4(texture(gColorMap, texCoord0).xyz, 1.0f);
}
