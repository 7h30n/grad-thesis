#version 330

layout (location = 0) in vec3 Position;
layout (location = 1) in vec3 Normal;

layout (std140) uniform GlobalMatrices
{
  mat4 perspectiveMatrix;
  mat4 worldToCameraMatrix;
  mat4 transformMatrix;
};

uniform mat4 gLightPersp;

out vec3 TexCoord;
out vec4 lightSpacePos;

void main()
{
  vec4 worldPos = transformMatrix * vec4(Position, 1.0f);
  vec4 WVP = perspectiveMatrix * worldToCameraMatrix * worldPos;
  lightSpacePos = gLightPersp * worldPos;
  gl_Position = WVP;
  TexCoord = Position;
}
