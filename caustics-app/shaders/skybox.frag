#version 330

in vec3 TexCoord;
in vec4 lightSpacePos;

layout (location = 0) out vec4 FragColor;
layout (location = 1) out vec3 PositionData;

uniform samplerCube gTexture;

void main()
{
  FragColor = texture(gTexture, TexCoord);
  PositionData = vec3(lightSpacePos.xyz);
  //FragColor *= vec4(0.0f, 0.0f, 0.0f, 1.0f);
}
