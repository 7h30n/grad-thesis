/*
 * Computer Graphics course; Laboratory exercises
 *
 * Faculty of Electrical Engineering and Computing, Zagreb
 *
 * Copyright (C) 2013 Teon Banek
 *
 * This file is part of Laboratory exercises for Computer Graphics course
 * at Faculty of Electrical Engineering and Computing.
 *
 * Author(s): Teon Banek <teon.banek@fer.hr>
 * Date: 2013-10-05
 */

/*
 * The contents of the file are the definitions of methods for LineRenderer.
 */

#include "line_renderer.hpp"

#include <cstdio>

#include "glw/glw.hpp"

LineRenderer::LineRenderer(const std::vector<glm::vec3> &points)
  : points_(points)
{
}

LineRenderer::~LineRenderer()
{
  glDeleteBuffers(1, &vertex_buffer_object_);
}

/**
 * Creates 1 buffer object with point positions.
 */
bool LineRenderer::Init()
{
  // TODO: Use vertex array object.
  glGenBuffers(1, &vertex_buffer_object_);
  glBindBuffer(GL_ARRAY_BUFFER, vertex_buffer_object_);
  glBufferData(GL_ARRAY_BUFFER, points_.size() * sizeof(points_[0]),
                &points_[0], GL_STATIC_DRAW);
  glBindBuffer(GL_ARRAY_BUFFER, 0);
  return true;
}

/**
 * Performs array drawing of lines from the stored buffers.
 */
void LineRenderer::Render()
{
  glw::VertexAttrib pos_attr(0);
  pos_attr.Pointer(3, GL_FLOAT, GL_FALSE, 0, 0, vertex_buffer_object_);
  glDrawArrays(GL_LINE_STRIP, 0, points_.size());
  pos_attr.Disable();
}

/**
 * Performs array drawing of lines from the stored buffers, starting
 * from index i and drawing n lines.
 */
void LineRenderer::RenderSegment(int i, int n)
{
  glw::VertexAttrib pos_attr(0);
  pos_attr.Pointer(3, GL_FLOAT, GL_FALSE, 0, 0, vertex_buffer_object_);
  glDrawArrays(GL_LINE_STRIP, i * 2, n * 2);
  pos_attr.Disable();
}
