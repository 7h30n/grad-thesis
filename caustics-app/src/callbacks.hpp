/*
 * Computer Graphics course; Laboratory exercises
 *
 * Faculty of Electrical Engineering and Computing, Zagreb
 *
 * Copyright (C) 2013 Teon Banek
 *
 * This file is part of Laboratory exercises for Computer Graphics course
 * at Faculty of Electrical Engineering and Computing.
 *
 * Author(s): Teon Banek <teon.banek@fer.hr>
 * Date: 2013-10-23
 */

/*
 * This file contains the declaration of ICallbacks interface.
 */
#ifndef CALLBACKS_H
#define CALLBACKS_H

/**
 * This is an interface with callback functions mapped 1 to 1 with
 * OpenGL callbacks. Implementors may implement any of the callbacks they
 * need and they will be registered.
 */
class ICallbacks
{
    public:
        virtual ~ICallbacks() {}

        /**
         * Callback for handling special keyboard events.
         */
        virtual void specialKeyboardCB(int key, int x, int y)=0;
        /**
         * Callback for handling regular keyboard events.
         */
        virtual void keyboardCB(unsigned char key, int x, int y)=0;
        /**
         * Callback for passive mouse events (moving the mouse).
         */
        virtual void passiveMouseCB(int x, int y)=0;
        /**
         * Callback for click-dragging mouse events.
         */
        virtual void draggingMouseCB(int x, int y)=0;
        /**
         * Callback for mouse click events.
         */
        virtual void mouseCB(int button, int state, int x, int y)=0;
        /**
         * Callback used for rendering.
         */
        virtual void renderSceneCB()=0;
        /**
         * Callback function for idle work,
         * when not rendering or handling input.
         */
        virtual void idleCB()=0;
        /**
         * Callback for handling resize events.
         */
        virtual void reshapeCB(int w, int h)=0;
};

#endif
