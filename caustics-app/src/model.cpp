#include "model.hpp"

Model Model::Plane()
{
  Model plane;
  plane.vertices_.push_back(glm::vec3(-1.0f, -1.0f, 0.0f));
  plane.vertices_.push_back(glm::vec3(-1.0f, 1.0f, 0.0f));
  plane.vertices_.push_back(glm::vec3(1.0f, -1.0f, 0.0f));
  plane.vertices_.push_back(glm::vec3(1.0f, 1.0f, 0.0f));

  plane.uvs_.push_back(glm::vec2(0.0f, 0.0f));
  plane.uvs_.push_back(glm::vec2(0.0f, 1.0f));
  plane.uvs_.push_back(glm::vec2(1.0f, 0.0f));
  plane.uvs_.push_back(glm::vec2(1.0f, 1.0f));

  Face face1;
  face1.index[0] = 0;
  face1.uv_index[0] = 0;
  face1.vn_index[0] = 0;
  face1.index[1] = 2;
  face1.uv_index[1] = 2;
  face1.vn_index[1] = 0;
  face1.index[2] = 1;
  face1.uv_index[2] = 1;
  face1.vn_index[2] = 0;
  plane.calculatePlane(face1);
  plane.faces_.push_back(face1);

  Face face2;
  face2.index[0] = 1;
  face2.uv_index[0] = 1;
  face2.vn_index[0] = 0;
  face2.index[1] = 2;
  face2.uv_index[1] = 2;
  face2.vn_index[1] = 0;
  face2.index[2] = 3;
  face2.uv_index[2] = 3;
  face2.vn_index[2] = 0;
  plane.calculatePlane(face2);
  plane.faces_.push_back(face2);

  return plane;
}

Model Model::Cube()
{
  Model cube;
  cube.readObjFile("models/kocka.obj");
  return cube;
}

bool Model::readObjFile(const char *name)
{
    FILE *file;
    if ((file = fopen(name, "r")) == NULL) {
        fprintf(stderr, "Unable to open: %s\n", name);
        return false;
    }
    while (feof(file) == 0) {
        int c = fgetc(file);
        switch (c) {
        case 'v':
            if (!readVertex(file)) {
                return false;
            }
            break;
        case 'f':
        case 'p':
            if (!readFace(file)) {
                return false;
            }
            break;
        case '#': // ignore comment
          if (!ignoreLine(file)) {
            return false;
          }
          break;
        case '\n':
        case '\r':
          break;
        default: // ignore not implemented
            if (!ignoreLine(file)) {
                return false;
            }
            break;
        }
    }
    fclose(file);
    normalize();
    return true;
}

bool Model::insideConvexBody(glm::vec3 point)
{
    for (unsigned i = 0; i < faces_.size(); i++) {
        float check = glm::dot(glm::vec4(point, 1.0), faces_[i].plane);
        if (check > 0) {
            return false;
        }
    }
    return true;
}

bool Model::readVertex(FILE *file)
{
    float v1, v2, v3;
    int c = fgetc(file);
    if (c == ' ') {
      if (fscanf(file, "%f %f %f\n", &v1, &v2, &v3) != 3) {
          fprintf(stderr, "Unable to read vertex info!");
          return false;
      }
      glm::vec3 v(v1, v2, v3);
      vertices_.push_back(v);
      return true;
    } else if (c == 't') {
      if (fscanf(file, "%f %f\n", &v1, &v2) != 2) {
          fprintf(stderr, "Unable to read texture vertex info!");
          return false;
      }
      glm::vec2 v(v1, v2);
      uvs_.push_back(v);
      return true;
    } else if (c == 'n') {
      // Read vertex normal (vn)
      if (fscanf(file, "%f %f %f\n", &v1, &v2, &v3) != 3) {
          fprintf(stderr, "Unable to read vertex normal info.");
          return false;
      }
      glm::vec3 v(v1, v2, v3);
      normals_.push_back(v);
      return true;
    } else {
      return ignoreLine(file);
    }
}

bool Model::readFace(FILE *file)
{
    Face face;
    fgetc(file);
    if (readFaceIndex(file, 0, face) &&
        readFaceIndex(file, 1, face) &&
        readFaceIndex(file, 2, face)) {
      // If successfully read all 3 face indices.
      calculatePlane(face);
      faces_.push_back(face);
    } else {
      fprintf(stderr, "Unable to read face indices!");
      return false;
    }
    return true;
}

bool Model::readFaceIndex(FILE *file, unsigned i, Face &f)
{
  long ind = 0;
  long uv = 0;
  long vn = 0;
  bool tex = false;
  bool normal = false;
  int sign = 1;
  while (!feof(file)) {
    int c = fgetc(file);
    if (ind && (c == ' ' || c == '\n' || c == '\r')) {
      if (normal) {
        vn *= sign;
      } else if (tex) {
        uv *= sign;
      } else {
        ind *= sign;
      }
      // indices are +1 (unless default 0) or negative
      f.index[i] = ind > 0 ?
                   ind - 1 : static_cast<long>(vertices_.size()) + ind;
      f.uv_index[i] = uv > 0 ?
                      uv - 1 : static_cast<long>(uvs_.size()) + uv;
      f.vn_index[i] = vn > 0 ?
                      vn - 1 : static_cast<long>(normals_.size()) + vn;
      return true;
    } else if (c == '/') {
      if (!tex) {
        tex = true;
        ind *= sign;
      } else {
        normal = true;
        uv *= sign;
      }
      sign = 1;
    } else if (c == '-') {
      sign = -1;
    } else if (c >= '0' && c <= '9') {
      if (normal) {
        vn = vn * 10 + c - '0';
      } else if (tex) {
        uv = uv * 10 + c - '0';
      } else {
        ind = ind * 10 + c - '0';
      }
    } else {
      fprintf(stderr, "Unable to read face data! (%c)\n", c);
      return false;
    }
  }
  return false;
}

bool Model::ignoreLine(FILE *file)
{
    int ch = 0;
    while ((ch = fgetc(file)) != EOF && ch != '\n' && ch != '\r') {
        // printf("%c", ch);
    }
    // printf("\n");
    return true;
}

void Model::calculatePlane(Face &face)
{
    glm::vec3 v1 = vertices_[face.index[0]];
    glm::vec3 v2 = vertices_[face.index[1]];
    glm::vec3 v3 = vertices_[face.index[2]];
    float A = (v2.y - v1.y) * (v3.z - v1.z) - (v2.z - v1.z) * (v3.y - v1.y);
    float B = -(v2.x - v1.x) * (v3.z - v1.z) + (v2.z - v1.z) * (v3.x - v1.x);
    float C = (v2.x - v1.x) * (v3.y - v1.y) - (v2.y - v1.y) * (v3.x - v1.x);
    glm::vec3 n = glm::normalize(glm::vec3(A, B, C));
    float D = -v1.x * n.x - v1.y * n.y - v1.z * n.z;
    // printf("Plane [ %.4f %.4f %.4f %.4f ]\n", n.x, n.y, n.z, D);
    face.plane = glm::vec4(n.x, n.y, n.z, D);
}

std::vector<unsigned> Model::getVisibleIndices(glm::vec3 O)
{
    std::vector<unsigned> visible;
    for (size_t i = 0; i < faces_.size(); ++i) {
        if (isVisibleFace(faces_[i], O)) {
            visible.push_back((faces_[i]).index[0]);
            visible.push_back((faces_[i]).index[1]);
            visible.push_back((faces_[i]).index[2]);
        }
    }
    return visible;
}

bool Model::isVisibleFace(Face f, glm::vec3 O)
{
    glm::vec3 v1 = vertices_[f.index[0]];
    glm::vec3 v2 = vertices_[f.index[1]];
    glm::vec3 v3 = vertices_[f.index[2]];
    float xMin = v1.x;
    float xMax = v1.x;
    float yMin = v1.y;
    float yMax = v1.y;
    float zMin = v1.z;
    float zMax = v1.z;
    if (v2.x > xMax) xMax = v2.x;
    if (v3.x > xMax) xMax = v3.x;
    if (v2.x < xMin) xMin = v2.x;
    if (v3.x < xMin) xMin = v3.x;
    if (v2.y > yMax) yMax = v2.y;
    if (v3.y > yMax) yMax = v3.y;
    if (v2.y < yMin) yMin = v2.y;
    if (v3.y < yMin) yMin = v3.y;
    if (v2.z > zMax) zMax = v2.z;
    if (v3.z > zMax) zMax = v3.z;
    if (v2.z < zMin) zMin = v2.z;
    if (v3.z < zMin) zMin = v3.z;
    float xc = (xMax + xMin) / 2;
    float yc = (yMax + yMin) / 2;
    float zc = (zMax + zMin) / 2;
    glm::vec4 NP(O.x - xc, O.y - yc, O.z - zc, 0);
    glm::vec4 N = glm::vec4(f.plane.x, f.plane.y, f.plane.z, 0);
    if (glm::dot(NP, N) >= 0) {
        return true;
    } else {
        return false;
    }
}

void Model::normalize()
{
    float minX = vertices_[0].x;
    float maxX = vertices_[0].x;
    float minY = vertices_[0].y;
    float maxY = vertices_[0].y;
    float minZ = vertices_[0].z;
    float maxZ = vertices_[0].z;
    for (size_t i = 1; i < vertices_.size(); ++i) {
        glm::vec3 v = vertices_[i];
        if (v.x > maxX) maxX = v.x;
        if (v.x < minX) minX = v.x;
        if (v.y > maxY) maxY = v.y;
        if (v.y < minY) minY = v.y;
        if (v.z > maxZ) maxZ = v.z;
        if (v.z < minZ) minZ = v.z;
    }
    float scale = maxX - minX;
    if (scale < maxY - minY) {
        scale = maxY - minY;
    }
    if (scale < maxZ - minZ) {
        scale = maxZ - minZ;
    }
    scale = 2.0f / scale;
  // Move the center of the object to origin (0, 0, 0).
  // Scale the object to fit in [-1, 1] range.
    for (size_t i = 0; i < vertices_.size(); ++i) {
        vertices_[i].x -= (maxX + minX) / 2.0f;
        vertices_[i].y -= (maxY + minY) / 2.0f;
        vertices_[i].z -= (maxZ + minZ) / 2.0f;
        vertices_[i].x *= scale;
        vertices_[i].y *= scale;
        vertices_[i].z *= scale;
    }
    for (size_t i = 0; i < faces_.size(); ++i) {
        calculatePlane(faces_[i]);
    }
}
