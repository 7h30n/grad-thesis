#ifndef CAUSTIC_SHADING_H
#define CAUSTIC_SHADING_H

#include "technique.hpp"

class CausticShading: public Technique
{
  public:
    virtual bool init();
    void setIsRefractive(bool is_refractive) const;
    void setPositionMap(unsigned tex_unit) const;

  private:
    GLint isRefractiveLocation;
    GLint positionMapLocation;
};

#endif
