#ifndef PS_UPDATE_TECHNIQUE_H_
#define PS_UPDATE_TECHNIQUE_H_

#include "technique.hpp"

#include <glm/glm.hpp>

class PSUpdateTechnique : public Technique
{
  public:
    PSUpdateTechnique();

    virtual bool init();

    void setParticleLifetime(float ttl);
    void setDeltaTime(float dt);
    void setTime(float time);
    void setRandDir(const glm::vec3 &dir);

  private:
    GLint particleLifetimeLocation;
    GLint deltaTimeLocation;
    GLint timeLocation;
    GLint randDirLocation;
};

#endif
