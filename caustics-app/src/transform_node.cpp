#include "scene_graph.hpp"

TransformNode::TransformNode(glm::mat4 transform)
    : transform_(transform), next_node_(0)
{
}

TransformNode::~TransformNode()
{
  if (next_node_) {
    delete next_node_;
  }
}

void TransformNode::AcceptVisitor(SceneVisitor &visitor)
{
  visitor.Visit(*this);
  if (next_node_) {
    next_node_->AcceptVisitor(visitor);
  }
  visitor.FinishedVisit(*this);
}
