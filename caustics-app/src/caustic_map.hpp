#ifndef CAUSTIC_MAP_H
#define CAUSTIC_MAP_H

#include <GL/glew.h>

#include "render_frame.hpp"

class CausticMap
{
  public:
    CausticMap(unsigned width, unsigned height);
    ~CausticMap();

    bool Init();
    void WriteCausticMap();
    void BindForWritingReceiver();
    void BindForWritingRefractive();
    void RenderReceiverPositions();
    void RenderRefractivePositions();
    void RenderRefractiveNormals();
    void RenderReceiverDepth();
    void RenderCausticMap();
    void BindPositionMap(GLenum tex_unit);
    void BindDepthMap(GLenum tex_unit);
    void BindCausticMap(GLenum tex_unit);
    unsigned width() const { return width_; }
    unsigned height() const { return height_; }

  private:
    unsigned width_;
    unsigned height_;
    RenderFrame recv_frame_;
    RenderFrame refr_frame_;
    RenderFrame caustic_frame_;
    GLuint vao_;
    GLuint vbo_[3];
};

#endif
