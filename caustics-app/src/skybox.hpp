#ifndef SKYBOX_H_
#define SKYBOX_H_

#include <glm/glm.hpp>
#include <string>

#include "mesh_renderer.hpp"
#include "model.hpp"

class CubemapTexture;
class SkyboxTechnique;

class Skybox {
  public:
    Skybox();
    ~Skybox();

    bool Init(const std::string &dir,
              const std::string &pos_x, const std::string &neg_x,
              const std::string &pos_y, const std::string &neg_y,
              const std::string &pos_z, const std::string &neg_z,
              unsigned global_binding_index);

    void Render(const glm::mat4 &light_persp);
    void Bind(GLenum tex_unit) const;

  private:
    SkyboxTechnique *skybox_tech_;
    CubemapTexture *cubemap_tex_;
    Model cube_;
    MeshRenderer cube_mesh_;
};

#endif
