#include "caustic_map.hpp"

#include <cassert>
#include <cstdio>
#include <cstring>

#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>

#include "glw/glw.hpp"
#include "mesh_renderer.hpp"
#include "model.hpp"
#include "position_shading.hpp"
#include "texture_shading.hpp"

struct TextureData
{
  float r;
  float g;
  float b;
  float a;
};

enum VertexBuffer
{
  VERTEX_DATA,
  POS_DATA,
  NORMAL_DATA
};

static void initBuffer(GLuint buf, GLsizeiptr size, const GLvoid *data,
                       GLenum usage)
{
  glBindBuffer(GL_ARRAY_BUFFER, buf);
  glBufferData(GL_ARRAY_BUFFER, size, data, usage);
  glBindBuffer(GL_ARRAY_BUFFER, 0);
}

CausticMap::CausticMap(unsigned width, unsigned height)
  : width_(width), height_(height),
    recv_frame_(width, height), refr_frame_(width, height),
    caustic_frame_(width, height), vao_(0)
{
  for (int i = 0; i < 3; ++i) {
    vbo_[i] = 0;
  }
}

CausticMap::~CausticMap()
{
  glDeleteVertexArrays(1, &vao_);
  glDeleteBuffers(3, vbo_);
}

bool CausticMap::Init()
{
  glGenVertexArrays(1, &vao_);
  glGenBuffers(3, vbo_);
  if (vao_ == 0) {
    return false;
  }
  initBuffer(vbo_[VERTEX_DATA], 2 * sizeof(TextureData) * width_ * height_, 0,
             GL_DYNAMIC_DRAW);
  for (int i = 1; i < 3; ++i) {
    initBuffer(vbo_[i], sizeof(TextureData) * width_ * height_, 0,
               GL_STREAM_READ);
  }
  if (!recv_frame_.Init()) {
    return false;
  }
  if (!refr_frame_.Init()) {
    return false;
  }
  if (!caustic_frame_.Init()) {
    return false;
  }
  return true;
}

void CausticMap::BindForWritingRefractive()
{
  refr_frame_.BindForWriting();
  glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
}

void CausticMap::BindForWritingReceiver()
{
  recv_frame_.BindForWriting();
  glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
}

void CausticMap::RenderReceiverPositions()
{
  // Positions are instead of color.
  recv_frame_.RenderColor();
}

void CausticMap::RenderRefractivePositions()
{
  refr_frame_.RenderColor();
}

void CausticMap::RenderRefractiveNormals()
{
  refr_frame_.RenderPosition();
}

void CausticMap::RenderReceiverDepth()
{
  recv_frame_.RenderDepth();
}

void CausticMap::RenderCausticMap()
{
  glEnable(GL_BLEND);
  glBlendFunc(GL_ONE, GL_ONE);
  caustic_frame_.RenderColor();
  glDisable(GL_BLEND);
}

void CausticMap::BindPositionMap(GLenum tex_unit)
{
  recv_frame_.BindColorMap(tex_unit);
}

void CausticMap::BindDepthMap(GLenum tex_unit)
{
  refr_frame_.BindDepthMap(tex_unit);
}

void CausticMap::BindCausticMap(GLenum tex_unit)
{
  caustic_frame_.BindColorMap(tex_unit);
}

static unsigned fillWithData(const TextureData *pos_data,
                             const TextureData *normals,
                             unsigned len,
                             TextureData *buf)
{
  assert(pos_data);
  assert(normals);
  assert(buf);
  unsigned count = 0;
  for (unsigned i = 0; i < len; ++i) {
    if (normals[i].r != 0.0f || normals[i].g != 0.0f || normals[i].b != 0.0f) {
      count++;
      memcpy(buf, &pos_data[i], sizeof(pos_data[i]));
      buf++;
      memcpy(buf, &normals[i], sizeof(normals[i]));
      buf++;
    }
  }
  return count;
}

static void readFrame(RenderFrame &frame, unsigned width, unsigned height,
                      GLuint pos_buf, GLuint normal_buf)
{
  frame.BindForWriting();
  glReadBuffer(GL_COLOR_ATTACHMENT0);
  glBindBuffer(GL_PIXEL_PACK_BUFFER, pos_buf);
  glReadPixels(0, 0, width, height, GL_RGBA, GL_FLOAT, 0);
  glReadBuffer(GL_COLOR_ATTACHMENT1);
  glBindBuffer(GL_PIXEL_PACK_BUFFER, normal_buf);
  glReadPixels(0, 0, width, height, GL_RGBA, GL_FLOAT, 0);
  glBindBuffer(GL_PIXEL_PACK_BUFFER, 0);
  glFlush();
}

static unsigned transfer(GLuint pos_buf, GLuint normal_buf, unsigned len,
                         GLuint vertex_buf)
{
  glBindBuffer(GL_COPY_READ_BUFFER, pos_buf);
  glBindBuffer(GL_COPY_WRITE_BUFFER, normal_buf);
  TextureData *pos_data = glw::MapBufferRange<TextureData>(
      GL_COPY_READ_BUFFER, 0, sizeof(TextureData) * len, GL_MAP_READ_BIT);
  TextureData *normals = glw::MapBufferRange<TextureData>(
        GL_COPY_WRITE_BUFFER, 0, sizeof(TextureData) * len, GL_MAP_READ_BIT);
  glBindBuffer(GL_ARRAY_BUFFER, vertex_buf);
  TextureData *buf = glw::MapBufferRange<TextureData>(
        GL_ARRAY_BUFFER, 0, 2 * sizeof(TextureData) * len,
        GL_MAP_WRITE_BIT | GL_MAP_INVALIDATE_BUFFER_BIT |
        GL_MAP_FLUSH_EXPLICIT_BIT);
  unsigned count = fillWithData(pos_data, normals, len, buf);
  glFlushMappedBufferRange(GL_ARRAY_BUFFER, 0, 2 * sizeof(TextureData) * count);
  glUnmapBuffer(GL_ARRAY_BUFFER);
  glUnmapBuffer(GL_COPY_WRITE_BUFFER);
  glUnmapBuffer(GL_COPY_READ_BUFFER);
  glBindBuffer(GL_ARRAY_BUFFER, 0);
  glBindBuffer(GL_COPY_WRITE_BUFFER, 0);
  glBindBuffer(GL_COPY_READ_BUFFER, 0);
  return count;
}

static void drawCausticMap(RenderFrame &frame, GLuint vertex_array,
                           GLuint vertex_buf, unsigned count)
{
  glw::BoundVertexArray bound_vao(vertex_array);
  frame.BindForWriting();
  glClear(GL_COLOR_BUFFER_BIT);
  glBindBuffer(GL_ARRAY_BUFFER, vertex_buf);
  glw::VertexAttrib pos_attr(0);
  pos_attr.Pointer(3, GL_FLOAT, GL_FALSE, 2 * sizeof(TextureData), 0);
  glw::VertexAttrib normal_attr(1);
  const GLvoid* normal_offset =
    reinterpret_cast<const GLvoid*>(sizeof(TextureData));
  normal_attr.Pointer(3, GL_FLOAT, GL_FALSE, 2 * sizeof(TextureData),
                      normal_offset);
  //glVertexAttrib2f(2, 0.0f, 0.0f);
  glPointSize(2);
  glEnable(GL_BLEND);
  glBlendFunc(GL_ONE, GL_ONE);
  glDisable(GL_DEPTH_TEST);
  glDrawArrays(GL_POINTS, 0, count);
  glBindBuffer(GL_ARRAY_BUFFER, 0);
  glBindFramebuffer(GL_FRAMEBUFFER, 0);
  glEnable(GL_DEPTH_TEST);
  glDisable(GL_BLEND);
  glPointSize(1);
}

void CausticMap::WriteCausticMap()
{
  static int frame_delay = 2;
  static int frame = 0;
  // Wait couple of frames while the pixels are read.
  if (frame == 0) {
    readFrame(refr_frame_, width_, height_,
              vbo_[POS_DATA], vbo_[NORMAL_DATA]);
  }
  if (frame == frame_delay) {
    unsigned count = transfer(vbo_[POS_DATA], vbo_[NORMAL_DATA],
                              width_ * height_, vbo_[VERTEX_DATA]);
    drawCausticMap(caustic_frame_, vao_, vbo_[VERTEX_DATA], count);
  }
  frame = (frame + 1) % (frame_delay + 1);
}
