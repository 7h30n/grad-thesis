#include <stdio.h>
#include <math.h>

#include <glm/gtc/type_ptr.hpp>
#include <glm/gtc/matrix_transform.hpp>

#include "simple_technique.hpp"

bool SimpleTechnique::init()
{
    if (!Technique::init()) {
        return false;
    }

    if (!addShaderFromFile(GL_VERTEX_SHADER, "shaders/gouraud.vert")) {
        return false;
    }

    if (!addShaderFromFile(GL_FRAGMENT_SHADER, "shaders/gouraud.frag")) {
        return false;
    }

    if (!finalize()) {
        return false;
    }

    lightLocation.color = getUniformLocation("gLight.color");
    if (lightLocation.color == -1) {
        fprintf(stderr, "Missing gLight.color in shaders!");
        return false;
    }
    lightLocation.ambientK = getUniformLocation("gLight.ambientK");
    if (lightLocation.ambientK == -1) {
        fprintf(stderr, "Missing gLight.ambientK in shaders!");
        return false;
    }
    lightLocation.position = getUniformLocation("gLight.position");
    if (lightLocation.position == -1) {
        fprintf(stderr, "Missing gLight.position in shaders!");
        return false;
    }
    lightLocation.diffuseK = getUniformLocation("gLight.diffuseK");
    if (lightLocation.diffuseK == -1) {
        fprintf(stderr, "Missing gLight.diffuseK in shaders!");
        return false;
    }
    return true;
}

void SimpleTechnique::setLight(const Light &light)
{
    glUniform3f(lightLocation.color, light.color.x, light.color.y,
                light.color.z);
    glUniform1f(lightLocation.ambientK, light.ambientK);
    glm::vec4 pos = glm::normalize(light.position);
    glUniform4f(lightLocation.position, pos.x, pos.y, pos.z, pos.w);
    glUniform1f(lightLocation.diffuseK, light.diffuseK);
}
