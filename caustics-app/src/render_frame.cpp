#include "render_frame.hpp"

#include <cstdio>

#include "mesh_renderer.hpp"
#include "model.hpp"
#include "texture_shading.hpp"

RenderFrame::RenderFrame(unsigned width, unsigned height)
  : width_(width), height_(height), fbo_(0), color_map_(0), depth_map_(0),
    pos_map_(0)
{
}

RenderFrame::~RenderFrame()
{
  if (fbo_) {
    glDeleteFramebuffers(1, &fbo_);
  }
  if (color_map_) {
    glDeleteTextures(1, &color_map_);
  }
  if (pos_map_) {
    glDeleteTextures(1, &pos_map_);
  }
  if (depth_map_) {
    glDeleteTextures(1, &depth_map_);
  }
}

bool RenderFrame::Init()
{
  glGenFramebuffers(1, &fbo_);
  glBindFramebuffer(GL_FRAMEBUFFER, fbo_);

  glGenTextures(1, &color_map_);
  glBindTexture(GL_TEXTURE_2D, color_map_);
  glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB32F, width_, height_, 0, GL_RGBA,
                GL_FLOAT, 0);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
  glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0,
                          GL_TEXTURE_2D, color_map_, 0);

  glGenTextures(1, &depth_map_);
  glBindTexture(GL_TEXTURE_2D, depth_map_);
  glTexImage2D(GL_TEXTURE_2D, 0, GL_DEPTH_COMPONENT, width_, height_, 0,
                GL_DEPTH_COMPONENT, GL_FLOAT, 0);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
  glFramebufferTexture2D(GL_DRAW_FRAMEBUFFER, GL_DEPTH_ATTACHMENT,
                          GL_TEXTURE_2D, depth_map_, 0);

  glGenTextures(1, &pos_map_);
  glBindTexture(GL_TEXTURE_2D, pos_map_);
  glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB32F, width_, height_, 0, GL_RGB,
                GL_FLOAT, 0);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
  glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT1,
                          GL_TEXTURE_2D, pos_map_, 0);

  GLenum draw_bufs[] = { GL_COLOR_ATTACHMENT0, GL_COLOR_ATTACHMENT1 };
  glDrawBuffers(2, draw_bufs);

  if (glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE) {
    fprintf(stderr, "Framebuffer is not complete!\n");
    return false;
  }

  glBindFramebuffer(GL_FRAMEBUFFER, 0);

  return true;
}

void RenderFrame::BindForWriting()
{
  glBindFramebuffer(GL_FRAMEBUFFER, fbo_);
  glViewport(0, 0, width_, height_);
}

void RenderFrame::BindForReading(GLenum tex_unit)
{
  BindColorMap(tex_unit);
  glActiveTexture(tex_unit + 1);
  glBindTexture(GL_TEXTURE_2D, pos_map_);
}

void RenderFrame::BindColorMap(GLenum tex_unit)
{
  glActiveTexture(tex_unit);
  glBindTexture(GL_TEXTURE_2D, color_map_);
}

void RenderFrame::BindDepthMap(GLenum tex_unit)
{
  glActiveTexture(tex_unit);
  glBindTexture(GL_TEXTURE_2D, depth_map_);
}

static bool renderFromTexture(GLuint texture)
{
  Model plane(Model::Plane());
  MeshRenderer plane_mesh(plane);
  if (!plane_mesh.Init()) {
    return false;
  }
  TextureShading texture_shading;
  if (!texture_shading.init()) {
    return false;
  }
  glActiveTexture(GL_TEXTURE0);
  glBindTexture(GL_TEXTURE_2D, texture);
  texture_shading.enable();
  plane_mesh.Render();
  return true;
}

void RenderFrame::RenderColor() const
{
  if (!renderFromTexture(color_map_)) {
    fprintf(stderr, "RenderFrame::RenderColor() unable to render\n");
  }
}

void RenderFrame::RenderPosition() const
{
  if (!renderFromTexture(pos_map_)) {
    fprintf(stderr, "RenderFrame::RenderPosition() unable to render\n");
  }
}

void RenderFrame::RenderDepth() const
{
  if (!renderFromTexture(depth_map_)) {
    fprintf(stderr, "RenderFrame::RenderDepth() unable to render\n");
  }
}
