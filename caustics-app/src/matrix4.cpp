#include "matrix4.hpp"

#include <cassert>

const double Matrix4::PI = 3.14159265358979;
const double Matrix4::DEG2RAD = PI / 180.0;
const double Matrix4::RAD2DEG = 180.0 / PI;

glm::mat4 Matrix4::Translate(glm::vec3 pos)
{
  glm::mat4 mat(1, 0, 0, 0,
                0, 1, 0, 0,
                0, 0, 1, 0,
                pos.x, pos.y, pos.z, 1);
  return mat;
}

glm::mat4 Matrix4::Scale(float sx, float sy, float sz)
{
  glm::mat4 mat(sx, 0, 0, 0,
                0, sy, 0, 0,
                0, 0, sz, 0,
                0, 0, 0, 1.0f);
  return mat;
}

glm::mat4 Matrix4::Perspective(float fov, float ar, float z_near, float z_far)
{
  assert(fov > 0.0f);
  assert(z_near > 0.0f);
  assert(z_far > z_near);
  float tan_half_fov = tanf((fov / 2.0f) * DEG2RAD);
  glm::mat4 p(1 / (ar * tan_half_fov), 0, 0, 0,
              0, 1 / tan_half_fov, 0, 0,
              0, 0, (z_near + z_far) / (z_near - z_far), -1,
              0, 0, 2 * z_far * z_near / (z_near - z_far), 0);
  return p;
}

glm::mat4 Matrix4::LookAt(glm::vec3 eye, glm::vec3 center, glm::vec3 up)
{
  glm::vec3 n = glm::normalize(center - eye);
  glm::vec3 v = up - (glm::dot(n, up)) * n;
  glm::vec3 u = glm::normalize(glm::cross(n, v));
  v = glm::cross(u, n);
  glm::mat4 t(u.x, v.x, -n.x, 0.0f,
        u.y, v.y, -n.y, 0.0f,
        u.z, v.z, -n.z, 0.0f,
        -glm::dot(u, eye), -glm::dot(v, eye), glm::dot(n, eye), 1.0f);
  return t;
}
