/////////////////////////////////////////////////////
// Interactive Computer Graphics
//
// Laboratory exercises
// III cycle
// Exercise 6: Drawing Bezier curve.
//
// Author: Teon Banek
// Date: 05.2013.
// Faculty of Electrical Engineering and Computing
/////////////////////////////////////////////////////
#include <fstream>
#include <iostream>

#include "bezier_curve.hpp"

static int factorial(int n)
{
    int val = 1;
    for (int i = 2; i <= n; ++i)
    {
        val *= i;
    }
    return val;
}

const std::vector<glm::vec3> &BezierCurve::getPoints()
{
    return points;
}

const std::vector<glm::vec3> &BezierCurve::getControlPoints()
{
    return controlPoints;
}

bool BezierCurve::readFile(const char *filename)
{
    std::ifstream file(filename);
    while (!file.eof()) {
        float x, y, z;
        file >> x;
        if (file.eof()) {
            break;
        }
        if (file.fail()) {
            std::cerr << "Bad file: " << filename << std::endl;
            return false;
        }
        file >> y;
        if (file.fail() || file.eof()) {
            std::cerr << "Bad file: " << filename << std::endl;
            return false;
        }
        file >> z;
        if (file.fail() || file.eof()) {
            std::cerr << "Bad file: " << filename << std::endl;
            return false;
        }
        controlPoints.push_back(glm::vec3(x, y, z));
    }
    file.close();
    calculate();
    return true;
}

void BezierCurve::calculate()
{
    for (float t = 0; t <= 1; t += 0.01) {
        glm::vec3 p = calculatePoint(t);
        points.push_back(p);
    }
}

glm::vec3 BezierCurve::calculatePoint(float t)
{
    int n = controlPoints.size() - 1;
    glm::vec3 p(0.0f, 0.0f, 0.0f);
    for (int i = 0; i <= n; ++i) {
        p += controlPoints[i] * bernsteinPolynom(i, n, t);
    }
    return glm::vec3(p);
}

float BezierCurve::bernsteinPolynom(int i, int n, float t)
{
    float b = (float)factorial(n) / (factorial(i) * factorial(n - i));
    b *= pow(t, (float)i) * pow(1 - t, (float)(n - i));
    // std::cout << "i = " << i << " n = " << n << " t = " << t;
    // std::cout << " b = " << b << std::endl;
    return b;
}
