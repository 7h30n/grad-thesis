#include "caustic_shading.hpp"

#include <stdio.h>

bool CausticShading::init()
{
    if (!Technique::init()) {
        return false;
    }

    if (!addShaderFromFile(GL_VERTEX_SHADER, "shaders/caustic.vert")) {
        return false;
    }

    if (!addShaderFromFile(GL_FRAGMENT_SHADER, "shaders/caustic.frag")) {
        return false;
    }

    if (!finalize()) {
        return false;
    }

    isRefractiveLocation = getUniformLocation("isRefractive");
    if (isRefractiveLocation == -1) {
        return false;
    }
    positionMapLocation = getUniformLocation("positionMap");
    if (positionMapLocation == -1) {
        return false;
    }

    return true;
}

void CausticShading::setIsRefractive(bool is_refractive) const
{
  glUniform1i(isRefractiveLocation, is_refractive ? 1 : 0);
}

void CausticShading::setPositionMap(unsigned tex_unit) const
{
  glUniform1i(positionMapLocation, tex_unit);
}
