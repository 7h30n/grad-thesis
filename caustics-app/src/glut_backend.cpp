#ifndef _WIN32
  #include <unistd.h>
#endif
#include <stdio.h>

#include <GL/glew.h>
#include <GL/freeglut.h>

#include "glut_backend.hpp"

// ICallbacks delivered to GLUTBackendRun()
static ICallbacks *s_callbacks = NULL;

static void specialKeyboardCB(int key, int x, int y)
{
    s_callbacks->specialKeyboardCB(key, x, y);
}

static void keyboardCB(unsigned char key, int x, int y)
{
    s_callbacks->keyboardCB(key, x, y);
}

static void draggingMouseCB(int x, int y)
{
    s_callbacks->draggingMouseCB(x, y);
}

static void passiveMouseCB(int x, int y)
{
    s_callbacks->passiveMouseCB(x, y);
}

static void mouseCB(int button, int state, int x, int y)
{
    s_callbacks->mouseCB(button, state, x, y);
}

static void renderSceneCB()
{
    s_callbacks->renderSceneCB();
}

static void idleCB()
{
    s_callbacks->idleCB();
}

static void reshapeCB(int w, int h)
{
    s_callbacks->reshapeCB(w, h);
}

static void initCallbacks()
{
    glutDisplayFunc(renderSceneCB);
    glutIdleFunc(idleCB);
    glutSpecialFunc(specialKeyboardCB);
    glutMotionFunc(draggingMouseCB);
    glutPassiveMotionFunc(passiveMouseCB);
    glutMouseFunc(mouseCB);
    glutKeyboardFunc(keyboardCB);
    glutReshapeFunc(reshapeCB);
}

static void printGLInfo()
{
    const GLubyte *renderer = glGetString(GL_RENDERER);
    const GLubyte *vendor = glGetString(GL_VENDOR);
    const GLubyte *version = glGetString(GL_VERSION);
    const GLubyte *glslVersion = glGetString(GL_SHADING_LANGUAGE_VERSION);
    GLint major, minor;
    glGetIntegerv(GL_MAJOR_VERSION, &major);
    glGetIntegerv(GL_MINOR_VERSION, &minor);
    printf("GL Vendor: %s\n", vendor);
    printf("GL Renderer: %s\n", renderer);
    printf("GL Version: %s\n", version);
    printf("GL Version: %d.%d\n", major, minor);
    printf("GLSL Version: %s\n", glslVersion);
}

void GLUTBackendInit(int argc, char **argv)
{
    glutInit(&argc, argv);
    glutInitContextVersion(4, 0);
    glutInitContextFlags(GLUT_FORWARD_COMPATIBLE);
    glutInitContextProfile(GLUT_CORE_PROFILE);
    glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGBA);
    glutSetOption(GLUT_ACTION_ON_WINDOW_CLOSE,
                    GLUT_ACTION_GLUTMAINLOOP_RETURNS);
}

bool GLUTBackendCreateWindow(unsigned int width, unsigned int height,
        unsigned int bpp, bool isFullscreen, const char *title)
{
#ifndef _WIN32
    if (isFullscreen) {
        char modeString[64] = {0};
        snprintf(modeString, sizeof(modeString), "%dx%d@%d",
                    width, height, bpp);
        glutGameModeString(modeString);
        glutEnterGameMode();
    } else {
        glutInitWindowSize(width, height);
        glutCreateWindow(title);
    }
#else
    glutInitWindowSize(width, height);
    glutCreateWindow(title);
#endif

    // This is needed for beta GPU drivers,
    // otherwise glew bindings will not work.
    glewExperimental = GL_TRUE;
    // Must be done after glut is initialized!
    GLenum res = glewInit();
    if (res != GLEW_OK) {
      fprintf(stderr, "Error: '%s'\n", glewGetErrorString(res));
      return false;
    }
    GLenum err = glGetError();
    // glewInit might set GL_INVALID_ENUM error.
    if (err != GL_NO_ERROR && err != GL_INVALID_ENUM) {
      fprintf(stderr, "glGetError returned: %d\n", err);
    }
    printGLInfo();
    return true;
}

void GLUTBackendRun(ICallbacks *callbacks)
{
    if (!callbacks) {
        fprintf(stderr, "%s : callbacks not specified!\n", __FUNCTION__);
        return;
    }

    glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
    glFrontFace(GL_CCW);
    glCullFace(GL_BACK);
    glEnable(GL_CULL_FACE);
    glEnable(GL_DEPTH_TEST);
    glDepthFunc(GL_LESS);

    s_callbacks = callbacks;
    initCallbacks();
    glutMainLoop();
}
