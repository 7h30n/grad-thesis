#include "position_shading.hpp"

#include <stdio.h>

bool PositionShading::init()
{
    if (!Technique::init()) {
        return false;
    }

    if (!addShaderFromFile(GL_VERTEX_SHADER, "shaders/pass.vert")) {
        return false;
    }

    if (!addShaderFromFile(GL_FRAGMENT_SHADER, "shaders/position.frag")) {
        return false;
    }

    if (!finalize()) {
        return false;
    }
    return true;
}
