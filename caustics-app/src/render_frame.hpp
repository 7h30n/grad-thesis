#ifndef RENDER_FRAME_H_
#define RENDER_FRAME_H_

#include <GL/glew.h>

class RenderFrame {
  public:
    RenderFrame(unsigned width, unsigned height);
    ~RenderFrame();

    bool Init();
    void BindForWriting();
    void BindForReading(GLenum tex_unit);
    void BindColorMap(GLenum tex_unit);
    void BindDepthMap(GLenum tex_unit);
    void RenderColor() const;
    void RenderPosition() const;
    void RenderDepth() const;

    unsigned width() const { return width_; }
    unsigned height() const { return height_; }

  private:
    RenderFrame(const RenderFrame&);
    RenderFrame &operator=(const RenderFrame&);
    unsigned width_;
    unsigned height_;
    GLuint fbo_;
    GLuint color_map_;
    GLuint depth_map_;
    GLuint pos_map_;
};

#endif
