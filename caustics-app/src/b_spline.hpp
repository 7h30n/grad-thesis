/*
 * Computer Graphics course; Laboratory exercises
 *
 * Faculty of Electrical Engineering and Computing, Zagreb
 *
 * Copyright (C) 2013 Teon Banek
 *
 * This file is part of Laboratory exercises for Computer Graphics course
 * at Faculty of Electrical Engineering and Computing.
 *
 * Author(s): Teon Banek <teon.banek@fer.hr>
 * Date: 2013-10-05
 */
#ifndef B_SPLINE_H_
#define B_SPLINE_H_

#include <vector>

#include <glm/glm.hpp>

class BSpline {
 public:
  bool readFile(const char *filename);
  const std::vector<glm::vec3> &getPoints() const;
  const std::vector<glm::vec3> &getControlPoints() const;
  const std::vector<glm::vec3> &tangents() const;
 private:
  void calculate();
  glm::vec3 calculatePoint(float t, size_t i);
  glm::vec3 calculateTangent(float t, size_t i);

  std::vector<glm::vec3> points;
  std::vector<glm::vec3> controlPoints;
  std::vector<glm::vec3> tangents_;
};

#endif

