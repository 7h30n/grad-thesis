#ifndef POSITION_SHADING_H
#define POSITION_SHADING_H

#include <glm/glm.hpp>

#include "technique.hpp"

class PositionShading: public Technique
{
  public:
    virtual bool init();
};

#endif

