#include "scene_graph.hpp"

#include "material.hpp"

MaterialNode::MaterialNode(Material &mat)
  : mat_(mat),
    next_node_(0),
    is_refractive_(false)
{
}

MaterialNode::~MaterialNode()
{
  if (next_node_) {
    delete next_node_;
  }
}

void MaterialNode::AcceptVisitor(SceneVisitor &visitor)
{
  visitor.Visit(*this);
  if (next_node_) {
    next_node_->AcceptVisitor(visitor);
  }
  visitor.FinishedVisit(*this);
}

void MaterialNode::Enable() const
{
  mat_.Enable();
}

Node *MaterialNode::next_node() const
{
  return next_node_;
}

void MaterialNode::set_next_node(Node *node)
{
  next_node_ = node;
}
