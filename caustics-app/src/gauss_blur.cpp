#include "gauss_blur.hpp"

#include <cmath>
#include <cstring>
#include <iostream>

#include "mesh_renderer.hpp"
#include "model.hpp"
#include "texture_shading.hpp"

static const float PI = 3.14159f;

static float gauss(float x, float sigma2)
{
  return exp((x*x) / (-2*sigma2)) / sqrt(2*PI*sigma2);
}

GaussBlur::GaussBlur(unsigned width, unsigned height)
  : width_(width), height_(height), sigma2_(4.0f)
{
  memset(weights_, 0, sizeof(weights_));
  memset(fbo_, 0, sizeof(fbo_));
  memset(textures_, 0, sizeof(textures_));
}

GaussBlur::~GaussBlur()
{
  glDeleteTextures(2, textures_);
  glDeleteFramebuffers(2, fbo_);
}

bool GaussBlur::Init()
{
  if (!blur_shader_.init()) {
    return false;
  }
  glGenFramebuffers(2, fbo_);
  glGenTextures(2, textures_);

  for (int i = 0; i < 2; ++i) {
    glBindFramebuffer(GL_FRAMEBUFFER, fbo_[i]);
    glBindTexture(GL_TEXTURE_2D, textures_[i]);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, width_, height_, 0,
                 GL_RGBA, GL_UNSIGNED_BYTE, 0);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0,
                           GL_TEXTURE_2D, textures_[i], 0);
    GLenum draw_bufs[] = { GL_COLOR_ATTACHMENT0 };
    glDrawBuffers(1, draw_bufs);
    if (glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE) {
      std::cerr << "Error: Framebuffer is not complete" << std::endl;
      return false;
    }
  }
  glBindFramebuffer(GL_FRAMEBUFFER, 0);
  float sum = weights_[0] = gauss(0, sigma2_);
  for (int i = 1; i < 5; ++i) {
    weights_[i] = gauss(i, sigma2_);
    sum += 2 * weights_[i];
  }
  for (int i = 0; i < 5; ++i) {
    weights_[i] = weights_[i] / sum;
  }
  blur_shader_.enable();
  blur_shader_.setWeights(weights_);
  blur_shader_.setWidth(width_);
  blur_shader_.setHeight(height_);
  return true;
}

void GaussBlur::Blur(int tex_unit)
{
  Model plane(Model::Plane());
  MeshRenderer plane_mesh(plane);
  if (!plane_mesh.Init()) {
    std::cerr << "Error: Unable to init plane_mesh" << std::endl;
    return;
  }
  glDisable(GL_DEPTH_TEST);
  // First pass
  glBindFramebuffer(GL_FRAMEBUFFER, fbo_[0]);
  glViewport(0, 0, width_, height_);
  glClear(GL_COLOR_BUFFER_BIT);
  blur_shader_.enable();
  blur_shader_.setPass(1);
  blur_shader_.setTexture(tex_unit);
  plane_mesh.Render();
  // Second pass
  glBindTexture(GL_TEXTURE_2D, textures_[0]);
  glBindFramebuffer(GL_FRAMEBUFFER, fbo_[1]);
  glClear(GL_COLOR_BUFFER_BIT);
  blur_shader_.setPass(2);
  plane_mesh.Render();
  glBindFramebuffer(GL_FRAMEBUFFER, 0);
  glEnable(GL_DEPTH_TEST);
}

void GaussBlur::BindForReading(GLenum tex_unit)
{
  glActiveTexture(tex_unit);
  glBindTexture(GL_TEXTURE_2D, textures_[1]);
}

void GaussBlur::RenderBlur()
{
  BindForReading(GL_TEXTURE0);
  Model plane(Model::Plane());
  MeshRenderer plane_mesh(plane);
  if (!plane_mesh.Init()) {
    return;
  }
  TextureShading texture_shading;
  if (!texture_shading.init()) {
    return;
  }
  texture_shading.enable();
  plane_mesh.Render();
}

bool BlurShading::init()
{
  if (!Technique::init()) {
    return false;
  }
  if (!addShaderFromFile(GL_VERTEX_SHADER, "shaders/pass.vert")) {
    return false;
  }
  if (!addShaderFromFile(GL_FRAGMENT_SHADER, "shaders/gauss_blur.frag")) {
    return false;
  }
  if (!finalize()) {
    return false;
  }

  weightsLocation = getUniformLocation("weights");
  if (weightsLocation == -1) {
    return false;
  }
  passLocation = getUniformLocation("pass");
  if (passLocation == -1) {
    return false;
  }
  textureLocation = getUniformLocation("texture0");
  if (textureLocation == -1) {
    return false;
  }
  return true;
}

void BlurShading::setWeights(float weights[5]) const
{
  glUniform1fv(weightsLocation, 5, weights);
}

void BlurShading::setPass(int i) const
{
  glUniform1i(passLocation, i);
}

void BlurShading::setTexture(int tex_unit) const
{
  glUniform1i(textureLocation, tex_unit);
}

void BlurShading::setWidth(int width) const
{
  GLint loc = getUniformLocation("width");
  glUniform1i(loc, width);
}

void BlurShading::setHeight(int height) const
{
  GLint loc = getUniformLocation("height");
  glUniform1i(loc, height);
}
