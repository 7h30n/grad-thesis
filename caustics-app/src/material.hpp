#ifndef MATERIAL_H_
#define MATERIAL_H_

class Material {
  public:
    virtual ~Material() {}

    virtual void Enable() const =0;
};

#endif
