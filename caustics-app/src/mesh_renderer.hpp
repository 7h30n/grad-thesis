/*
 * Computer Graphics course; Laboratory exercises
 *
 * Faculty of Electrical Engineering and Computing, Zagreb
 *
 * Copyright (C) 2013 Teon Banek
 *
 * This file is part of Laboratory exercises for Computer Graphics course
 * at Faculty of Electrical Engineering and Computing.
 *
 * Author(s): Teon Banek <teon.banek@fer.hr>
 * Date: 2013-10-05
 */

/*
 * The following file contains the declaration of MeshRenderer class.
 * This class is used to render Mesh objects.
 */

#ifndef MESH_RENDERER
#define MESH_RENDERER

#include "renderer.hpp"

#include <vector>

#include <GL/glew.h>
#include <glm/glm.hpp>

class Model;
struct VertexData;

/**
 * Renders the mesh objects using indexed drawing.
 * Constructor takes a reference to mesh object.
 * Before using the class Init must be called.
 * Sample usage:
 *     MeshRenderer renderer(mesh);
 *     renderer.Init();
 *     ...
 *     renderer.Render();
 */
class MeshRenderer : public Renderer {
 public:
  /**
   * Stores the reference to mesh which will be rendered.
   */
  MeshRenderer(Model &mesh);
  virtual ~MeshRenderer();

  virtual bool Init();
  virtual void Render();

 private:

  /**
   * Calculates the normal for vertex at given index position.
   */
  glm::vec3 CalculateNormal(unsigned vertex_index);
  void CreateVertexData(
      std::vector<VertexData> &vertex_data, std::vector<unsigned> &indices);

  Model &mesh_;
  GLuint vertex_buffer_object_;
  GLuint index_buffer_object_;
  GLuint vertex_array_object_;
};

#endif
