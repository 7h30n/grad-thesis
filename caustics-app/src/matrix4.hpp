#ifndef MATRIX4_H
#define MATRIX4_H

#include <glm/glm.hpp>

class Matrix4 {
 public:
  static glm::mat4 Translate(glm::vec3 pos);
  static glm::mat4 Scale(float sx, float sy, float sz);
   static glm::mat4 Perspective(float fov, float ar, float z_near, float z_far);
  static glm::mat4 LookAt(glm::vec3 pos, glm::vec3 target, glm::vec3 up);

  static const double PI;
  static const double DEG2RAD;
  static const double RAD2DEG;
};

#endif
