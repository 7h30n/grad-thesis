/////////////////////////////////////////////////////
// Interactive Computer Graphics
//
// Laboratory exercises
// III cycle
// Exercise 6: Drawing Bezier curve.
//
// Author: Teon Banek
// Date: 05.2013.
// Faculty of Electrical Engineering and Computing
/////////////////////////////////////////////////////
#ifndef BEZIER_CURVE_H
#define BEZIER_CURVE_H

#include <vector>

#include <glm/glm.hpp>

class BezierCurve {
    public:
        bool readFile(const char *filename);
        const std::vector<glm::vec3> &getPoints();
        const std::vector<glm::vec3> &getControlPoints();
    private:
        void calculate();
        glm::vec3 calculatePoint(float t);
        float bernsteinPolynom(int i, int n, float t);

        std::vector<glm::vec3> points;
        std::vector<glm::vec3> controlPoints;
};

#endif
