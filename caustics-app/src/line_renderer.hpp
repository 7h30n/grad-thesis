/*
 * Computer Graphics course; Laboratory exercises
 *
 * Faculty of Electrical Engineering and Computing, Zagreb
 *
 * Copyright (C) 2013 Teon Banek
 *
 * This file is part of Laboratory exercises for Computer Graphics course
 * at Faculty of Electrical Engineering and Computing.
 *
 * Author(s): Teon Banek <teon.banek@fer.hr>
 * Date: 2013-10-05
 */

/*
 * The following file contains the declaration of line renderer class.
 * This class is used to render arbitrary number of points as line strip.
 */

#ifndef LINE_RENDERER
#define LINE_RENDERER

#include "renderer.hpp"

#include <GL/glew.h>
#include <glm/glm.hpp>
#include <vector>

/**
 * Renders the points as line strip.
 * Before using the class Init must be called.
 * Sample usage:
 *     Renderer *renderer = new LineRenderer(points);
 *     renderer->Init();
 *     ...
 *     renderer->Render();
 *     delete renderer;
 */
class LineRenderer : public Renderer {
 public:
  LineRenderer(const std::vector<glm::vec3> &points);
  virtual ~LineRenderer();

  virtual bool Init();
  virtual void Render();
  void RenderSegment(int i, int n);

 private:
  std::vector<glm::vec3> points_;
  GLuint vertex_buffer_object_;
};

#endif
