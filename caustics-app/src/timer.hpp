#ifndef TIMER_H
#define TIMER_H

#ifdef _WIN32
#include "timer_win.hpp"
#else
#include <sys/time.h>

class Timer {
  public:
    Timer() : start_(0), last_(0)
    {
      Start();
    }

    void Start()
    {
      timeval tv;
      gettimeofday(&tv, 0);
      start_ = tv.tv_sec * 1e6 + tv.tv_usec;
    }

    // Returns time since last check in seconds.
    double GetDeltaTime()
    {
      timeval tv;
      gettimeofday(&tv, 0);
      last_ = start_;
      start_ = tv.tv_sec * 1e6 + tv.tv_usec;
      return (start_ - last_) * 1e-6;
    }

  private:
    // Track times in microseconds.
    unsigned long start_;
    unsigned long last_;
};
#endif // _WIN32

#endif
