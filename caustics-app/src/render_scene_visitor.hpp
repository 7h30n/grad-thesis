#ifndef RENDER_SCENE_VISITOR_H_
#define RENDER_SCENE_VISITOR_H_

#include <list>

#include <glm/glm.hpp>

#include "scene_visitor.hpp"

class Technique;

class RenderSceneVisitor : public SceneVisitor {
 public:
  RenderSceneVisitor(unsigned matrices_ubo);
  RenderSceneVisitor(unsigned matrices_ubo, bool material);

  virtual void Visit(const SceneNode &node);
  virtual void FinishedVisit(const SceneNode &node) {}
  virtual void Visit(const MaterialNode &node);
  virtual void FinishedVisit(const MaterialNode &node);
  virtual void Visit(const TransformNode &node);
  virtual void FinishedVisit(const TransformNode &node);
  virtual void Visit(const GeoNode &node);
  virtual void FinishedVisit(const GeoNode &node) {}

  void set_render_refractive(bool rend) { render_refractive_ = rend; }
  void set_render_receiver(bool rend) { render_receiver_ = rend; }
 private:
  typedef std::list<const MaterialNode*> MaterialStack;
  typedef std::list<glm::mat4> MatrixStack;

  MaterialStack material_stack_;
  MatrixStack matrix_stack_;
  unsigned global_matrices_ubo_;
  bool use_material_;
  bool render_refractive_;
  bool render_receiver_;
};
#endif
