#ifndef SCENE_GRAPH_H_
#define SCENE_GRAPH_H_

#include <list>
#include <string>

#include <glm/glm.hpp>

#include "scene_visitor.hpp"

class Material;
class Renderer;
class Technique;
class Texture;

class Node {
 public:
  virtual ~Node() {}

  virtual void AcceptVisitor(SceneVisitor &visitor)=0;

  const std::string &name() const { return name_; }
  void set_name(std::string &name) { name_ = name; }

 private:
  std::string name_;
};

class SceneNode : public Node {
 public:
  virtual ~SceneNode();

  virtual void AcceptVisitor(SceneVisitor &visitor);
  virtual void AddNode(Node *node);
  virtual void RemoveNode(Node *node);

  const std::list<Node*> &children() const;

 private:
  std::list<Node*> children_;
};

class MaterialNode : public Node {
 public:
  MaterialNode(Material &mat);
  //MaterialNode(Technique &technique);
  virtual ~MaterialNode();

  virtual void AcceptVisitor(SceneVisitor &visitor);
  void Enable() const; // This should move to future Material class.

  //void set_technique(Technique &technique);
  //Texture *texture() const;
  //void set_texture(Texture *texture);
  Node *next_node() const;
  void set_next_node(Node *node);
  bool is_refractive() const { return is_refractive_; }
  void set_is_refractive(bool refr) { is_refractive_ = refr; }

 private:
  Material &mat_;
  //Technique &technique_;
  Node *next_node_;
  bool is_refractive_;
  //Texture *texture_;
};

class TransformNode : public Node {
  public:
    TransformNode(glm::mat4 transform);
    virtual ~TransformNode();

    virtual void AcceptVisitor(SceneVisitor &visitor);

    const glm::mat4 &transform() const { return transform_; }
    void set_transform(glm::mat4 transform) { transform_ = transform; }
    Node *next_node() const { return next_node_; }
    void set_next_node(Node *node) { next_node_ = node; }

  private:
    glm::mat4 transform_;
    Node *next_node_;
};

class GeoNode : public Node {
 public:
  GeoNode(Renderer &renderer);

  virtual void AcceptVisitor(SceneVisitor &visitor);

  Renderer &renderer() const;
  void set_renderer(Renderer &renderer);

 private:
  Renderer &renderer_;
};
#endif
