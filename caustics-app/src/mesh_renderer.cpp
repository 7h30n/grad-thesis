/*
 * Computer Graphics course; Laboratory exercises
 *
 * Faculty of Electrical Engineering and Computing, Zagreb
 *
 * Copyright (C) 2013 Teon Banek
 *
 * This file is part of Laboratory exercises for Computer Graphics course
 * at Faculty of Electrical Engineering and Computing.
 *
 * Author(s): Teon Banek <teon.banek@fer.hr>
 * Date: 2013-10-05
 */

/*
 * The contents of the file are the definitions of methods for MeshRenderer.
 */

#include "mesh_renderer.hpp"

#include <cstddef>
#include <list>
#include <map>

#include "glw/glw.hpp"
#include "model.hpp"

struct VertexData {
  glm::vec3 position;
  glm::vec3 normal;
  glm::vec2 uv;
};

MeshRenderer::MeshRenderer(Model &mesh)
  : mesh_(mesh), vertex_buffer_object_(0), index_buffer_object_(0),
    vertex_array_object_(0)
{
}

MeshRenderer::~MeshRenderer()
{
  glDeleteBuffers(1, &vertex_buffer_object_);
  glDeleteBuffers(1, &index_buffer_object_);
  glDeleteVertexArrays(1, &vertex_array_object_);
}

/**
 * Creates 2 buffer objects, one with vertex data the other with
 * vertex indices for indexed drawing.
 * Vertex data consists of positions and vertex normals; each position is
 * followed by normal vector values.
 */
bool MeshRenderer::Init()
{
  glGenVertexArrays(1, &vertex_array_object_);
  glw::BoundVertexArray bound_vao(vertex_array_object_);

  std::vector<VertexData> vertex_data;
  std::vector<unsigned> indices;
  CreateVertexData(vertex_data, indices);

  glGenBuffers(1, &vertex_buffer_object_);
  glBindBuffer(GL_ARRAY_BUFFER, vertex_buffer_object_);
  glBufferData(GL_ARRAY_BUFFER, vertex_data.size() * sizeof(vertex_data[0]),
                &vertex_data[0], GL_STATIC_DRAW);
  glw::VertexAttrib pos_attr(0);
  glw::VertexAttrib normal_attr(1);
  glw::VertexAttrib uv_attr(2);
  GLsizei stride = sizeof(vertex_data[0]);
  const GLvoid* normal_offset =
    reinterpret_cast<const GLvoid*>(offsetof(VertexData, normal));
  const GLvoid* uv_offset =
    reinterpret_cast<const GLvoid*>(offsetof(VertexData, uv));
  pos_attr.Pointer(3, GL_FLOAT, GL_FALSE, stride, 0);
  normal_attr.Pointer(3, GL_FLOAT, GL_FALSE, stride, normal_offset);
  uv_attr.Pointer(2, GL_FLOAT, GL_FALSE, stride, uv_offset);

  glGenBuffers(1, &index_buffer_object_);
  glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, index_buffer_object_);
  glBufferData(GL_ELEMENT_ARRAY_BUFFER, indices.size() * sizeof(indices[0]),
                &indices[0], GL_STATIC_DRAW);

  return true;
}

/**
 * Performs indexed drawing of triangles from the stored buffers.
 */
void MeshRenderer::Render()
{
  glw::BoundVertexArray bound_vao(vertex_array_object_);
  glDrawElements(GL_TRIANGLES, mesh_.faces().size() * 3, GL_UNSIGNED_INT, 0);
}

/**
 * Calculates the normal for vertex by averaging the normals of adjacent faces.
 */
glm::vec3 MeshRenderer::CalculateNormal(unsigned vertex_index)
{
  // TODO: Move to model.cc
  // Calculate on each face read and add to previous while keeping track of
  // total count. Some faces might have normal indexes :/
  int count = 1;
  const std::vector<Face> &faces = mesh_.faces();
  glm::vec3 normal(0.0f, 0.0f, 0.0f);
  for (size_t i = 0; i < faces.size(); ++i) {
    for (int n = 0; n < 3; ++n) {
      if (faces[i].index[n] == vertex_index) {
        glm::vec4 p = faces[i].plane;
        normal += glm::vec3(p.x, p.y, p.z);
        count++;
        break;
      }
    }
  }
  return normal / (float)count;
}

static int addVertexData(std::vector<VertexData> &vertex_data,
    unsigned vi, const VertexData &vertex,
    std::map<unsigned, std::list<unsigned> > &copied_vertexes);

void MeshRenderer::CreateVertexData(
    std::vector<VertexData> &vertex_data, std::vector<unsigned> &indices)
{
  const std::vector<glm::vec3> &vertices = mesh_.vertices();
  const std::vector<glm::vec3> &normals = mesh_.normals();
  const std::vector<glm::vec2> &uvs = mesh_.uvs();
  const std::vector<Face> &faces = mesh_.faces();
  vertex_data.reserve(vertices.size()); // at least as much
  indices.reserve(faces.size() * 3); // face is a triangle
  std::map<unsigned, std::list<unsigned> > copied_vertexes;
  for (size_t i = 0; i < mesh_.faces().size(); ++i) {
    for (size_t v = 0; v < 3; ++v) {
      unsigned vi = faces[i].index[v]; // vertex index
      unsigned uvi = faces[i].uv_index[v]; // uv index
      unsigned vn = faces[i].vn_index[v]; // normal index
      glm::vec3 position(vertices[vi]);
      glm::vec3 normal;
      glm::vec2 uv;
      if (vn < normals.size()) {
        normal = normals[vn];
      } else {
        // TODO: Move normal calculation to model.cc
        normal = CalculateNormal(vi);
      }
      if (uvi < uvs.size()) {
        uv = glm::vec2(uvs[uvi].x, uvs[uvi].y);
      } else {
        // TODO: Set texture in model.cc ?
        uv = glm::vec2(0.0f);
      }
      VertexData vertex = { position, normal, uv };
      vi = addVertexData(vertex_data, vi, vertex, copied_vertexes);
      indices.push_back(vi);
    }
  }
}

static int addVertexData(std::vector<VertexData> &vertexes,
    unsigned vi, const VertexData &vertex,
    std::map<unsigned, std::list<unsigned> > &copied_vertexes)
{
  std::map<unsigned, std::list<unsigned> >::iterator find_it;
  find_it = copied_vertexes.find(vi);
  if (find_it == copied_vertexes.end()) {
    std::list<unsigned> list;
    list.push_back(vertexes.size());
    copied_vertexes[vi] = list;
    vertexes.push_back(vertex);
    return vertexes.size() -1;
  }
  std::list<unsigned> &list = find_it->second;
  std::list<unsigned>::iterator it;
  for (it = list.begin(); it != list.end(); ++it) {
    if (vertex.position == vertexes[*it].position
        && vertex.normal == vertexes[*it].normal
        && vertex.uv == vertexes[*it].uv) {
      return *it;
    }
  }
  list.push_back(vertexes.size());
  vertexes.push_back(vertex);
  return vertexes.size() - 1;
}
