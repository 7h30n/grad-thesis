/*
 * Computer Graphics course; Laboratory exercises
 *
 * Faculty of Electrical Engineering and Computing, Zagreb
 *
 * Copyright (C) 2013 Teon Banek
 *
 * This file is part of Laboratory exercises for Computer Graphics course
 * at Faculty of Electrical Engineering and Computing.
 *
 * Author(s): Teon Banek <teon.banek@fer.hr>
 * Date: 2013-10-23
 */

/*
 * This file contains functions for initializing GLUT context.
 */
#ifndef GLUT_BACKEND_H
#define GLUT_BACKEND_H

#include "callbacks.hpp"

/**
 * Initializes the */
void GLUTBackendInit(int argc, char **argv);

bool GLUTBackendCreateWindow(unsigned int width, unsigned int height,
        unsigned int bpp, bool isFullscreen, const char *title);

void GLUTBackendRun(ICallbacks *callbacks);

#endif
