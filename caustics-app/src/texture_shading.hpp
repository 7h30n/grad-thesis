#ifndef TEXTURE_SHADING_H
#define TEXTURE_SHADING_H

#include "technique.hpp"

class TextureShading: public Technique
{
  public:
    virtual bool init();
    void setTexture(unsigned tex_unit);

  private:
    GLint textureLocation;
};

#endif
