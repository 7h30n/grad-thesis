#ifndef SHADOW_MAP_H_
#define SHADOW_MAP_H_

#include <GL/glew.h>

class ShadowMap {
  public:
    ShadowMap(unsigned width, unsigned height);
    ~ShadowMap();

    bool Init();
    void BindForWriting();
    void BindForReading(GLenum tex_unit);

    unsigned width() const { return width_; }
    unsigned height() const { return height_; }

  private:
    unsigned width_;
    unsigned height_;
    GLuint fbo_;
    GLuint shadow_map_;
};

#endif
