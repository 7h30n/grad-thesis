#ifndef BILLBOARD_TECHNIQUE_H_
#define BILLBOARD_TECHNIQUE_H_

#include "technique.hpp"

#include <glm/glm.hpp>

class BillboardTechnique : public Technique
{
  public:
    BillboardTechnique();

    virtual bool init();
    void setCameraPos(const glm::vec3 &cameraPos);

  private:
    GLint cameraPosLocation;
};

#endif
