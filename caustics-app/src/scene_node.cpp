#include "scene_graph.hpp"

#include <algorithm>

static void DeleteNode(Node *node)
{
  delete node;
}

SceneNode::~SceneNode()
{
  std::for_each(children_.begin(), children_.end(), DeleteNode);
  children_.clear();
}

void SceneNode::AcceptVisitor(SceneVisitor &visitor)
{
  visitor.Visit(*this);
  std::list<Node*>::iterator it = children_.begin();
  for ( ; it != children_.end(); ++it) {
    (*it)->AcceptVisitor(visitor);
  }
  visitor.FinishedVisit(*this);
}

void SceneNode::AddNode(Node *node)
{
  children_.push_back(node);
}

void SceneNode::RemoveNode(Node *node)
{
  children_.remove(node);
}

const std::list<Node*> &SceneNode::children() const
{
  return children_;
}
