#include "billboards.hpp"

Billboards::Billboards() : vb_(0), tex_(0)
{
}

Billboards::~Billboards()
{
  if (tex_) {
    delete tex_;
  }
  glDeleteBuffers(1, &vb_);
}

bool Billboards::init(const std::string &texFilename)
{
  tex_ = new Texture(GL_TEXTURE_2D, texFilename);
  if (!tex_->load()) {
    return false;
  }

  createPositionBuffer();

  if (!technique_.init()) {
    return false;
  }
  return true;
}

void Billboards::render(const glm::vec3 &cameraPos)
{
  technique_.enable();
  technique_.setCameraPos(cameraPos);
  tex_->bind(GL_TEXTURE0);
  glEnableVertexAttribArray(0);
  glBindBuffer(GL_ARRAY_BUFFER, vb_);
  glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, 0);
  glDrawArrays(GL_POINTS, 0, 1);
  glDisableVertexAttribArray(0);
}

void Billboards::createPositionBuffer()
{
  glm::vec3 pos(0.0f, 0.0f, 0.0f);
  glGenBuffers(1, &vb_);
  glBindBuffer(GL_ARRAY_BUFFER, vb_);
  glBufferData(GL_ARRAY_BUFFER, sizeof(pos), &pos, GL_STATIC_DRAW);
}
