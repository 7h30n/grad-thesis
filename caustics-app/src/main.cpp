/*
 * Computer Graphics course; Laboratory exercises
 *
 * Faculty of Electrical Engineering and Computing, Zagreb
 *
 * Copyright (C) 2013 Teon Banek
 *
 * This file is part of Laboratory exercises for Computer Graphics course
 * at Faculty of Electrical Engineering and Computing.
 *
 * Author(s): Teon Banek <teon.banek@fer.hr>
 * Date: 2014-01-xx
 */

/*
 * The following file contains the declaration and definition of
 * Vjezba class which is the main class in this project.
 * After the class is the main function which starts the program.
 */

#include <cmath>
#include <stdio.h>
#include <sstream>
#include <string>

#include <GL/glew.h>
#include <GL/freeglut.h>

#define GLM_FORCE_RADIANS 1
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>

#include "callbacks.hpp"
#include "caustic_map.hpp"
#include "caustic_shading.hpp"
#include "gauss_blur.hpp"
#include "glut_backend.hpp"
#include "matrix4.hpp"
#include "mesh_renderer.hpp"
#include "model.hpp"
#include "phong_material.hpp"
#include "phong_shading.hpp"
#include "render_frame.hpp"
#include "render_scene_visitor.hpp"
#include "scene_graph.hpp"
#include "shadow_map.hpp"
#include "simple_technique.hpp"
#include "skybox.hpp"
#include "texture.hpp"
#include "timer.hpp"
#include "vol_light_shading.hpp"

int g_width = 800;
int g_height = 600;

// Print all OpenGL errors to stderr.
static void flushGLErrors();

/**
 * Implements the ICallbacks interface and serves as the basis of
 * the application.
 * Sample usage:
 *    Vjezba3 vjezba;
 *    vjezba.Init(argv[1], argv[2]);
 *    vjezba.Run();
 */
class Vjezba3 : public ICallbacks {
 public:
  Vjezba3(int shadow_map_size)
      : plane(Model::Plane()),
        shadow_map_(shadow_map_size, shadow_map_size),
        caustic_map_(shadow_map_size, shadow_map_size),
        phong_shading_(0),
        mesh_renderer_(model),
        plane_mesh(plane),
        global_matrices_ubo_(0),
        plane_tex_(GL_TEXTURE_2D, "textures/skybox/botsh.jpg"),
        model_mat_(0),
        plane_mat_(0),
        render_frame_(g_width, g_height),
        gauss_blur_(shadow_map_size, shadow_map_size),
        total_delta_(0.0), frame_(0),
        cube(Model::Cube()), cube_mesh(cube)
  {
    drawWireframe = false;
    drawReceiverPositions = false;
    drawRefractivePositions = false;
    drawRefractiveNormals = false;
    drawCausticMap = false;
    isBlurOn = true;
    phong_shading_ = NULL;
    O = glm::vec3(0.0f, 1.0f, 1.0f);
    G = glm::vec3(0.0f, 0.0f, 0.0f);
    world_pos_ = glm::mat4(lookAt(O, G, glm::vec3(0.0f, 1.0f, 0.0f)));
    light.color = glm::vec3(1.0f, 1.0f, 1.0f);
    light.ambientK = 0.1f;
    light.diffuseK = 0.8;
    light.position = glm::vec4(0.0f, 3.0f, -3.0f, 0.0f);
    glm::vec3 lpos =
      glm::vec3(light.position.x, light.position.y, light.position.z);
    light_pos_ = glm::lookAt(lpos, G, glm::vec3(0.0f, 1.0f, 0.0f));
    light_projection_ = glm::ortho(-1.2f, 1.2f, -1.2f, 1.2f, -6.0f, 6.0f);
    scene_ = 0;
    model_transform_ = 0;
    tangentIndex_ = 0;
  }

  ~Vjezba3()
  {
    if (phong_shading_) {
      delete phong_shading_;
    }
    if (scene_) {
      delete scene_;
    }
    if (model_mat_) {
      delete model_mat_;
    }
    if (plane_mat_) {
      delete plane_mat_;
    }
    if (global_matrices_ubo_) {
      glDeleteBuffers(1, &global_matrices_ubo_);
    }
    glDeleteQueries(6, frame_time_q_);
  }

  /**
   * Takes path to .obj mesh file and to .txt file which describes the curved
   * path of the loaded mesh.
   * Initializes the scene, sets the shaders and returns true if everything
   * initialized as expected.
   */
  bool Init(const char *objFileName)
  {
    if (!render_frame_.Init()) {
      return false;
    }
    if (!gauss_blur_.Init()) {
      return false;
    }
    if (!vol_light_shading_.init()) {
      //return false;
    }
    if (!caustic_shading_.init()) {
      return false;
    }
    if (!caustic_map_.Init()) {
      return false;
    }
    if (!shadow_map_.Init()) {
      return false;
    }

    if (!model.readObjFile(objFileName)) {
      return false;
    }

    if (!plane_tex_.load()) {
      return false;
    }
    glGenQueries(6, frame_time_q_);
    phong_shading_ = new PhongShading();
    phong_shading_->init();

    glGenBuffers(1, &global_matrices_ubo_);
    glBindBuffer(GL_UNIFORM_BUFFER, global_matrices_ubo_);
    glBufferData(GL_UNIFORM_BUFFER, sizeof(glm::mat4) * 3, 0, GL_DYNAMIC_DRAW);
    glBindBuffer(GL_UNIFORM_BUFFER, 0);

    GLuint global_uniform_block_index = glGetUniformBlockIndex(
            phong_shading_->getShaderProg(), "GlobalMatrices");
    glUniformBlockBinding(phong_shading_->getShaderProg(),
            global_uniform_block_index, GLOBAL_MATRICES_BINDING_INDEX);

    global_uniform_block_index = glGetUniformBlockIndex(
            caustic_shading_.getShaderProg(), "GlobalMatrices");
    glUniformBlockBinding(caustic_shading_.getShaderProg(),
            global_uniform_block_index, GLOBAL_MATRICES_BINDING_INDEX);

    // Create Scene
    scene_ = new SceneNode();
    // Add model to scene
    model_mat_ =
      new PhongMaterial(*phong_shading_, glm::vec3(1.0f, 0.5f, 0.0f));
    MaterialNode *material = new MaterialNode(*model_mat_);
    material->set_is_refractive(true);
    model_mat_->set_is_reflective(true);
    material->set_next_node(new GeoNode(mesh_renderer_));
    glm::mat4 transform = glm::scale(glm::rotate(glm::mat4(1.0f),
                                                 glm::radians(0.0f),
                                                 glm::vec3(1.0f, 0.0f, 0.0f)),
                                     glm::vec3(0.5f));
    model_transform_ = new TransformNode(transform);
    model_transform_->set_next_node(material);
    scene_->AddNode(model_transform_);
    // Add plane to scene
    plane_mat_ = new PhongMaterial(*phong_shading_, glm::vec3(0.2f, 0.2f, 0.2f));
    MaterialNode *mat2 = new MaterialNode(*plane_mat_);
    mat2->set_next_node(new GeoNode(plane_mesh));
    glm::mat4 trans = Matrix4::Translate(glm::vec3(0.0f, 0.0f, -0.1f));
    trans *= Matrix4::Scale(2.0f, 2.0f, 1.0f);
    trans = glm::scale(glm::rotate(glm::translate(glm::mat4(1.0f),
                                                  glm::vec3(0.0f, -0.1f, 0.0f)),
                                   glm::radians(-90.0f),
                                   glm::vec3(1.0f, 0.0f, 0.0f)),
                       glm::vec3(2.0f, 2.0f, 1.0f));
    TransformNode *plane_trans = new TransformNode(trans);
    plane_trans->set_next_node(mat2);
    scene_->AddNode(plane_trans);

    // Add cubes to scene
    material = new MaterialNode(*model_mat_);
    material->set_next_node(new GeoNode(cube_mesh));
    trans = Matrix4::Translate(glm::vec3(0.0f, -5.0f, -0.3f));
    trans *= Matrix4::Scale(5.0f, 0.25f, 1.0f);
    TransformNode *cube_trans = new TransformNode(trans);
    cube_trans->set_next_node(material);
    // scene_->AddNode(cube_trans);

    material = new MaterialNode(*model_mat_);
    material->set_next_node(new GeoNode(cube_mesh));
    trans = Matrix4::Translate(glm::vec3(3.0f, -5.0f, 1.7f));
    trans *= Matrix4::Scale(2.0f, 0.25f, 1.0f);
    cube_trans = new TransformNode(trans);
    cube_trans->set_next_node(material);
    // scene_->AddNode(cube_trans);

    material = new MaterialNode(*model_mat_);
    material->set_next_node(new GeoNode(cube_mesh));
    trans = Matrix4::Translate(glm::vec3(-3.0f, -5.0f, 1.7f));
    trans *= Matrix4::Scale(2.0f, 0.25f, 1.0f);
    cube_trans = new TransformNode(trans);
    cube_trans->set_next_node(material);
    // scene_->AddNode(cube_trans);

    material = new MaterialNode(*model_mat_);
    material->set_next_node(new GeoNode(cube_mesh));
    trans = Matrix4::Translate(glm::vec3(0.0f, -5.0f, 3.7f));
    trans *= Matrix4::Scale(5.0f, 0.25f, 1.0f);
    cube_trans = new TransformNode(trans);
    cube_trans->set_next_node(material);
    // scene_->AddNode(cube_trans);

    mesh_renderer_.Init();
    plane_mesh.Init();
    cube_mesh.Init();

    if (!skybox.Init("textures/skybox", "leftsh.jpg", "rightsh.jpg",
                     "toptsh.jpg", "botsh.jpg",
                     "frontsh.jpg", "backsh.jpg",
                     GLOBAL_MATRICES_BINDING_INDEX)) {
      return false;
    }
    return true;
  }

  /**
   * Runs the application (itself).
   */
  void run()
  {
    GLUTBackendRun(this);
  }

  void renderSceneCB()
  {
    static bool is_first_frame = true;
    static GLuint64 frame_time = 0;
    static GLuint64 swap_time = 0;
    static int query_ix = 0;
    glQueryCounter(frame_time_q_[3*query_ix], GL_TIMESTAMP);
    if (!is_first_frame) {
      GLuint64 start_time = 0, time = 0, end_time = 0;
      int ix = (query_ix + 1) % 2;
      glGetQueryObjectui64v(frame_time_q_[3*ix], GL_QUERY_RESULT, &start_time);
      glGetQueryObjectui64v(frame_time_q_[3*ix + 1], GL_QUERY_RESULT, &time);
      glGetQueryObjectui64v(frame_time_q_[3*ix + 2], GL_QUERY_RESULT, &end_time);
      frame_time += time - start_time;
      swap_time += end_time - time;
    } else {
      is_first_frame = false;
    }
    flushGLErrors();
    static float angle = 0.0f;
    double dts = timer_.GetDeltaTime();
    glm::mat4 transform = glm::rotate(Matrix4::Scale(0.5f, 0.5f, 0.5f),
        static_cast<float>(90 * Matrix4::DEG2RAD), glm::vec3(1.0f, 0.0f, 0.0f));
    transform = glm::rotate(transform,
        static_cast<float>(90 * Matrix4::DEG2RAD), glm::vec3(0.0f, 1.0f, 0.0f));
    angle += 20.0f * dts;
    if (angle >= 360.0f) {
      angle -= 360.0f;
    }
    glm::vec3 axis(0.0f, 1.0f, 0.0f);
    transform = glm::rotate(transform,
        static_cast<float>(angle * Matrix4::DEG2RAD), axis);
    //model_transform_->set_transform(transform);

    total_delta_ += dts;
    frame_++;
    if (total_delta_ >= 0.5) {
      double fps = frame_ / total_delta_;
      double cpu_time = (total_delta_ * 1E3) / frame_;
      double gpu_time = (frame_time * 1E-6) / frame_;
      double gpu_swap = (swap_time * 1E-6) / frame_;
      frame_time = 0;
      swap_time = 0;
      frame_ = 0;
      total_delta_ = 0.0;
      std::stringstream ss;
      ss.precision(2);
      ss << "Caustics    CPU: " << std::fixed << cpu_time << "ms (" << fps <<
            " FPS)    GPU: " << gpu_time << "ms (" << gpu_swap << "ms swap)";
      std::string title = ss.str();
      glutSetWindowTitle(title.c_str());
    }

    ShadowMapPass();
    RenderPass();

    glBindFramebuffer(GL_FRAMEBUFFER, 0);
    glViewport(0, 0, g_width, g_height);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    if (drawReceiverPositions) {
      caustic_map_.RenderReceiverPositions();
    } else if (drawRefractivePositions) {
      caustic_map_.RenderRefractivePositions();
    } else if (drawRefractiveNormals) {
      caustic_map_.RenderRefractiveNormals();
    } else if (drawCausticMap) {
      caustic_map_.RenderCausticMap();
    } else {
      //render_frame_.BindColorMap(GL_TEXTURE0);
      //gauss_blur_.Blur(0);
      //glBindFramebuffer(GL_FRAMEBUFFER, 0);
      //gauss_blur_.RenderBlur();
      render_frame_.RenderColor();
    }
    // shadow_map_.BindForReading(GL_TEXTURE2);
    // vol_light_shading_.enable();
    // vol_light_shading_.setColorMap(0);
    // glm::vec4 cam = light_projection_ * light_pos_ * glm::vec4(O, 1.0f);
    // vol_light_shading_.setCameraLightSpacePos(glm::vec3(cam.x, cam.y, cam.z));
    // plane_mesh.Render();

    glQueryCounter(frame_time_q_[3*query_ix + 1], GL_TIMESTAMP);
    glutSwapBuffers();
    glQueryCounter(frame_time_q_[3*query_ix + 2], GL_TIMESTAMP);
    query_ix = (query_ix + 1) % 2;
  }

  void specialKeyboardCB(int key, int x, int y) {}
  void keyboardCB(unsigned char key, int x, int y)
  {
    glm::vec3 up(0.0f, 1.0f, 0.0f);
    glm::vec3 n = glm::normalize(G - O);
    glm::vec3 v = up - (n * up) * n;
    glm::vec3 u = glm::normalize(glm::cross(n, v));
    v = glm::cross(u, n);
    switch (key) {
    case 'W':
      drawWireframe = !drawWireframe;
      break;
    case 'w':
      O += n * 0.1f;
      world_pos_ = Matrix4::LookAt(O, G, up);
      break;
    case 's':
      O -= n * 0.1f;
      world_pos_ = Matrix4::LookAt(O, G, up);
      break;
    case 'd':
      O += u * 0.1f;
      world_pos_ = Matrix4::LookAt(O, G, up);
      break;
    case 'a':
      O -= u * 0.1f;
      world_pos_ = Matrix4::LookAt(O, G, up);
      break;
    case ' ':
      O += v * 0.1f;
      //G += v * 0.1f;
      world_pos_ = Matrix4::LookAt(O, G, up);
      break;
    case 'b':
      isBlurOn = !isBlurOn;
      break;
    case 'N':
      drawRefractiveNormals = !drawRefractiveNormals;
      break;
    case 'n':
      drawWireframe = false;
      drawReceiverPositions = false;
      drawRefractivePositions = false;
      drawRefractiveNormals = false;
      drawCausticMap = false;
      break;
    case 'C':
      drawCausticMap = !drawCausticMap;
      break;
    case 'c':
      O -= v * 0.1f;
      //G -= v * 0.1f;
      world_pos_ = Matrix4::LookAt(O, G, up);
      break;
    case 'P':
      drawReceiverPositions = !drawReceiverPositions;
      break;
    case 'q':
      glutLeaveMainLoop();
      break;
    case 'R':
      drawRefractivePositions = !drawRefractivePositions;
      break;
    default:
      break;
    }
  }

  void mouseCB(int button, int state, int x, int y) {}
  void draggingMouseCB(int x, int y) { passiveMouseCB(x, y); }
  void passiveMouseCB(int x, int y) {}
  void idleCB()
  {
    glutPostRedisplay();
  }

  void reshapeCB(int w, int h)
  {
    g_width = w;
    g_height = h;
    float ar = static_cast<float>(g_width) / g_height;
    projection_ = glm::perspective(glm::radians(75.0f), ar, 0.1f, 100.0f);
    glViewport(0, 0, g_width, g_height);
    glutPostRedisplay();
  }

 private:

  void ShadowMapPass()
  {
    //shadow_map_.BindForWriting();
    //glViewport(0, 0, shadow_map_.width(), shadow_map_.height());
    //glClear(GL_DEPTH_BUFFER_BIT);

    glViewport(0, 0, caustic_map_.width(), caustic_map_.height());
    caustic_shading_.enable();

    glBindBuffer(GL_UNIFORM_BUFFER, global_matrices_ubo_);
    glBufferSubData(GL_UNIFORM_BUFFER, 0, sizeof(glm::mat4),
        glm::value_ptr(light_projection_));

    glBufferSubData(GL_UNIFORM_BUFFER, sizeof(glm::mat4),
        sizeof(glm::mat4), glm::value_ptr(light_pos_));
    glBindBuffer(GL_UNIFORM_BUFFER, 0);

    glBindBufferRange(GL_UNIFORM_BUFFER, GLOBAL_MATRICES_BINDING_INDEX,
        global_matrices_ubo_, 0, sizeof(glm::mat4) * 3);

    caustic_shading_.setIsRefractive(false);
    // Receivers (everything)
    caustic_map_.BindForWritingReceiver();
    RenderSceneVisitor renderVisitor(global_matrices_ubo_, false);
    renderVisitor.set_render_refractive(false);
    renderVisitor.set_render_receiver(true);
    scene_->AcceptVisitor(renderVisitor);
    // Refractors
    caustic_map_.BindForWritingRefractive();
    // TODO: Reflective cull front, refractive cull back.
    glCullFace(GL_FRONT); // prevents self shadowing on plane
    renderVisitor.set_render_receiver(false);
    renderVisitor.set_render_refractive(true);
    scene_->AcceptVisitor(renderVisitor);


    // Create caustic map
    caustic_shading_.setIsRefractive(true);
    caustic_map_.BindPositionMap(GL_TEXTURE0);
    caustic_shading_.setPositionMap(0);
    caustic_map_.WriteCausticMap();

    glCullFace(GL_BACK);
    glBindFramebuffer(GL_FRAMEBUFFER, 0);
  }

  void RenderPass()
  {
    if (isBlurOn) {
      // Blur caustic map
      caustic_map_.BindCausticMap(GL_TEXTURE0);
      gauss_blur_.Blur(0);
    }

    glViewport(0, 0, g_width, g_height);

    render_frame_.BindForWriting();

    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    glBindBuffer(GL_UNIFORM_BUFFER, global_matrices_ubo_);
    glBufferSubData(GL_UNIFORM_BUFFER, 0, sizeof(glm::mat4),
        glm::value_ptr(projection_));
    glBufferSubData(GL_UNIFORM_BUFFER, sizeof(glm::mat4),
        sizeof(glm::mat4), glm::value_ptr(world_pos_));
    glBindBuffer(GL_UNIFORM_BUFFER, 0);

    // shadow_map_.BindForReading(GL_TEXTURE1);
    phong_shading_->enable();
    phong_shading_->setTexture(0); // 0 is color map
    caustic_map_.BindDepthMap(GL_TEXTURE1); // Shadow map
    phong_shading_->setShadowMap(1); // 1 is shadow map
    if (isBlurOn) {
      gauss_blur_.BindForReading(GL_TEXTURE2);
    } else {
      caustic_map_.BindCausticMap(GL_TEXTURE2);
    }
    phong_shading_->setCausticMap(2);
    skybox.Bind(GL_TEXTURE3);
    phong_shading_->setEnvironmentMap(3);
    phong_shading_->setLight(light);
    phong_shading_->setLightPerspective(light_projection_ * light_pos_);
    phong_shading_->setWorldCameraPos(O);

    if (drawWireframe) {
      glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
    } else {
      glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
    }

    glBindBufferRange(GL_UNIFORM_BUFFER, GLOBAL_MATRICES_BINDING_INDEX,
        global_matrices_ubo_, 0, sizeof(glm::mat4) * 3);

    RenderSceneVisitor renderVisitor(global_matrices_ubo_);
    // Receivers
    phong_shading_->setIsCausticReceiver(true);
    renderVisitor.set_render_refractive(false);
    renderVisitor.set_render_receiver(true);
    scene_->AcceptVisitor(renderVisitor);
    glFlush();
    // Refractors
    phong_shading_->setIsCausticReceiver(false);
    renderVisitor.set_render_receiver(false);
    renderVisitor.set_render_refractive(true);
    scene_->AcceptVisitor(renderVisitor);

    // Render skybox
    glm::mat4 skybox_transform = glm::scale(glm::rotate(glm::mat4(1.0f),
                                                        glm::radians(180.0f),
                                                        glm::vec3(1.0f, 0.0f, 0.0f)),
                                            glm::vec3(10.0f, 10.0f, 10.0f));
    // Prevents the camera to exit the skybox.
    skybox_transform = glm::translate(glm::mat4(1.0f), O) * skybox_transform;

    glBindBuffer(GL_UNIFORM_BUFFER, global_matrices_ubo_);
    glBufferSubData(GL_UNIFORM_BUFFER, 2 * sizeof(glm::mat4),
        sizeof(glm::mat4), glm::value_ptr(skybox_transform));
    skybox.Render(light_projection_ * light_pos_);
    glBindBuffer(GL_UNIFORM_BUFFER, 0);

    glBindFramebuffer(GL_FRAMEBUFFER, 0);
    glFlush();
  }

  static const GLuint GLOBAL_MATRICES_BINDING_INDEX = 1;

  // Members
  Model model;
  Model plane;
  // Shadow map
  ShadowMap shadow_map_;
  CausticMap caustic_map_;
  CausticShading caustic_shading_;
  bool drawWireframe;
  bool drawReceiverPositions;
  bool drawRefractivePositions;
  bool drawRefractiveNormals;
  bool drawCausticMap;
  bool isBlurOn;
  PhongShading *phong_shading_;
  glm::vec3 O;
  glm::vec3 G;
  glm::mat4 world_pos_;
  glm::mat4 projection_;
  glm::mat4 light_pos_;
  glm::mat4 light_projection_;
  Light light;
  MeshRenderer mesh_renderer_;
  MeshRenderer plane_mesh;
  GLuint global_matrices_ubo_;
  SceneNode *scene_;
  TransformNode *model_transform_;
  int tangentIndex_;
  Skybox skybox;
  Texture plane_tex_;
  PhongMaterial *model_mat_;
  PhongMaterial *plane_mat_;
  RenderFrame render_frame_;
  GaussBlur gauss_blur_;
  Timer timer_;
  double total_delta_;
  unsigned frame_;
  GLuint frame_time_q_[6];
  VolLightShading vol_light_shading_;
  Model cube;
  MeshRenderer cube_mesh;
};

static void flushGLErrors()
{
  GLenum err = GL_NO_ERROR;
  while ((err = glGetError()) != 0) {
    switch (err) {
      case GL_INVALID_ENUM:
        fprintf(stderr, "GL_INVALID_ENUM\n");
        break;
      case GL_INVALID_VALUE:
        fprintf(stderr, "GL_INVALID_VALUE\n");
        break;
      case GL_INVALID_OPERATION:
        fprintf(stderr, "GL_INVALID_OPERATION\n");
        break;
      case GL_INVALID_FRAMEBUFFER_OPERATION:
        fprintf(stderr, "GL_INVALID_FRAMEBUFFER_OPERATION\n");
        break;
      case GL_OUT_OF_MEMORY:
        fprintf(stderr, "GL_OUT_OF_MEMORY\n");
        break;
      case GL_STACK_OVERFLOW:
        fprintf(stderr, "GL_STACK_OVERFLOW\n");
        break;
      case GL_STACK_UNDERFLOW:
        fprintf(stderr, "GL_STACK_UNDERFLOW\n");
        break;
      default:
        break;
    }
  }
}

/**
 * The main function. Opens the window and runs the application - Vjezba.
 */
int main(int argc, char **argv)
{
  const char *model_file = 0;
  int shadow_map_size = 512;

  if (argc != 3) {
    printf("Usage: %s file.obj shadow_map_size\n", argv[0]);
    model_file = "models/ring.obj";
  } else {
    model_file = argv[1];
    shadow_map_size = atoi(argv[2]);
    if (shadow_map_size <= 0) {
      shadow_map_size = 512;
    }
  }

  GLUTBackendInit(argc, argv);

  if (!GLUTBackendCreateWindow(g_width, g_height, 32, false, "Caustics")) {
    fprintf(stderr, "GLUTBackendCreateWindow returned false!");
    return 1;
  }
  Vjezba3 app(shadow_map_size);

  if (!app.Init(model_file)) {
    fprintf(stderr, "Vjezba3::Init returned false!");
    return 1;
  }
  app.run();
  return 0;
}
