#ifndef CUBEMAP_TEXTURE_H_
#define CUBEMAP_TEXTURE_H_

#include <string>

#include <GL/glew.h>

class CubemapTexture {
  public:
    CubemapTexture(const std::string &dir,
        const std::string &pos_x, const std::string &neg_x,
        const std::string &pos_y, const std::string &neg_y,
        const std::string &pos_z, const std::string &neg_z);

    ~CubemapTexture();

    bool Load();
    void Bind(GLenum tex_unit);

  private:
    static const GLenum types[6];

    std::string files[6];
    GLuint texture;
};

#endif
