#include "shadow_map.hpp"

#include <cstdio>

ShadowMap::ShadowMap(unsigned width, unsigned height)
    : width_(width), height_(height), fbo_(0), shadow_map_(0)
{
}

ShadowMap::~ShadowMap()
{
  if (fbo_) {
    glDeleteFramebuffers(1, &fbo_);
  }
  if (shadow_map_) {
    glDeleteTextures(1, &shadow_map_);
  }
}

bool ShadowMap::Init()
{
  glGenFramebuffers(1, &fbo_);
  glGenTextures(1, &shadow_map_);
  glBindTexture(GL_TEXTURE_2D, shadow_map_);
  glTexImage2D(GL_TEXTURE_2D, 0, GL_DEPTH_COMPONENT,
      width_, height_, 0, GL_DEPTH_COMPONENT, GL_FLOAT, 0);
  glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
  glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
  glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
  glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);

  glBindFramebuffer(GL_FRAMEBUFFER, fbo_);
  glFramebufferTexture2D(GL_DRAW_FRAMEBUFFER, GL_DEPTH_ATTACHMENT,
      GL_TEXTURE_2D, shadow_map_, 0);
  glDrawBuffer(GL_NONE); // don't write to color

  if (glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE) {
    fprintf(stderr, "Framebuffer is not complete!\n");
    return false;
  }

  return true;
}

void ShadowMap::BindForWriting()
{
  glBindFramebuffer(GL_DRAW_FRAMEBUFFER, fbo_);
}

void ShadowMap::BindForReading(GLenum tex_unit)
{
  glActiveTexture(tex_unit);
  glBindTexture(GL_TEXTURE_2D, shadow_map_);
}
