#include "texture_shading.hpp"

#include <stdio.h>

bool TextureShading::init()
{
    if (!Technique::init()) {
        return false;
    }

    if (!addShaderFromFile(GL_VERTEX_SHADER, "shaders/pass.vert")) {
        return false;
    }

    if (!addShaderFromFile(GL_FRAGMENT_SHADER, "shaders/texture.frag")) {
        return false;
    }

    if (!finalize()) {
        return false;
    }
    textureLocation = getUniformLocation("gTexture");
    if (textureLocation == -1) {
        fprintf(stderr, "Missing gTexture in shaders!");
        return false;
    }
    return true;
}

void TextureShading::setTexture(unsigned tex_unit)
{
  glUniform1i(textureLocation, tex_unit);
}
