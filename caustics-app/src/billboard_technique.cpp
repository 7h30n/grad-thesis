#include "billboard_technique.hpp"

BillboardTechnique::BillboardTechnique()
{
}

bool BillboardTechnique::init()
{
  if (!Technique::init()) {
      return false;
  }

  if (!addShaderFromFile(GL_VERTEX_SHADER, "shaders/basic.vert")) {
      return false;
  }
  if (!addShaderFromFile(GL_GEOMETRY_SHADER, "shaders/billboard.geom")) {
      return false;
  }
  if (!addShaderFromFile(GL_FRAGMENT_SHADER, "shaders/texture.frag")) {
      return false;
  }

  if (!finalize()) {
      return false;
  }

  cameraPosLocation = getUniformLocation("gCameraPos");

  if (cameraPosLocation == -1) {
    return false;
  }

  return true;
}

void BillboardTechnique::setCameraPos(const glm::vec3 &cameraPos)
{
  glUniform3f(cameraPosLocation, cameraPos.x, cameraPos.y, cameraPos.z);
}
