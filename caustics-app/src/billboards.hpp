#ifndef BILLBOARDS_H_
#define BILLBOARDS_H_

#include <string>

#include <glm/glm.hpp>

#include "billboard_technique.hpp"
#include "texture.hpp"

class Billboards
{
  public:
    Billboards();
    ~Billboards();

    bool init(const std::string &texFilename);
    void render(const glm::vec3 &cameraPos);

    BillboardTechnique &technique() { return technique_; }

  private:
    void createPositionBuffer();

    GLuint vb_;
    Texture *tex_;
    BillboardTechnique technique_;
};

#endif
