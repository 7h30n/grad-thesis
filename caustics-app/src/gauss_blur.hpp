#ifndef GAUSS_BLUR_HPP
#define GAUSS_BLUR_HPP

#include <GL/glew.h>

#include "technique.hpp"

class BlurShading: public Technique
{
  public:
    virtual bool init();
    void setWeights(float weights[5]) const;
    void setPass(int i) const;
    void setTexture(int tex_unit) const;
    void setWidth(int width) const;
    void setHeight(int height) const;
  
  private:
    GLint weightsLocation;
    GLint passLocation;
    GLint textureLocation;
};

class GaussBlur
{
  public:
    GaussBlur(unsigned width, unsigned height);
    ~GaussBlur();

    bool Init();
    void Blur(int tex_unit);
    void BindForReading(GLenum tex_unit);
    void RenderBlur();

  private:
    unsigned width_;
    unsigned height_;
    float sigma2_;
    float weights_[5];
    GLuint fbo_[2];
    GLuint textures_[2];
    BlurShading blur_shader_;
};

#endif
