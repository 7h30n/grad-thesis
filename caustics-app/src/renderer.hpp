/*
 * Computer Graphics course; Laboratory exercises
 *
 * Faculty of Electrical Engineering and Computing, Zagreb
 *
 * Copyright (C) 2013 Teon Banek
 *
 * This file is part of Laboratory exercises for Computer Graphics course
 * at Faculty of Electrical Engineering and Computing.
 *
 * Author(s): Teon Banek <teon.banek@fer.hr>
 * Date: 2013-10-05
 */

/*
 * The following file contains the declaration of renderer interface.
 * This class is used to render objects.
 */

#ifndef RENDERER
#define RENDERER

/**
 * Renders the objects.
 * Before using the class Init must be called.
 * Sample usage:
 *     Renderer *renderer = new ConcreteRenderer();
 *     renderer->Init();
 *     ...
 *     renderer->Render();
 *     delete renderer;
 */
class Renderer {
 public:
  virtual ~Renderer() {}

  /**
   * Binds the data to GPU. It's the clients responsibility to
   * call this once before rendering.
   */
  virtual bool Init()=0;
  /**
   * Renders the object on screen. If Init wasn't called, results are undefined.
   */
  virtual void Render()=0;
};

#endif

