#ifndef SKYBOX_TECHNIQUE_H_
#define SKYBOX_TECHNIQUE_H_

#include <glm/glm.hpp>

#include "technique.hpp"

class SkyboxTechnique : public Technique {
  public:
    virtual bool init();

    void setTextureUnit(unsigned tex_unit);
    void setLightPerspective(const glm::mat4 &light_persp);

  private:
    GLint textureLocation;
    GLint lightPerspLocation;
};

#endif
