#ifndef TECHNIQUE_H
#define TECHNIQUE_H

#include <iostream>
#include <list>
#include <string>

#include <GL/glew.h>

class Technique
{
  public:
    Technique();
    virtual ~Technique();

    virtual bool init();
    void enable() const;

    GLuint getShaderProg() const { return shaderProg; }

  protected:
    bool addShaderFromFile(GLenum shaderType, const std::string &filename);
    bool addShaderFromFile(GLenum shaderType, const char *filename);
    bool addShader(GLenum shaderType, const char *source);
    bool addShader(GLenum shaderType, std::istream &ins);
    bool finalize();
    bool link();
    bool validate();
    GLint getUniformLocation(const std::string &name) const;
    GLint getUniformLocation(const char *name) const;

  private:
    typedef std::list<GLuint> ShaderObjList;

    GLuint shaderProg;
    ShaderObjList shaderObjList;
};

#endif
