#ifndef SIMPLE_TECHNIQUE_H
#define SIMPLE_TECHNIQUE_H

#include <glm/glm.hpp>

#include "technique.hpp"

struct Light
{
    glm::vec3 color;
    float ambientK;
    glm::vec4 position;
    float diffuseK;
};

class SimpleTechnique: public Technique
{
    public:
        virtual bool init();
        void setLight(const Light &light);
        void setColor(const glm::vec3 &color) {}

    private:
        struct {
            GLint color;
            GLint ambientK;
            GLint position;
            GLint diffuseK;
        } lightLocation;
};

#endif
