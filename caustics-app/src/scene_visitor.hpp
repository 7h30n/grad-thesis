#ifndef SCENE_VISITOR_H_
#define SCENE_VISITOR_H_

class SceneNode;
class MaterialNode;
class TransformNode;
class GeoNode;

class SceneVisitor {
 public:
  virtual ~SceneVisitor() {}

  virtual void Visit(const SceneNode &node)=0;
  virtual void FinishedVisit(const SceneNode &node)=0;
  virtual void Visit(const MaterialNode &node)=0;
  virtual void FinishedVisit(const MaterialNode &node)=0;
  virtual void Visit(const TransformNode &node)=0;
  virtual void FinishedVisit(const TransformNode &node)=0;
  virtual void Visit(const GeoNode &node)=0;
  virtual void FinishedVisit(const GeoNode &node)=0;
};
#endif
