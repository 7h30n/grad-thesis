#ifndef PHONG_MATERIAL_H_
#define PHONG_MATERIAL_H_

#include <glm/glm.hpp>

#include "material.hpp"

class PhongShading;
class Texture;

class PhongMaterial : public Material
{
  public:
    PhongMaterial(PhongShading &phong);
    PhongMaterial(PhongShading &phong, const glm::vec3 &color);
    PhongMaterial(PhongShading &phong, Texture *tex);
    PhongMaterial(PhongShading &phong, Texture *tex, const glm::vec3 &color);
    virtual ~PhongMaterial() {}

    virtual void Enable() const;
    void set_specular(const glm::vec3 &color);
    void set_is_reflective(bool is_reflective);

  private:
    PhongShading &phong_;
    Texture *tex_;
    glm::vec3 color_;
    glm::vec3 specular_;
    bool is_reflective_;
};

#endif
