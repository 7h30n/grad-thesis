#include "scene_graph.hpp"

#include "renderer.hpp"

GeoNode::GeoNode(Renderer &renderer) : renderer_(renderer)
{
}

void GeoNode::AcceptVisitor(SceneVisitor &visitor)
{
  visitor.Visit(*this);
  visitor.FinishedVisit(*this);
}

Renderer &GeoNode::renderer() const
{
  return renderer_;
}

void GeoNode::set_renderer(Renderer &renderer)
{
  renderer_ = renderer;
}
