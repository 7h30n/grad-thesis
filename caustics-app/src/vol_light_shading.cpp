#include "vol_light_shading.hpp"

#include <cstdio>

bool VolLightShading::init()
{
  if (!Technique::init()) {
    return false;
  }

  if (!addShaderFromFile(GL_VERTEX_SHADER, "shaders/pass.vert")) {
    return false;
  }

  if (!addShaderFromFile(GL_FRAGMENT_SHADER, "shaders/vol_light.frag")) {
    return false;
  }

  if (!finalize()) {
    return false;
  }

  colorMapLocation = getUniformLocation("gColorMap");
  if (colorMapLocation == -1) {
    fprintf(stderr, "Missing gColorMap in shaders!");
    //return false;
  }
  posMapLocation = getUniformLocation("gPosMap");
  if (posMapLocation == -1) {
    fprintf(stderr, "Missing gPosMap in shaders!");
    //return false;
  }
  shadowMapLocation = getUniformLocation("gShadowMap");
  if (shadowMapLocation == -1) {
      fprintf(stderr, "Missing gShadowMap in shaders!");
      //return false;
  }
  cameraLightSpacePosLocation = getUniformLocation("gCameraLightSpacePos");
  if (cameraLightSpacePosLocation == -1) {
    fprintf(stderr, "Missing gCameraLightSpacePos in shaders!");
    //return false;
  }
  return true;
}

void VolLightShading::setColorMap(unsigned tex_unit)
{
  glUniform1i(colorMapLocation, tex_unit);
  glUniform1i(posMapLocation, tex_unit + 1);
  glUniform1i(shadowMapLocation, tex_unit + 2);
}

void VolLightShading::setCameraLightSpacePos(const glm::vec3 &camera)
{
  glUniform3f(cameraLightSpacePosLocation, camera.x, camera.y, camera.z);
}
