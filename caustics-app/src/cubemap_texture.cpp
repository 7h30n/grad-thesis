#include "cubemap_texture.hpp"

#include <iostream>

#include "img/image.hpp"

const GLenum CubemapTexture::types[6] = {
      GL_TEXTURE_CUBE_MAP_POSITIVE_X,
      GL_TEXTURE_CUBE_MAP_NEGATIVE_X,
      GL_TEXTURE_CUBE_MAP_NEGATIVE_Y, // Y is inverted
      GL_TEXTURE_CUBE_MAP_POSITIVE_Y,
      GL_TEXTURE_CUBE_MAP_POSITIVE_Z,
      GL_TEXTURE_CUBE_MAP_NEGATIVE_Z,
};

CubemapTexture::CubemapTexture(const std::string &dir,
      const std::string &pos_x, const std::string &neg_x,
      const std::string &pos_y, const std::string &neg_y,
      const std::string &pos_z, const std::string &neg_z)
    : texture(0)
{
  std::string::const_iterator it = dir.end();
  it--;
  std::string base_dir = (*it == '/') ? dir : dir + "/";
  files[0] = base_dir + pos_x;
  files[1] = base_dir + neg_x;
  files[2] = base_dir + pos_y;
  files[3] = base_dir + neg_y;
  files[4] = base_dir + pos_z;
  files[5] = base_dir + neg_z;
}

CubemapTexture::~CubemapTexture()
{
  if (texture) {
    glDeleteTextures(1, &texture);
  }
}

bool CubemapTexture::Load()
{
  glGenTextures(1, &texture);
  glBindTexture(GL_TEXTURE_CUBE_MAP, texture);
  for (int i = 0; i < 6; ++i) {
    img::Image *image = img::Image::Load(files[i]);
    if (!image) {
      std::cerr << "Error: Unable to load image: " << files[i] << std::endl;
      return false;
    }
    glTexImage2D(types[i], 0, GL_RGBA, image->GetWidth(), image->GetHeight(), 0,
                 GL_BGR, GL_UNSIGNED_BYTE, image->GetPixels());
    delete image;
  }
  glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
  glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
  glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
  glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
  glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_R, GL_CLAMP_TO_EDGE);
  return true;
}

void CubemapTexture::Bind(GLenum tex_unit)
{
  glActiveTexture(tex_unit);
  glBindTexture(GL_TEXTURE_CUBE_MAP, texture);
}
