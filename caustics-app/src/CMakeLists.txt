set(caustic-app_SOURCES main.cpp glut_backend.cpp model.cpp
  technique.cpp simple_technique.cpp bezier_curve.cpp mesh_renderer.cpp
  matrix4.cpp phong_shading.cpp line_renderer.cpp scene_node.cpp
  material_node.cpp geo_node.cpp render_scene_visitor.cpp
  b_spline.cpp billboard_technique.cpp texture.cpp billboards.cpp
  particle_system.cpp ps_update_technique.cpp transform_node.cpp
  shadow_map.cpp cubemap_texture.cpp skybox_technique.cpp skybox.cpp
  phong_material.cpp render_frame.cpp vol_light_shading.cpp
  texture_shading.cpp position_shading.cpp caustic_map.cpp caustic_shading.cpp
  glw/glw.cpp img/image.cpp gauss_blur.cpp)

add_executable(caustic-app ${caustic-app_SOURCES})

include_directories(${GLUT_INCLUDE_DIR} ${GLEW_INCLUDE_DIR} ${GLM_INCLUDE_DIR}
                    ${FREEIMAGE_INCLUDE_DIR})

set(ALL_LIBRARIES ${OPENGL_LIBRARIES} ${GLUT_LIBRARIES} ${GLEW_LIBRARIES}
                  ${FREEIMAGE_LIBRARY})

target_link_libraries(caustic-app ${ALL_LIBRARIES})

set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -Wall")
