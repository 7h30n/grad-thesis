#ifndef PHONG_SHADING_H
#define PHONG_SHADING_H

#include <glm/glm.hpp>

#include "technique.hpp"

struct Light;

class PhongShading: public Technique
{
  public:
    virtual bool init();
    void setLight(const Light &light) const;
    void setLightPerspective(const glm::mat4 &light_persp) const;
    void setWorldCameraPos(const glm::vec3 &camera_pos) const;
    void setColor(const glm::vec3 &color) const;
    void setSpecularColor(const glm::vec3 &color) const;
    void setShadowMap(unsigned tex_unit) const;
    void setCausticMap(unsigned tex_unit) const;
    void setTexture(unsigned tex_unit) const;
    void setEnvironmentMap(unsigned tex_unit) const;
    void setIsTextured(bool is_textured) const;
    void setIsCausticReceiver(bool is_recv) const;
    void setIsReflective(bool is_reflective) const;

  private:
    GLint lightPerspLocation;
    GLint cameraWorldPosLocation;
    GLint shadowMapLocation;
    GLint causticMapLocation;
    GLint textureLocation;
    GLint isTexturedLocation;
    GLint isCausticReceiverLocation;
    GLint isReflectiveLocation;
    GLint environmentMapLocation;

    struct
    {
        GLint color;
        GLint ambientK;
        GLint position;
        GLint diffuseK;
    } lightLocation;

    struct
    {
        GLint diffuseK;
        GLint specularK;
        GLint shininess;
    } materialLocation;
};

#endif

