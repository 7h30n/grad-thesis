#include "ps_update_technique.hpp"

PSUpdateTechnique::PSUpdateTechnique()
{
}

bool PSUpdateTechnique::init()
{
  if (!Technique::init()) {
    return false;
  }

  if (!addShaderFromFile(GL_VERTEX_SHADER, "shaders/particle.vert")) {
      return false;
  }

  if (!addShaderFromFile(GL_GEOMETRY_SHADER, "shaders/particle.geom")) {
      return false;
  }

  const GLchar* varyings[4];
  varyings[0] = "Type1";
  varyings[1] = "Position1";
  varyings[2] = "Velocity1";
  varyings[3] = "Age1";

  glTransformFeedbackVaryings(getShaderProg(), 4, varyings,
                              GL_INTERLEAVED_ATTRIBS);

  if (!finalize()) {
      return false;
  }

  particleLifetimeLocation = getUniformLocation("gParticleLifetime");
  deltaTimeLocation = getUniformLocation("gDeltaTime");
  timeLocation = getUniformLocation("gTime");
  randDirLocation = getUniformLocation("gRandDir");

  if (particleLifetimeLocation == -1 || deltaTimeLocation == -1 ||
      timeLocation == -1 || randDirLocation == -1) {
    return false;
  }

  return true;
}

void PSUpdateTechnique::setParticleLifetime(float ttl)
{
  glUniform1f(particleLifetimeLocation, ttl);
}

void PSUpdateTechnique::setDeltaTime(float dt)
{
  glUniform1f(deltaTimeLocation, dt);
}

void PSUpdateTechnique::setTime(float time)
{
  glUniform1f(timeLocation, time);
}

void PSUpdateTechnique::setRandDir(const glm::vec3 &dir)
{
  glUniform3f(randDirLocation, dir.x, dir.y, dir.z);
}
