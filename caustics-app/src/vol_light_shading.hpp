#ifndef VOL_LIGHT_SHADING_H_
#define VOL_LIGHT_SHADING_H_

#include <glm/glm.hpp>

#include "technique.hpp"

class VolLightShading : public Technique {
  public:
    virtual bool init();
    void setColorMap(unsigned tex_unit);
    void setCameraLightSpacePos(const glm::vec3 &camera);

  private:
    GLint colorMapLocation;
    GLint shadowMapLocation;
    GLint posMapLocation;
    GLint cameraLightSpacePosLocation;
};

#endif
