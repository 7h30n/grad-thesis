#include "phong_shading.hpp"

#include <stdio.h>
#include <math.h>

#include <glm/gtc/type_ptr.hpp>
#include <glm/gtc/matrix_transform.hpp>

#include "simple_technique.hpp"

bool PhongShading::init()
{
    if (!Technique::init()) {
        return false;
    }

    if (!addShaderFromFile(GL_VERTEX_SHADER, "shaders/phong.vert")) {
        return false;
    }
    if (!addShaderFromFile(GL_FRAGMENT_SHADER, "shaders/phong.frag")) {
        return false;
    }

    if (!link()) {
        return false;
    }

    cameraWorldPosLocation = getUniformLocation("gCameraWorldPos");
    if (cameraWorldPosLocation == -1) {
      return false;
    }
    lightLocation.color = getUniformLocation("gLight.color");
    if (lightLocation.color == -1) {
        return false;
    }
    lightLocation.ambientK = getUniformLocation("gLight.ambientK");
    if (lightLocation.ambientK == -1) {
        return false;
    }
    lightLocation.position = getUniformLocation("gLight.position");
    if (lightLocation.position == -1) {
        return false;
    }
    lightLocation.diffuseK = getUniformLocation("gLight.diffuseK");
    if (lightLocation.diffuseK == -1) {
        return false;
    }
    lightPerspLocation = getUniformLocation("gLightPersp");
    if (lightPerspLocation == -1) {
        return false;
    }
    shadowMapLocation = getUniformLocation("gShadowMap");
    if (shadowMapLocation == -1) {
        return false;
    }
    causticMapLocation = getUniformLocation("gCausticMap");
    if (causticMapLocation == -1) {
        return false;
    }
    textureLocation = getUniformLocation("gTexture");
    if (textureLocation == -1) {
        return false;
    }
    environmentMapLocation = getUniformLocation("gEnvironmentMap");
    if (environmentMapLocation == -1) {
      return false;
    }
    enable();
    setTexture(1);
    setShadowMap(2);
    setCausticMap(3);
    setEnvironmentMap(4);
    
    isTexturedLocation = getUniformLocation("isTextured");
    if (isTexturedLocation == -1) {
        return false;
    }

    isCausticReceiverLocation = getUniformLocation("isCausticReceiver");
    if (isCausticReceiverLocation == -1) {
        return false;
    }

    isReflectiveLocation = getUniformLocation("isReflective");
    if (isReflectiveLocation == -1) {
      return false;
    }

    materialLocation.diffuseK = getUniformLocation("material.diffuseK");
    if (materialLocation.diffuseK == -1) {
        return false;
    }
    materialLocation.specularK = getUniformLocation("material.specularK");
    if (materialLocation.specularK == -1) {
        return false;
    }
    materialLocation.shininess = getUniformLocation("material.shininess");
    if (materialLocation.shininess == -1) {
        return false;
    }

    return validate();
}

void PhongShading::setLight(const Light &light) const
{
    glUniform3f(lightLocation.color, light.color.x, light.color.y,
                light.color.z);
    glUniform1f(lightLocation.ambientK, light.ambientK);
    glm::vec4 pos = glm::normalize(light.position);
    glUniform4f(lightLocation.position, pos.x, pos.y, pos.z, pos.w);
    glUniform1f(lightLocation.diffuseK, light.diffuseK);
}

void PhongShading::setWorldCameraPos(const glm::vec3 &camera_pos) const
{
  glUniform3f(cameraWorldPosLocation, camera_pos.x, camera_pos.y, camera_pos.z);
}

void PhongShading::setLightPerspective(const glm::mat4 &light_persp) const
{
  glUniformMatrix4fv(lightPerspLocation, 1,
      GL_FALSE, glm::value_ptr(light_persp));
}

void PhongShading::setColor(const glm::vec3 &color) const
{
    glUniform3f(materialLocation.diffuseK, color.x, color.y, color.z);
}

void PhongShading::setSpecularColor(const glm::vec3 &color) const
{
    glUniform3f(materialLocation.specularK, color.x, color.y, color.z);
}

void PhongShading::setShadowMap(unsigned tex_unit) const
{
  glUniform1i(shadowMapLocation, tex_unit);
}

void PhongShading::setCausticMap(unsigned tex_unit) const
{
  glUniform1i(causticMapLocation, tex_unit);
}

void PhongShading::setTexture(unsigned tex_unit) const
{
  glUniform1i(textureLocation, tex_unit);
}

void PhongShading::setEnvironmentMap(unsigned tex_unit) const
{
  glUniform1i(environmentMapLocation, tex_unit);
}

void PhongShading::setIsTextured(bool is_textured) const
{
  glUniform1i(isTexturedLocation, is_textured ? 1 : 0);
}

void PhongShading::setIsCausticReceiver(bool is_recv) const
{
  glUniform1i(isCausticReceiverLocation, is_recv ? 1 : 0);
}

void PhongShading::setIsReflective(bool is_reflective) const
{
  glUniform1i(isReflectiveLocation, is_reflective ? 1 : 0);
}
