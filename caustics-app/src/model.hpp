#ifndef MODEL_H
#define MODEL_H

#include <stdio.h>
#include <vector>

#include <GL/glew.h>
#include <glm/glm.hpp>

struct Face
{
    unsigned index[3];
    unsigned uv_index[3];
    unsigned vn_index[3];
    glm::vec4 plane;
};

class Model {
  public:
    static Model Plane(); // Constructs basic plane mesh (hardcoded square)
    static Model Cube(); // Constructs basic cube mesh (from file)

    bool readObjFile(const char *name);
    bool insideConvexBody(glm::vec3 point);
    std::vector<unsigned> getVisibleIndices(glm::vec3 O);

    const std::vector<glm::vec3> &vertices() const { return vertices_; }
    const std::vector<Face> &faces() const { return faces_; }
    const std::vector<glm::vec2> &uvs() const { return uvs_; }
    const std::vector<glm::vec3> &normals() const { return normals_; }

  private:
    std::vector<glm::vec3> vertices_;
    std::vector<Face> faces_;
    std::vector<glm::vec2> uvs_;
    std::vector<glm::vec3> normals_;

    bool isVisibleFace(Face f, glm::vec3 O);
    bool readVertex(FILE *file);
    bool readFace(FILE *file);
    bool readFaceIndex(FILE *file, unsigned i, Face &f);
    bool ignoreLine(FILE *file);
    void calculatePlane(Face &face);
    void normalize();
};
#endif
