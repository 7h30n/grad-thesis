#include "glw.hpp"

namespace glw
{

VertexAttrib::VertexAttrib(GLuint index) : index_(index)
{
}

void VertexAttrib::Disable()
{
  glDisableVertexAttribArray(index_);
}

void VertexAttrib::Pointer(GLint size, GLenum type, GLboolean normalize,
                           GLsizei stride, const GLvoid *offset)
{
  glEnableVertexAttribArray(index_);
  glVertexAttribPointer(index_, size, type, normalize, stride, offset);
}

void VertexAttrib::Pointer(GLint size, GLenum type, GLboolean normalize,
                           GLsizei stride, const GLvoid *offset, GLuint buffer)
{
  glBindBuffer(GL_ARRAY_BUFFER, buffer);
  Pointer(size, type, normalize, stride, offset);
  glBindBuffer(GL_ARRAY_BUFFER, 0);
}

BoundVertexArray::BoundVertexArray(GLuint array)
{
  glBindVertexArray(array);
}

BoundVertexArray::~BoundVertexArray()
{
  glBindVertexArray(0);
}

}; // namespace glw
