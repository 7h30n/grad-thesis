#ifndef GLW_GLW_H
#define GLW_GLW_H

#include <GL/glew.h>

namespace glw {

class VertexAttrib
{
  public:
    VertexAttrib(GLuint index);
    void Disable();
    void Pointer(GLint size, GLenum type, GLboolean normalize, GLsizei stride,
                 const GLvoid *offset);
    void Pointer(GLint size, GLenum type, GLboolean normalize, GLsizei stride,
                 const GLvoid *offset, GLuint buffer);

  private:
    GLuint index_;
};

class BoundVertexArray
{
  public:
    BoundVertexArray(GLuint array);
    ~BoundVertexArray();

  private:
    BoundVertexArray(const BoundVertexArray &);
    BoundVertexArray &operator=(const BoundVertexArray &);
};

template<typename T>
T *MapBuffer(GLenum target, GLenum access)
{
  return static_cast<T*>(glMapBuffer(target, access));
}

template<typename T>
T *MapBufferRange(GLenum target, GLintptr offset, GLsizeiptr length,
                  GLbitfield access)
{
  return static_cast<T*>(glMapBufferRange(target, offset, length, access));
}

}; // namespace glw

#endif
