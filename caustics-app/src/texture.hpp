#ifndef TEXTURE_H
#define TEXTURE_H

#include <string>

#include <GL/glew.h>

namespace img
{
class Image;
}

class Texture
{
    public:
        Texture(GLenum textureTarget, const std::string &fileName);
        ~Texture();

        bool load();

        void bind(GLenum textureUnit);

    private:
        std::string fileName;
        GLenum textureTarget;
        GLuint textureObj;
        img::Image *image;
};

#endif
