#include <cstring>
#include <fstream>
#include <vector>

#include "technique.hpp"

Technique::Technique()
{
  shaderProg = 0;
}

Technique::~Technique()
{
  for (ShaderObjList::iterator it = shaderObjList.begin();
       it != shaderObjList.end(); it++) {
    glDeleteShader(*it);
  }

  if (shaderProg != 0) {
    glDeleteProgram(shaderProg);
    shaderProg = 0;
  }
}

bool Technique::init()
{
  shaderProg = glCreateProgram();
  if (shaderProg == 0) {
    std::cerr << "Error: Unable to create shader program" << std::endl;
    return false;
  }
  return true;
}

bool Technique::addShaderFromFile(GLenum shaderType, const std::string &filename)
{
  return addShaderFromFile(shaderType, filename.c_str());
}

bool Technique::addShaderFromFile(GLenum shaderType, const char *filename)
{
  std::ifstream file(filename);
  if (file.fail()) {
    std::cerr << "Error: Unable to open " << filename << std::endl;
    return false;
  }
  if (!addShader(shaderType, file)) {
    std::cerr << "Error: In shader from file " << filename << std::endl;
    return false;
  }
  return true;
}

bool Technique::addShader(GLenum shaderType, std::istream &ins)
{
  std::vector<char> text;
  int c = 0;
  while ((c = ins.get()) != EOF) {
    text.push_back(c);
  }
  text.push_back(0);
  return addShader(shaderType, &text[0]);
}

bool Technique::addShader(GLenum shaderType, const char *shaderText)
{
  GLuint shaderObj = glCreateShader(shaderType);

  if (shaderObj == 0) {
    std::cerr << "Error: Unable to create shader type " << shaderType
              << std::endl;
    return false;
  }

  const GLchar *p[1];
  p[0] = shaderText;
  GLint lengths[1];
  lengths[0] = strlen(shaderText);
  glShaderSource(shaderObj, 1, p, lengths);

  glCompileShader(shaderObj);

  GLint success;
  glGetShaderiv(shaderObj, GL_COMPILE_STATUS, &success);

  if (!success) {
    GLchar infoLog[1024];
    glGetShaderInfoLog(shaderObj, 1024, NULL, infoLog);
    std::cerr << "Error: Unable to compile shader of type " << shaderType
              << ": " << infoLog << std::endl;
    glDeleteShader(shaderObj);
    return false;
  }

  glAttachShader(shaderProg, shaderObj);

  shaderObjList.push_back(shaderObj);

  return true;
}

bool Technique::finalize()
{
  if (!link()) {
    return false;
  }
  return validate();
}

bool Technique::link()
{
  GLint success = 0;
  GLchar errorLog[1024] = {0};

  glLinkProgram(shaderProg);
  glGetProgramiv(shaderProg, GL_LINK_STATUS, &success);
  if (success == 0) {
    glGetProgramInfoLog(shaderProg, sizeof(errorLog), NULL, errorLog);
    std::cerr << "Error: Unable to link shader program: " << errorLog
              << std::endl;
    return false;
  }

  for (ShaderObjList::iterator it = shaderObjList.begin();
       it != shaderObjList.end(); it++) {
    glDetachShader(shaderProg, *it);
    glDeleteShader(*it);
  }

  shaderObjList.clear();
  return true;
}

bool Technique::validate()
{
  GLint success = 0;
  GLchar errorLog[1024] = {0};

  glValidateProgram(shaderProg);
  glGetProgramiv(shaderProg, GL_VALIDATE_STATUS, &success);
  if (success == 0) {
    glGetProgramInfoLog(shaderProg, sizeof(errorLog), NULL, errorLog);
    std::cerr << "Error: Invalid shader program: " << errorLog << std::endl;
    return false;
  }
  return true;
}

void Technique::enable() const
{
  glUseProgram(shaderProg);
}

GLint Technique::getUniformLocation(const std::string &name) const
{
  return getUniformLocation(name.c_str());
}

GLint Technique::getUniformLocation(const char *uniformName) const
{
  GLint location = glGetUniformLocation(shaderProg, uniformName);

  if (location == -1) {
    std::cerr << "Warning! Unable to get the location of uniform "
              << uniformName << std::endl;
  }

  return location;
}
