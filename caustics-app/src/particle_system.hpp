#ifndef PARTICLE_SYSTEM_H_
#define PARTICLE_SYSTEM_H_

#include <glm/glm.hpp>

#include "billboard_technique.hpp"
#include "ps_update_technique.hpp"
#include "texture.hpp"

class ParticleSystem
{
  public:
    ParticleSystem();
    ~ParticleSystem();

    BillboardTechnique &billboardTechnique() { return billboardTechnique_; }
    bool init(const glm::vec3 &pos);
    void render(int dt, const glm::vec3 &cameraPos);

  private:
    static const int MAX_PARTICLES = 100000;
    static const float PARTICLE_TYPE_LAUNCHER;
    static const float PARTICLE_TYPE_EFFECT;
    static const float PARTICLE_LIFETIME;

    void updateParticles(int dt);
    void renderParticles(const glm::vec3 &cameraPos);

    bool isFirst;
    unsigned currVB;
    unsigned currTFB;
    GLuint particleBuffer[2];
    GLuint transformFeedback[2];
    PSUpdateTechnique updateTechnique;
    BillboardTechnique billboardTechnique_;
    Texture *tex;
    int time_;
};

const float ParticleSystem::PARTICLE_TYPE_LAUNCHER = 0.0f;
const float ParticleSystem::PARTICLE_TYPE_EFFECT = 1.0f;
const float ParticleSystem::PARTICLE_LIFETIME = 5000.0f;

#endif
