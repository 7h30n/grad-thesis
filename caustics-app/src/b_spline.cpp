/*
 * Computer Graphics course; Laboratory exercises
 *
 * Faculty of Electrical Engineering and Computing, Zagreb
 *
 * Copyright (C) 2013 Teon Banek
 *
 * This file is part of Laboratory exercises for Computer Graphics course
 * at Faculty of Electrical Engineering and Computing.
 *
 * Author(s): Teon Banek <teon.banek@fer.hr>
 * Date: 2013-10-05
 */

#include "b_spline.hpp"

#include <fstream>
#include <iostream>

bool BSpline::readFile(const char *filename)
{
  std::ifstream file(filename);
  while (!file.eof()) {
    float x, y, z;
    file >> x;
    if (file.eof()) {
      break;
    }
    if (file.fail()) {
      std::cerr << "Bad file: " << filename << std::endl;
      return false;
    }
    file >> y;
    if (file.fail() || file.eof()) {
      std::cerr << "Bad file: " << filename << std::endl;
      return false;
    }
    file >> z;
    if (file.fail() || file.eof()) {
      std::cerr << "Bad file: " << filename << std::endl;
      return false;
    }
    controlPoints.push_back(glm::vec3(x, y, z));
  }
  file.close();
  calculate();
  return true;
}

const std::vector<glm::vec3> &BSpline::getPoints() const
{
  return points;
}

const std::vector<glm::vec3> &BSpline::getControlPoints() const
{
  return controlPoints;
}

const std::vector<glm::vec3> &BSpline::tangents() const
{
  return tangents_;
}

void BSpline::calculate()
{
  for (size_t i = 1; i + 2 < controlPoints.size(); ++i) {
    for (float t = 0.0f; t < 1.0f; t += 0.01f) {
      glm::vec3 p = calculatePoint(t, i);
      points.push_back(p);
      glm::vec3 tan = calculateTangent(t, i);
      tangents_.push_back(glm::normalize(tan));
    }
  }
}

glm::vec3 BSpline::calculatePoint(float t, size_t i)
{
  glm::mat4 b(-1.0f, 3.0f, -3.0f, 1.0f,
               3.0f, -6.0f, 3.0f, 0.0f,
              -3.0f, 0.0f, 3.0f, 0.0f,
               1.0f, 4.0f, 1.0f, 0.0f);

  glm::vec4 t3(t * t * t, t * t, t, 1.0f);

  glm::vec4 r_i0(controlPoints[i - 1], 0.0f);
  glm::vec4 r_i1(controlPoints[i], 0.0f);
  glm::vec4 r_i2(controlPoints[i + 1], 0.0f);
  glm::vec4 r_i3(controlPoints[i + 2], 0.0f);

  glm::mat4 r(r_i0, r_i1, r_i2, r_i3);
  glm::vec4 res = r * b / 6.0f * t3;
  return glm::vec3(res.x, res.y, res.z);
}

glm::vec3 BSpline::calculateTangent(float t, size_t i)
{
  glm::mat4 b(-1.0f, 3.0f, -3.0f, 1.0f,
               2.0f, -4.0f, 2.0f, 0.0f,
              -1.0f, 0.0f, 1.0f, 0.0f,
               0.0f, 0.0f, 0.0f, 0.0f);

  glm::vec4 t2(t * t, t, 1.0f, 0.0f);

  glm::vec4 r_i0(controlPoints[i - 1], 0.0f);
  glm::vec4 r_i1(controlPoints[i], 0.0f);
  glm::vec4 r_i2(controlPoints[i + 1], 0.0f);
  glm::vec4 r_i3(controlPoints[i + 2], 0.0f);

  glm::mat4 r(r_i0, r_i1, r_i2, r_i3);
  glm::vec4 res = r * b / 2.0f * t2;
  return glm::vec3(res.x, res.y, res.z);
}
