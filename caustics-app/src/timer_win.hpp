#ifdef _WIN32
#define WIN32_LEAN_AND_MEAN
#include <windows.h>

// NOTE: Uses LARGE_INTEGER.QuadPart which needs 64bit integer arithmetic
// support from compiler. Tested only with VC++ 2012.
class Timer {
  public:
    Timer()
    {
      start_.QuadPart = 0;
      last_.QuadPart = 0;
      Start();
    }

    void Start()
    {
      QueryPerformanceCounter(&start_);
    }

    // Returns time since last check in seconds.
    double GetDeltaTime()
    {
      LARGE_INTEGER freq;
      LARGE_INTEGER elapsed;
      QueryPerformanceFrequency(&freq);
      last_ = start_;
      QueryPerformanceCounter(&start_);
      // Multiplying here saves precision.
      elapsed.QuadPart = (start_.QuadPart - last_.QuadPart) * 1e6;
      elapsed.QuadPart /= freq.QuadPart;
      return elapsed.QuadPart * 1e-6;
    }

  private:
    // Track times in microseconds.
    LARGE_INTEGER start_;
    LARGE_INTEGER last_;
};
#endif // _WIN32
