#include "render_scene_visitor.hpp"

#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>

#include "renderer.hpp"
#include "scene_graph.hpp"
#include "technique.hpp"

RenderSceneVisitor::RenderSceneVisitor(unsigned matrices_ubo)
    : global_matrices_ubo_(matrices_ubo), use_material_(true),
      render_refractive_(true), render_receiver_(true)
{
  matrix_stack_.push_back(glm::mat4(1.0f));
}

RenderSceneVisitor::RenderSceneVisitor(unsigned matrices_ubo, bool mat)
    : global_matrices_ubo_(matrices_ubo), use_material_(mat),
      render_refractive_(true), render_receiver_(true)
{
  matrix_stack_.push_back(glm::mat4(1.0f));
}

void RenderSceneVisitor::Visit(const SceneNode &node)
{
}

void RenderSceneVisitor::Visit(const MaterialNode &node)
{
  material_stack_.push_back(&node);
}

void RenderSceneVisitor::FinishedVisit(const MaterialNode &node)
{
  material_stack_.pop_back();
}

void RenderSceneVisitor::Visit(const TransformNode &node)
{
  const glm::mat4 &mat = node.transform();
  if (matrix_stack_.empty()) {
    matrix_stack_.push_back(mat);
  } else {
    glm::mat4 trans = matrix_stack_.back() * mat;
    matrix_stack_.push_back(trans);
  }
}

void RenderSceneVisitor::FinishedVisit(const TransformNode &node)
{
  matrix_stack_.pop_back();
}

void RenderSceneVisitor::Visit(const GeoNode &node)
{
  if (!material_stack_.empty()) {
    const MaterialNode &material = *material_stack_.back();
    if (material.is_refractive() && !render_refractive_) {
      return;
    }
    if (!material.is_refractive() && !render_receiver_) {
      return;
    }
    if (use_material_) {
      material.Enable();
    }
  }
  // Constructor loads identity matrix on stack
  // so no need to check if stack is empty.
  glm::mat4 &transform = matrix_stack_.back();
  glBindBuffer(GL_UNIFORM_BUFFER, global_matrices_ubo_);
  glBufferSubData(GL_UNIFORM_BUFFER, 2 * sizeof(glm::mat4),
      sizeof(glm::mat4), glm::value_ptr(transform));
  glBindBuffer(GL_UNIFORM_BUFFER, 0);

  node.renderer().Render();
}
