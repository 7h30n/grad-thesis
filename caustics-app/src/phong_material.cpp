#include "phong_material.hpp"

#include "phong_shading.hpp"
#include "texture.hpp"

PhongMaterial::PhongMaterial(PhongShading &phong)
    : phong_(phong),
      tex_(0),
      color_(1.0f, 1.0f, 1.0f),
      specular_(0.0f, 0.0f, 0.0f),
      is_reflective_(false)
{
}

PhongMaterial::PhongMaterial(PhongShading &phong, const glm::vec3 &color)
    : phong_(phong),
      tex_(0),
      color_(color),
      specular_(0.0f, 0.0f, 0.0f),
      is_reflective_(false)
{
}

PhongMaterial::PhongMaterial(PhongShading &phong, Texture *tex)
    : phong_(phong),
      tex_(tex),
      color_(1.0f, 1.0f, 1.0f),
      specular_(0.0f, 0.0f, 0.0f),
      is_reflective_(false)
{
}

PhongMaterial::PhongMaterial(
    PhongShading &phong, Texture *tex, const glm::vec3 &color)
    : phong_(phong),
      tex_(tex),
      color_(color),
      specular_(0.0f, 0.0f, 0.0f),
      is_reflective_(false)
{
}

void PhongMaterial::Enable() const
{
  phong_.enable();
  phong_.setColor(color_);
  phong_.setSpecularColor(specular_);
  phong_.setIsReflective(is_reflective_);
  if (tex_) {
    tex_->bind(GL_TEXTURE0);
    phong_.setTexture(0);
    phong_.setIsTextured(true);
  } else {
    phong_.setIsTextured(false);
  }
}
void PhongMaterial::set_specular(const glm::vec3 &color)
{
  specular_= color;
}

void PhongMaterial::set_is_reflective(bool is_reflective)
{
  is_reflective_ = is_reflective;
}
