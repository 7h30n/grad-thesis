#include "texture.hpp"

#include <iostream>

#include "img/image.hpp"

Texture::Texture(GLenum textureTarget, const std::string &fileName)
{
    this->textureTarget = textureTarget;
    this->fileName = fileName;
    image = NULL;
}

Texture::~Texture()
{
  if (image) {
    delete image;
  }
}

bool Texture::load()
{
    image = img::Image::Load(fileName);
    if (!image) {
      std::cerr << "Error: Unable to load image: " << fileName << std::endl;
      return false;
    }

    glGenTextures(1, &textureObj);
    glBindTexture(textureTarget, textureObj);
    glTexImage2D(textureTarget, 0, GL_RGB, image->GetWidth(), image->GetHeight(),
                    0, GL_BGR, GL_UNSIGNED_BYTE, image->GetPixels());
    glTexParameterf(textureTarget, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameterf(textureTarget, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

    return true;
}

void Texture::bind(GLenum textureUnit)
{
    glActiveTexture(textureUnit);
    glBindTexture(textureTarget, textureObj);
}
