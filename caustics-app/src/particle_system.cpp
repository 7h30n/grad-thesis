#include "particle_system.hpp"

#include <cstdlib>
#include <ctime>
#include <vector>

struct Particle
{
  float type;
  glm::vec3 pos;
  glm::vec3 vel;
  float lifetime;
};

ParticleSystem::ParticleSystem()
    : isFirst(true), currVB(0), currTFB(1), tex(0), time_(0)
{
  for (int i = 0; i < 2; ++i) {
    particleBuffer[i] = 0;
    transformFeedback[i] = 0;
  }
}

ParticleSystem::~ParticleSystem()
{
  if (tex) {
    delete tex;
  }
  glDeleteTransformFeedbacks(2, transformFeedback);
  glDeleteBuffers(2, particleBuffer);
}

bool ParticleSystem::init(const glm::vec3 &pos)
{
  srand(time(0));
  Particle base;
  base.type = 0.0f;
  base.pos = glm::vec3(0.0f, 0.0f, 0.0f);
  base.vel = glm::vec3(0.0f, 0.0f, 0.0f);
  base.lifetime = 0.0f;
  std::vector<Particle> particles(MAX_PARTICLES, base);
  particles[0].type = PARTICLE_TYPE_LAUNCHER;
  particles[0].pos = pos;

  glGenTransformFeedbacks(2, transformFeedback);
  glGenBuffers(2, particleBuffer);

  for (int i = 0; i < 2; ++i) {
    glBindTransformFeedback(GL_TRANSFORM_FEEDBACK, transformFeedback[i]);
    glBindBuffer(GL_ARRAY_BUFFER, particleBuffer[i]);
    glBufferData(GL_ARRAY_BUFFER, sizeof(Particle) * MAX_PARTICLES,
                 &particles[0], GL_DYNAMIC_DRAW);
    glBindBufferBase(GL_TRANSFORM_FEEDBACK_BUFFER, 0, particleBuffer[i]);
  }

  if (!updateTechnique.init()) {
    return false;
  }
  updateTechnique.enable();
  updateTechnique.setParticleLifetime(PARTICLE_LIFETIME);

  if (!billboardTechnique_.init()) {
    return false;
  }
  billboardTechnique_.enable();

  tex = new Texture(GL_TEXTURE_2D, "textures/particles/snow.bmp");

  if (!tex->load()) {
    return false;
  }

  return true;
}

void ParticleSystem::render(int dt, const glm::vec3 &cameraPos)
{
  time_ += dt;
  updateParticles(dt);
  renderParticles(cameraPos);
  currVB = currTFB;
  currTFB = (currTFB + 1) % 2;
}

void ParticleSystem::updateParticles(int dt)
{
  updateTechnique.enable();
  updateTechnique.setTime(time_);
  updateTechnique.setDeltaTime(dt);
  float rx = rand() % 101 * 0.1f - 5.0f;
  float ry = rand() % 101 * 0.1f - 5.0f;
  //float rz = rand() % 101 * 0.1f - 5.0f;
  float rz = 0.0f;
  glm::vec3 dir(rx, ry, rz);
  updateTechnique.setRandDir(dir);

  glEnable(GL_RASTERIZER_DISCARD);

  glBindBuffer(GL_ARRAY_BUFFER, particleBuffer[currVB]);
  glBindTransformFeedback(GL_TRANSFORM_FEEDBACK, transformFeedback[currTFB]);

  glEnableVertexAttribArray(0);
  glEnableVertexAttribArray(1);
  glEnableVertexAttribArray(2);
  glEnableVertexAttribArray(3);

  // type
  glVertexAttribPointer(0, 1, GL_FLOAT, GL_FALSE,
                        sizeof(Particle), 0);
  // position
  glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE,
                        sizeof(Particle), (const GLvoid*)4);
  // velocity
  glVertexAttribPointer(2, 3, GL_FLOAT, GL_FALSE,
                        sizeof(Particle), (const GLvoid*)16);
  // lifetime
  glVertexAttribPointer(3, 1, GL_FLOAT, GL_FALSE,
                        sizeof(Particle), (const GLvoid*)28);

  glBeginTransformFeedback(GL_POINTS);

  if (isFirst) {
      glDrawArrays(GL_POINTS, 0, 1);
      isFirst = false;
  } else {
      glDrawTransformFeedback(GL_POINTS, transformFeedback[currVB]);
  }

  glEndTransformFeedback();

  glDisableVertexAttribArray(0);
  glDisableVertexAttribArray(1);
  glDisableVertexAttribArray(2);
  glDisableVertexAttribArray(3);

  glDisable(GL_RASTERIZER_DISCARD);
}

void ParticleSystem::renderParticles(const glm::vec3 &cameraPos)
{
  billboardTechnique_.enable();
  billboardTechnique_.setCameraPos(cameraPos);
  tex->bind(GL_TEXTURE0);

  glBindBuffer(GL_ARRAY_BUFFER, particleBuffer[currTFB]);

  glEnableVertexAttribArray(0);

  // position
  glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE,
                        sizeof(Particle), (const GLvoid*)4);

  glDrawTransformFeedback(GL_POINTS, transformFeedback[currTFB]);

  glDisableVertexAttribArray(0);
}
