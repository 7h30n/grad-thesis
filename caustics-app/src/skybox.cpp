#include "skybox.hpp"

#include "cubemap_texture.hpp"
#include "skybox_technique.hpp"

Skybox::Skybox()
  : skybox_tech_(0),
    cubemap_tex_(0),
    cube_(Model::Cube()),
    cube_mesh_(cube_)
{
}

Skybox::~Skybox()
{
  if (skybox_tech_) {
    delete skybox_tech_;
  }
  if (cubemap_tex_) {
    delete cubemap_tex_;
  }
}

bool Skybox::Init(const std::string &dir,
    const std::string &pos_x, const std::string &neg_x,
    const std::string &pos_y, const std::string &neg_y,
    const std::string &pos_z, const std::string &neg_z,
    unsigned global_binding_index)
{
  skybox_tech_ = new SkyboxTechnique();
  if (!skybox_tech_->init()) {
    return false;
  }
  GLuint global_uniform_block_index = glGetUniformBlockIndex(
          skybox_tech_->getShaderProg(), "GlobalMatrices");
  glUniformBlockBinding(skybox_tech_->getShaderProg(),
          global_uniform_block_index, global_binding_index);

  skybox_tech_->enable();
  skybox_tech_->setTextureUnit(2);
  cubemap_tex_ =
    new CubemapTexture(dir, pos_x, neg_x, pos_y, neg_y, pos_z, neg_z);
  if (!cubemap_tex_->Load()) {
    return false;
  }

  if (!cube_mesh_.Init()) {
    return false;
  }
  return true;
}

void Skybox::Render(const glm::mat4 &light_persp)
{
  skybox_tech_->enable();
  skybox_tech_->setLightPerspective(light_persp);
  GLint old_cull;
  glGetIntegerv(GL_CULL_FACE_MODE, &old_cull);
  GLint old_depth;
  glGetIntegerv(GL_DEPTH_FUNC, &old_depth);

  glCullFace(GL_FRONT);
  glDepthFunc(GL_LEQUAL);
  cubemap_tex_->Bind(GL_TEXTURE2);
  cube_mesh_.Render();

  glCullFace(old_cull);
  glDepthFunc(old_depth);
}

void Skybox::Bind(GLenum tex_unit) const
{
  cubemap_tex_->Bind(tex_unit);
}
