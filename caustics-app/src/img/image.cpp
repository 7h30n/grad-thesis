#include "image.hpp"

#include <FreeImage.h>

namespace img
{

struct Image::ImageImpl
{
  ImageImpl(FIBITMAP *img) : image(img) {}
  ~ImageImpl()
  {
    if (image) {
      FreeImage_Unload(image);
    }
  }
  FIBITMAP *image;
};

static FIBITMAP *loadImage(const char *path)
{
  FREE_IMAGE_FORMAT fmt = FreeImage_GetFileType(path, 0);
  if (fmt == FIF_UNKNOWN) {
    fmt = FreeImage_GetFIFFromFilename(path);
  }
  FIBITMAP *bitmap = NULL;
  if (fmt != FIF_UNKNOWN && FreeImage_FIFSupportsReading(fmt)) {
    bitmap = FreeImage_Load(fmt, path, 0);
  }
  return bitmap;
}

Image::Image(ImageImpl *impl) : impl_(impl)
{
}

Image::~Image()
{
  if (impl_) {
    delete impl_;
  }
}

Image *Image::Load(const std::string &path)
{
  FIBITMAP* image = loadImage(path.c_str());
  if (image) {
    Image::ImageImpl *impl = new Image::ImageImpl(image);
    return new Image(impl);
  }
  return NULL;
}

unsigned char *Image::GetPixels() const { return FreeImage_GetBits(impl_->image); }
unsigned Image::GetWidth() const { return FreeImage_GetWidth(impl_->image); }
unsigned Image::GetHeight() const { return FreeImage_GetHeight(impl_->image); }

} // namespace img
