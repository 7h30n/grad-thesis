#ifndef IMG_IMAGE_H
#define IMG_IMAGE_H

#include <string>

namespace img
{

class Image
{
  public:
    // Returns NULL on failure.
    static Image *Load(const std::string &path);
    ~Image();

    unsigned char *GetPixels() const;
    unsigned GetWidth() const;
    unsigned GetHeight() const;

  private:
    struct ImageImpl;
    Image(ImageImpl *impl);
    Image(const Image&);
    Image &operator=(const Image&);

    ImageImpl *impl_;
};

} // namespace img

#endif
