#include "skybox_technique.hpp"

#include <cstdio>
#include <glm/gtc/type_ptr.hpp>

bool SkyboxTechnique::init()
{
  if (!Technique::init()) {
    return false;
  }

  if (!addShaderFromFile(GL_VERTEX_SHADER, "shaders/skybox.vert")) {
      return false;
  }

  if (!addShaderFromFile(GL_FRAGMENT_SHADER, "shaders/skybox.frag")) {
      return false;
  }

  if (!finalize()) {
      return false;
  }

  textureLocation = getUniformLocation("gTexture");
  if (textureLocation == -1) {
      fprintf(stderr, "Missing gTexture in shaders!");
      return false;
  }
  lightPerspLocation = getUniformLocation("gLightPersp");
  if (lightPerspLocation == -1) {
      fprintf(stderr, "Missing gLightPersp in shaders!");
      return false;
  }
  return true;
}

void SkyboxTechnique::setTextureUnit(unsigned tex_unit)
{
  glUniform1i(textureLocation, tex_unit);
}

void SkyboxTechnique::setLightPerspective(const glm::mat4 &light_persp)
{
  glUniformMatrix4fv(lightPerspLocation, 1,
      GL_FALSE, glm::value_ptr(light_persp));
}
