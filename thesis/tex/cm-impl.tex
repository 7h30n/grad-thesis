\chapter{Implementacija mapiranja kaustike}
Za potrebe implementacije ovog algoritma korišten je programski jezik C++ i
OpenGL biblioteka za ostvarenje 3D grafike. Programi za sjenčanje koji se
izvršavaju na grafičkom procesoru su napisani pomoću jezika GLSL. Poslije
samog opisa implementacije, dana je usporedba rezultate s primjerom praćenja
fotona napravljenom pomoću Nvidia OptiX programskog sučelja.

\section{Opis programskog koda}
Algoritam mapiranja kaustike je vrlo jednostavan ako se gleda s visoke
razine. Prva tri koraka pseudokoda \ref{alg:cm-pseudo} prenesena u
implementacijski jezik C++ su dana u nastavku.
\lstset{language=C++, tabsize=2, frame=leftline, columns=fixed,
  basicstyle=\small}
\begin{lstlisting}
    // Receivers (everything)
    caustic_map_.BindForWritingReceiver();
    RenderSceneVisitor rv(global_matrices_ubo_, false);
    rv.set_render_refractive(false);
    rv.set_render_receiver(true);
    scene_->AcceptVisitor(rv);
    // Refractors
    caustic_map_.BindForWritingRefractive();
    rv.set_render_receiver(false);
    rv.set_render_refractive(true);
    scene_->AcceptVisitor(rv);
    // Create caustic map
    caustic_shading_.setIsRefractive(true);
    caustic_map_.BindPositionMap(GL_TEXTURE0);
    caustic_shading_.setPositionMap(0);
    caustic_map_.WriteCausticMap();
\end{lstlisting}
Varijabla !caustic_map_! je instanca razreda !CausticMap!.
Taj razred sadrži metode za manipuliranje spremnikom slike \engl{framebuffer}
koji je povezan s teksturama u koje se pohranjuju podaci potrebni za
generiranje mape kaustike kao i sama mapa. Razred !RenderSceneVisitor! služi
za obilazak grafa scene \engl{scene graph} kako bi se pravilno iscrtala
geometrija. U varijabli !global_matrices_ubo_! je pohranjen indikator gdje se
u memoriji grafičkog procesora nalaze matrice za pretvorbu 3D pozicije modela
u koordinatne sustave svijeta scene, kamere i projekcije.

Generiranje spremnika slike pomoću OpenGL biblioteke je prikazano sljedećim
kodom.
\begin{lstlisting}
  glGenFramebuffers(1, &fbo_);
  glBindFramebuffer(GL_FRAMEBUFFER, fbo_);
\end{lstlisting}
Potom se generiraju teksture za pohranu boje, pozicije i dubinske mape. Te
teksture se povežu s generiranim spremnikom slike koristeći funkciju
!glFrameBufferTexture2D!.
\begin{lstlisting}
  glGenTextures(1, &color_map_);
  glBindTexture(GL_TEXTURE_2D, color_map_);
  glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB32F, width_, height_,
               0, GL_RGBA, GL_FLOAT, 0);
  ...
  glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0,
                         GL_TEXTURE_2D, color_map_, 0);
  glGenTextures(1, &pos_map_);
  ...
  glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT1,
                         GL_TEXTURE_2D, pos_map_, 0);
  glGenTextures(1, &depth_map_);
  glBindTexture(GL_TEXTURE_2D, depth_map_);
  glTexImage2D(GL_TEXTURE_2D, 0, GL_DEPTH_COMPONENT, width_,
               height_, 0, GL_DEPTH_COMPONENT, GL_FLOAT, 0);
  ...
  glFramebufferTexture2D(GL_DRAW_FRAMEBUFFER,
                         GL_DEPTH_ATTACHMENT,
                         GL_TEXTURE_2D, depth_map_, 0);
\end{lstlisting}
Postavke određenih parametara tekstura poput ponašanja prilikom uvećanja ili
smanjenja teksture kod prikaza su uklonjene kako bi prikaz koda bio
koncizniji. Teksture spremaju podatke u formatu brojeva s posmačnom točkom
veličine 32 bita. Kako podaci koji se zapisuju predstavljaju pozicije i
normale takav format nudi potrebnu preciznost uz veći trošak
memorije. Posljednji korak je postavljanje oznake da spremnik sadrži dva
mjesta za iscrtavanje.
\begin{lstlisting}
  GLenum draw_bufs[] = {GL_COLOR_ATTACHMENT0,
                        GL_COLOR_ATTACHMENT1};
  glDrawBuffers(2, draw_bufs);
\end{lstlisting}

Generiranje teksture kaustike prema pseudokodu \ref{alg:cm-gen-pseudo}
implementirano je kao program sjenčanja vrhova \engl{vertex shader}.
\lstset{language=glsl}
\begin{lstlisting}
void displaceRefractive()
{
  vec4 inLightPos = vec4(Position, 1.0f);
  vec4 inLightNormal = vec4(Normal, 0.0f);
  inLightNormal = normalize(inLightNormal);
  vec3 lightDir = normalize(Position);
  vec3 r = refract(lightDir, inLightNormal.xyz, eta);
  r = normalize(r);
  vec3 inters = estimateIntersection(inLightPos.xyz, r, 1);
  gl_Position = transform.perspective * vec4(inters, 1.0f);
  pos0 = inLightPos.xyz;
  normal0 = inLightNormal.xyz;
}
\end{lstlisting}
Varijable !Position! i !Normal! su pozicija i normala trenutnog vrha
geometrije kojeg obrađuje program te su one u koordinatnom sustavu
svjetla. Nakon njihove normalizacije poziva se funkcija !refract! koja vraća
refraktirani vektor. Ako želimo efekt kaustike dobiven refleksijom taj poziv
možemo zamijeniti funkcijom !reflect!. Također je moguće ostvariti i učinak
kaustike kod objekata koji su i reflektivni i refraktivni korištenjem obje
funkcije. Zatim se pomoću dobivenog vektora odredi mjesto presjecišta
funkcijom !estimateIntersection! te prenesu podaci u program za sjenčanje
fragmenata \engl{fragment shader} varijablama !pos0! i !normal0!.

Za procjenu presjecišta je korištena modificirana metoda Newton-Rhapson
\citep{cm2007}. Ilustracija algoritma je dana slikom \ref{fig:intersect}.
\begin{figure}[h]
  \centering
  \includegraphics[height=10cm]{img/intersect.png}
  \caption{Izračun presjecišta}
  \label{fig:intersect}
\end{figure}
Točka $v$ predstavlja trenutan vrh geometrije, $\vec{n}$ je normala a
$\vec{r}$ je vektor dobiven refrakcijom. Točke na refraktiranom vektoru su
dane sljedećom jednadžbom.
\begin{equation}
  \label{eq:refr-ray}
  p = v + d * \vec{r}
\end{equation}
$d$ je dužina od pozicije $v$ uzduž vektora $\vec{r}$. Vrijednost te dužine se
prvo postavi na $1$ i izračuna nova pozicija $p1$. Projekcijom $p1$ iz
perspektive svjetlosti na površinu geometrije dobiva se $p1'$. Zatim se
udaljenost $d'$ između projicirane točke $p1'$ i vrha $v$ koristi u prethodnoj
jednadžbi \ref{eq:refr-ray} kako bi se dobila točka $p2$. Ponovo se vrši
projekcija i dobiva se točka $p2'$ koja predstavlja procijenjeno
presjecište. Implementacija tog algoritma se nalazi u funkciji
!estimateIntersection!.
\begin{lstlisting}
vec3 estimateIntersection(vec3 v, vec3 r, int iters)
{
  vec3 p1 = v + r;
  vec2 tc = calcTC(p1);
  for (int i = 0; i < iters; ++i) {
    vec4 recPos = texture(positionMap, tc);
    vec3 p2 = v + distance(v, recPos.xyz) * r;
    tc = calcTC(p2);
  }
  return texture(positionMap, tc).rgb;
}
\end{lstlisting}
Točke na površini geometrije su zapisane u teksturi kojoj se pristupa
funkcijom !texture!. Ona prima referencu na teksturu putem varijable
!positionMap! i koordinate teksture !tc!. Kako bi se pravilno pristupilo toj
teksturi potrebno je izračunati koordinate dvodimenzionalne teksture iz
trodimenzionalnih koordinata pozicije točke. Taj izračun je implementiran
unutar funkcije !calcTC!.
\begin{lstlisting}[float]
vec2 calcTC(vec3 pos)
{
  vec4 texPt = transform.perspective * vec4(pos, 1.0f);
  vec2 tc = vec2(0.5 * (texPt.xy / texPt.w) + vec2(0.5, 0.5));
  tc.y = 1.0 - tc.y;
  return tc;
}
\end{lstlisting}
Pozicija geometrije !pos! se prvo mora projicirati perspektivnom
transformacijom te se skalira unutar intervala $[0, 1]$. Koordinatu $y$ je
potrebno okrenuti jer je njena orijentacija u teksturi obrnuta u odnosu na
orijentaciju iste osi geometrije. Dobivena sjecišta se proslijede programu za
sjenčanje fragmenata \engl{fragment shader} koji ih zapiše u teksturu te ona
predstavlja mapu kaustike. Kako zrake dobivene refleksijom ili refrakcijom na
različitim vrhovima mogu završiti u istim sjecištima potrebno je zapisati ih u
teksturu stapanjem alfa kanala transparencije \engl{alpha blending}. Time će
se i ostvariti efekt kaustike gdje će određena područja akumulirati više
svjetlosti u odnosu na druge. Rezultat akumulacije i konačna mapa kaustike
može se vidjeti na slici \ref{fig:caustic-map}. Potom se ta tekstura koristi u
završnom koraku kod iscrtavanja gdje za svaku točku geometrije se boja iz
teksture doda boji geometrije. Programski kod koji radi to uzorkovanje i
sjenčanje je sadržan sljedećim isječkom.
\begin{lstlisting}
vec4 calcCaustic(vec4 pos)
{
  vec3 proj = pos.xyz / pos.w;
  vec2 uv;
  uv.x = 0.5f * proj.x + 0.5f;
  uv.y = 0.5f * proj.y + 0.5f;
  vec4 caustic = texture(gCausticMap, uv).rgba;
  caustic = caustic * caustic * vec4(gLight.color, 1.0f);
  return vec4(caustic.rgb * 100, 1.0f);
}
\end{lstlisting}
Koordinate pozicije u trodimenzionalnom prostoru se projiciraju u prostor
teksture kao što je bio slučaj kod procjene sjecišta u funkciji
!calcTC!. Varijabla !gCausticMap! referencira teksturu koja predstavlja mapu
kaustike te se njena uzorkovana boja množi bojom izvora svjetlosti. U praksi
se pokazalo da je dobro kvadrirati uzorkovanu boju čime se stvara veća razlika
između dijelova na kojima se fokusiralo više svjetlosti i onih gdje je manje
svjetlosti. Time prikaz izgleda bolje te se rijetke pogrešne procjene sjecišta
i ne vide. Dobro je i drugačijim koeficijentima pomnožiti različite komponente
boje ako želimo prikazati objekt koji drugačije apsorbira valne duljine
svjetlosti. Takve manipulacije bojom kaustike su moguće i u koraku generiranja
mape kaustike. Rezultat prethodno opisane implementacije učinka se može
vidjeti na slici \ref{fig:initial-caustic}. Na slici je moguće primijetiti
aliasing kao i pogrešno izračunata presjecišta jer luk kaustike počinje
prerano pa je izvan prstena.
\begin{figure}[H]
  \centering
  \includegraphics[width=0.6\textwidth]{./img/caustic-map.png}
  \caption{Tekstura koja predstavlja mapiran učinak kaustike}
  \label{fig:caustic-map}
\end{figure}
\begin{figure}[H]
  \centering
  \includegraphics[width=0.6\textwidth]{./img/initial-caustic}
  \caption{Ostvareni učinak kaustike}
  \label{fig:initial-caustic}
\end{figure}

Kako bi se umanjila pojava aliasinga, teksturu kaustike su zamućene koristeći
Gaussov filtar \citep{wolff2011}. Implementacija filtra je napravljena unutar
programa za sjenčanje te se sastoji od dva prolaza. Prvi prolaz dan je kodom u
nastavku.
\begin{lstlisting}
  float dy = 1.0 / float(height);
  vec4 sum = weights[0] * texture(texture0, texCoord0);
  for (int i = 1; i < 5; ++i) {
    vec2 offset = dy * vec2(0.0, pixOffset[i]);
    sum += weights[i] * texture(texture0, texCoord0 + offset);
    sum += weights[i] * texture(texture0, texCoord0 - offset);
  }
  return sum;
\end{lstlisting}
Varijabla !weights! sadrži polje težina dobivenih izračunom Gaussove
distribucije \ref{eq:gauss} za svaku točku uzorkovanja (u ovom slučaju je to
deset točaka). Standardnu devijacija $\sigma$ je postavljena na $2$ a
očekivanje $\mu$ na $0$.
\begin{equation}
  \label{eq:gauss}
  f(x, \mu, \sigma) = \frac{1}{\sqrt{2\pi\sigma^2}}e^{-\frac{(x - \mu)^2}{2\sigma^2}}
\end{equation}
Drugi prolaz se razlikuje u tome što se uzorkuju elementi slike po $x$ osi.
Dobiveni rezultat se može vidjeti na slici \ref{fig:gauss-blur}.
\begin{figure}[h]
  \centering
  \includegraphics[width=13cm]{./img/gauss-blur.png}
  \caption{Lijeva slika je bez filtra, desna koristi Gaussov filtar}
  \label{fig:gauss-blur}
\end{figure}

Kompletan prikaz je dodatno unaprijeđen korištenjem metode mapiranja okoline
\engl{environment mapping} za ostvarenje refleksije i refrakcije
\citep{rtr3rd}. Prvi korak algoritma je pohrana 6 slika okoline u teksturu
oblika kocke \engl{cube map texture} prikazani funkcijom
!CubemapTexture::Load!.
\begin{lstlisting}[language=C++, float=ht]
bool CubemapTexture::Load()
{
  glGenTextures(1, &texture);
  glBindTexture(GL_TEXTURE_CUBE_MAP, texture);
  for (int i = 0; i < 6; ++i) {
    img::Image *image = img::Image::Load(files[i]);
    if (!image) {
      std::cerr << "Error: Unable to load image: "
                << files[i] << std::endl;
      return false;
    }
    glTexImage2D(types[i], 0, GL_RGBA,
                 image->GetWidth(), image->GetHeight(), 0,
                 GL_BGR, GL_UNSIGNED_BYTE, image->GetPixels());
    delete image;
  }
  ...
  return true;
}
\end{lstlisting}
Zatim se pri svakom iscrtavanju reflektivnog ili refraktivnog objekta izvrši
refleksija/refrakcija vektora svjetlosti unutar program za sjenčanje vrhova.
Tako dobiveni vektor određuje trodimenzionalne koordinate teksture (kod
teksture oblika kocke koriste se 3D koordinate a ne 2D). Program za sjenčanje
fragmenata koji računa dobivenu boju je dan sljedećim isječkom koda.
\begin{lstlisting}
  if (isReflective) {
    vec4 envColor = texture(gEnvironmentMap, reflectDir);
    color = mix(color, envColor, reflectFactor);
  }
\end{lstlisting}
Kako je ova tehnika dodana u Phongov model osvjetljenja potrebno je prvo
provjeriti da li je materijal reflektivan varijablom !isReflective!. Zatim se
uzorkuje mapa okoline te izmiješa boja korištenjem specifičnog faktora
reflektivnosti za materijal. Konačan rezultat se može vidjeti na slici
\ref{fig:env-map}.
\begin{figure}[h]
  \centering
  \includegraphics[width=12cm]{./img/env-map.png}
  \caption{Lijevo je prikaz refleksije a desno refrakcije}
  \label{fig:env-map}
\end{figure}

Tijekom implementacije pojavio se problem performansi. Iako je implementacija
rađena na prijenosnom računalu, napravljeni program je radio neočekivano
sporo. Nakon mnogo vremena uloženog u potragu onoga što toliko usporava rad te
isprobavanju mnogo alata za analizu, rješenje je pokazao alat Apitrace. Na
grafičkom prikazu analize se vidio gubitak vremena između rada procesora i
grafičke kartice. Problem je bio u tome što je dolazilo do sinkronizacije
između njih a razlog je bilo čitanje pozicija geometrije zapisane u teksturama
kako bi se mogle pretvoriti u format za generiranje kaustike. Taj problem je
riješen tako što je napravljena varijanta komunikacije s dvostrukim spremnikom
\engl{double buffering}. Ideja je da se asinkrono prenesu podaci na procesor,
dok se za potrebe generiranja kaustike šalju podaci generirani iz prethodnog
prijenosa. Na taj način prvih nekoliko sličica nema efekt kaustike a kasnije
taj efekt kasni za stvarnim stanjem scene. Kašnjenje je neprimjetno za ljudsko
oko a dobivena brzina izvršavanja je dvostruka u odnosu na početno. Skok s
početnih 30 sličica u sekundi na 60 je itekako vidljiv i pruža mnogo ugodniju
interaktivnost. Rezultati mjerenja alatom Apitrace su vidljivi na slici
\ref{fig:apitrace}. Na vrhu prikaza tog alata nalazi se grafički prikaz
vremena koje troše CPU (plavom bojom) i GPU (crvenom bojom). Za GPU je
sadržano više mjerenja za pojedine programe sjenčanja. Tako se na gornjoj
slici vidi velika praznina između kraja rada grafičke kartice i početka rada
procesora. Na donjoj slici se također može primijetiti kako metoda mapiranja
kaustike ne uzima puno resursa na grafičkom procesoru, a većina vremena otpada
na procesor i njegovo slanje geometrije grafičkoj kartici.
\begin{figure}[H]
  \centering
  \includegraphics[width=12cm]{./img/apitrace.png}
  \caption{Gornja slika prikazuje program prije optimizacije, donja poslije}
  \label{fig:apitrace}
\end{figure}
Sama izvedba u programskom kodu je relativno jednostavna te se nalazi unutar
metode !CausticMap::WriteCausticMap!.
\begin{lstlisting}[language=C++, float]
void CausticMap::WriteCausticMap()
{
  static int frame_delay = 2;
  static int frame = 0;
  static unsigned count = 0;
  // Wait couple of frames while the pixels are read.
  if (frame == 0) {
    readFrame(refr_frame_, width_, height_,
              vbo_[POS_DATA], vbo_[NORMAL_DATA]);
  }
  if (frame == frame_delay) {
    count = transfer(vbo_[POS_DATA], vbo_[NORMAL_DATA],
                     width_ * height_, vbo_[VERTEX_DATA]);
  }
  drawCausticMap(caustic_frame_, vao_, vbo_[VERTEX_DATA], count);
  frame = (frame + 1) % (frame_delay + 1);
}
\end{lstlisting}
Varijabla !frame_delay! određuje koliko će mapiranje kaustike kasniti za
stanjem scene ali toliko vremena više ostavljamo komunikaciji između grafičke
kartice i procesora računala. Funkcija !readFrame! asinkrono čita podatke te
do blokiranja dolazi samo ako ju se ponovo pozove prije no što je dovršila
prijenos. Funkcija !transfer! prilagodi pročitane podatke za generiranje mape
kaustike a funkcija !drawCausticMap! izvrši to generiranje s trenutno
dostupnim podacima. Ovisno o karakteristikama računala treba izmijeniti
!frame_delay! a još bolje rješenje bi bilo koristiti tri spremnika
\engl{triple buffering}.

Ovako implementiran program je uspoređen s drugom metodom u nastavku. Upute za
instalaciju i korištenje su sadržane u privitku ovog rada.

\section{Usporedba mapiranja kaustike i praćenja fotona}
Nvidia OptiX je programsko sučelje namijenjeno izradi programa koji koriste
algoritam praćenja zrake. Za te potrebe nudi funkcije za lako detektiranje
sudara, rada s zrakama i prijenos izračuna na grafički procesor. Ako je
grafički procesor također proizvela Nvidia te ovisno o modelu, OptiX u
pozadini koristi sučelje CUDA što nudi dodatno poboljšanje
performansi. Algoritam kojim je ostvaren efekt kaustike je praćenje fotona
koji odlično paše uz ovaj API jer ja zasnovan na algoritmu praćenja zrake.

Program čija je implementacija prethodno opisana i program koji koristi Nvidia
OptiX su uspoređeni na prijenosnom računalu sljedećih karakteristika.
\begin{itemize}
\item Intel(R) Core(TM) i3-4030U CPU @ 1.90GHz
\item Nvidia GeForce 820M
\item 4GB RAM
\end{itemize}
Kako navedena grafička kartica ima podršku za CUDA tehnologiju, mogu se
očekivati dobri rezultati OptiX izvedbe metode praćenja fotona.

Algoritam mapiranja kaustike se izvodi u 60 sličica u sekundi \engl{Frames Per
Second, FPS}, odnosno za prikaz jedne sličice potrebno je oko
16.6ms. Rezolucija tekstura u kojoj se pohranjuju pozicije geometrije i
rezultat izračuna efekta kaustike je 256x256 slikovnih elemenata
\engl{pixel}. Povećanjem rezolucije smanjuje se efekt aliasinga ali i pad
brzine izvođenja. Omjer performansi i rezolucije dan je tablicom
\ref{tab:cm-perf} i grafikonom \ref{fig:cm-perf}. Razlike u prikazu se mogu
vidjeti na slici \ref{fig:cm-error}.  Cijena velike teksture skače naglo kada
ona počne zauzimati toliko memorije na grafičkom procesoru da manipuliranje
njome postane vrlo sporo. Ono što povećanje rezolucije ne rješava je problem
preciznosti. Na dobivenoj slici \ref{fig:cm-error} se može vidjeti pogrešno
projicirana kaustika jer su presjecišta krivo procijenjena.
\begin{table}[H]
  \centering
  \begin{tabular}{|c|c|c|}
    \hline
    Rezolucija teksture (px) & FPS & prikaz jedne sličice (ms) \\ \hline
    256x256 & 60.00 & 16.67 \\ \hline
    512x512 & 57.00 & 17.54 \\ \hline
    1024x1024 & 21.71 & 46.05 \\ \hline
    2048x2048 & 5.27 & 189.76 \\ \hline
  \end{tabular}
\caption{Utjecaj rezolucije tekstura za mapiranje kaustike na performanse}
\label{tab:cm-perf}
\end{table}
\begin{figure}[H]
  \centering
  \includegraphics[width=10cm]{./img/cm-perf.png}
  \caption{Utjecaj rezolucije teksture za mapiranje kaustike na performanse}
  \label{fig:cm-perf}
\end{figure}

Kod algoritma praćenja fotona performanse su nešto slabije, ali iznenađujuće
ne puno. Za potrebe prikaza jedne sličice potrebno je oko 50ms što odgovara
izvođenju od 20 sličica u sekundi. Iako je brzina prikaza skoro 5 puta
sporija, početak interaktivnosti ove metode se nazire. Glavna prednost ove
metode je njena precizna i odlična kvaliteta generirane slike
\ref{fig:optix}. Smanjenje rezolucije uzrokuje pojavu aliasinga na kompletnoj
slici što ne pogoduje dobrom podešavanju i kompromisu omjera kvalitete i
performansi. Utjecaj raznih rezolucija na brzinu izvođenja dan je tablicom
\ref{tab:optix-perf} i grafikonom \ref{fig:optix-perf}.
\begin{table}[H]
  \centering
  \begin{tabular}{|c|c|c|}
    \hline
    Rezolucija slike (px) & FPS & prikaz jedne sličice (ms) \\ \hline
    256x256 & 35.00 & 28.57 \\ \hline
    512x512 & 27.16 & 36.81 \\ \hline
    1024x1024 & 18.48 & 54.11 \\ \hline
  \end{tabular}
\caption{Utjecaj rezolucije slike kod praćenja fotona na performanse}
\label{tab:optix-perf}
\end{table}
\begin{figure}[H]
  \centering
  \includegraphics[width=12cm]{./img/optix-perf.png}
  \caption{Utjecaj rezolucije slike na performanse kod praćenja fotona}
  \label{fig:optix-perf}
\end{figure}
\begin{figure}
  \centering
  \includegraphics[height=10cm]{./img/cm-error.png}
  \caption{Mapiranje kaustike pri različitim rezolucijama teksture. Gore
    lijevo -- 256x256; dolje lijevo -- 512x512; gore desno -- 1024x1024; dolje
    desno -- 2048x2048}
  \label{fig:cm-error}
\end{figure}
\begin{figure}
  \centering
  \includegraphics[width=15cm]{./img/optix.png}
  \caption{Rezultat dobiven praćenjem fotona i korištenjem tehnologija OptiX i
    CUDA. Lijeva slika -- 256x256; srednja -- 512x512; desna slika -- 1024x1024}
  \label{fig:optix}
\end{figure}

%%% Local Variables:
%%% mode: latex
%%% TeX-master: "../thesis"
%%% End:
