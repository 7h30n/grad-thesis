\chapter{Tehnike prikaza kaustike}
Kako je ovaj rad okrenut prikazu kaustike u stvarnom vremenu u nastavku su
opisani algoritmi koji zadovoljavaju te zahtjeve, uključujući jedan koji tek
polako ulazi u domenu interaktivnost. Svi algoritmi su temeljeni na prirodnim
zakonitostima ali donose različite kompromise između brzine izvođenja i
stupnja fizikalne simulacije. Posljednja opisana metoda sadrži i opis
implementacije u zasebnom poglavlju ovog rada.

\section{Prikaz kaustike zakrivljenim volumenima}
Ovaj algoritam prikaza kaustike je vrlo sličan algoritmu volumena sjena
\engl{shadow volumes} kojeg je opisao F.~C.~Crow \citep{crow77}. Ideja
algoritma polazi od projekcije volumena u obliku prizme iz vrhova poligona
na kojem dolazi do refrakcije i/ili refleksije \citep{watt90}. Svaka
strana prizme je planarna čime se zanemaruje činjenica da snop zraka
svjetlosti može stvoriti zakrivljenu površinu. Bolji rezultati se postižu kada
se projicirane površine interpoliraju \citep{ernst05}. Tako dobivene površine
prikazane slikom \ref{fig:warped} su zakrivljene \engl{warped volumes} te je
po njima algoritam dobio ime.
\begin{figure}[H]
  \centering
  \includegraphics[height=10cm]{img/warped.png}
  \caption{Volumen kaustike dobiven refrakcijom svjetlosti kroz trokut}
  \label{fig:warped}
\end{figure}

Prvi korak algoritma je podjela geometrija na generatore kaustike
\engl{generators} i geometriju koja prima efekt \engl{receivers}. Neki objekti
mogu spadati u obje grupe. Glavni dio algoritma je opisan sljedećim
pseudokodom.
\begin{algorithm}[H]
  \caption{Prikaz efekta kaustike volumenom kaustike}
  \begin{algorithmic}
    \FORALL{vidljivu točku T geometrije koja prima efekt kaustike}
    \FORALL{volumen kaustike V}
    \IF{T unutar V}
    \STATE Izračunaj intenzitet kaustike
    \ENDIF
    \ENDFOR
    \ENDFOR
  \end{algorithmic}
\end{algorithm}

Rezultati koje ovaj algoritam daje su vrlo dobri za interaktivan prikaz te se
metoda lako nadopuni dinamičkim sjenama koristeći već spomenuti algoritam
volumena sjena. Mana algoritma je u tome što generatori kaustike moraju biti
trokuti te geometrija na kojoj se prikazuje efekt mora se moći zapisati u
dubinsku mapu \engl{depth map} kako bi se moglo odrediti koje su točke unutar
volumena. Kako ova metoda daje brzo izvršavanje, prikaz kaustike nije precizan
u fizikalnom smislu te kvaliteta, iako dovoljno dobra, nije na
fotorealističnoj razini.

\section{Algoritam praćenja fotona}
U odnosu na prethodno opisani algoritam, sljedeći daje vjernije
rezultate. Osnova je algoritam praćenja zrake \engl{ray tracing} te ga to čini
računalno zahtjevnijim. Zanimljivo je to što na današnjim procesorima, te
pogotovo na grafičkom procesoru se mogu ostvariti vrlo dobri rezultati u vrlo
kratkom vremenu izvršavanja.

Učinak kaustike nastaje pomoću indirektne svjetlosti pa konvencionalno
praćenje zrake se mora malo izmijeniti. Umjesto praćenja zrake iz točke
očišta, zraka se počinje pratiti iz izvora svjetlosti. Taj algoritam je
nazvan praćenje zrake unazad \engl{backward ray tracing} te se izvodi u dva
koraka: mapiranje indirektne svjetlosti i uzorkovanje dobivene mape
\citep{arvo86}. Dodatno se taj algoritam može pojednostaviti generiranjem
dvije mape fotona umjesto kompletne indirektne svjetlosti
\citep{jensen96}. Najveći problem performansi kod tih algoritama je praćenje
zrake za koju se mora izračunati točka sjecišta s nekim poligonom
geometrije. Grafički procesori mogu napraviti puno izračuna u paraleli te
jedan od načina prilagodbe algoritma takvom izvršavanju jest praćenje zrake
fotona unutar prostora ekrana \citep{kruger06}. Točke sjecišta se sada mogu
lako odrediti pristupom dubinskoj mapi što uvelike ubrzava izvođenje. Na
sličan način je izvedena implementacija ovog algoritma pomoću programskog
sučelja Nvidia OptiX. Kasnije je ta implementaciju iskorištena kao primjer
usporedbe.

Uz gore navedene modifikacije te izvršavanjem na grafičkom procesoru algoritam
pokazuje vrlo dobre rezultate koji polako ali sigurno dolaze u domenu
interaktivnosti. Također, sama kvaliteta prikaza je na visokoj razini. Glavni
nedostatak koji je nastao prijenosom izračuna u prostor ekrana je nemogućnost
prikaza indirektne svjetlosti odnosno kaustike dobivene objektima koji su
izvan ekrana. To može narušiti uvjerljivost scene, no kako sam efekt nije
toliko prominentan ti problemi lako mogu proći nezapaženo.

\section{Mapiranje kaustike}
Nakon kratkog izleta u zahtjevniji algoritam, ovo potpoglavlje se vraća unutar
zahtjeva interaktivnog iscrtavanja. Ovaj algoritam je sličan metodi mapiranja
sjena \engl{shadow mapping} jer se iz perspektive svjetlosti računa tekstura
koja odgovara efektu kaustike dobivenog refleksijom i/ili refrakcijom
svjetlosnih zraka \citep{cm2007}. Takva tekstura, nazvana mapom kaustike
\engl{caustic map}, se potom projicira na predmete kako bi se na njima
prikazao vizualni učinak.

Algoritam se sastoji od sljedećih koraka.
\begin{algorithm}[H]
  \caption{Metoda mapiranja kaustike}
  \algsetup{linenodelimiter=.}
  \begin{algorithmic}[1]
    \STATE Pohrani poziciju geometrije koja prima efekt u teksturu
    \STATE Pohrani pozicije i normale reflektivne/refraktivne geometrije u
    teksturu
    \STATE Kreiraj teksturu kaustike
    \STATE Iscrtaj scenu s efektom kaustike
  \end{algorithmic}
  \label{alg:cm-pseudo}
\end{algorithm}
Pozicije i normale geometrije se pohranjuju u teksture u koordinatnom sustavu
svjetlosti te se u tom sustavu izvode izračuni na grafičkom procesoru. To
obilježje algoritma omogućuje lako spajanje s algoritmom mape sjena. Glavni
dio algoritma je generiranje teksture kaustike predstavljen sljedećim
pseudokodom.
\begin{algorithm}[H]
  \caption{Generiranje teksture kaustike}
  \begin{algorithmic}
    \FORALL{vrh V reflektivne/refraktivne geometrije}
    \STATE R = refrakcija(V.normala, smjerSvjetlosti)
    \STATE P = procjeniPresjecište(V.pozicija, R, geometrija)
    \STATE V.pozicija = P
    \ENDFOR
  \end{algorithmic}
  \label{alg:cm-gen-pseudo}
\end{algorithm}
Vektor dobiven refrakcijom se može zamijeniti reflektiranim ako želimo
ostvariti učinak kaustike uslijed refleksije. Ovako dobiveni vrhovi
predstavljaju mjesta gdje pojedina zraka svjetlosti dolazi na površinu
geometrije koja prima efekt kaustike. Kada ovakav izračun za više vrhova vrati
isti rezultat to predstavlja fokusiranje svjetlosnih zraka na određenu točku
uslijed čega i nastaje željeni efekt. Procjena točke presjecišta se može
odrediti iterativnom Newton-Rhapson metodom gdje u ovom slučaju algoritam daje
dobre rezultate već i nakon druge iteracije.

Algoritam mapiranja kaustike daje vrlo brze rezultate te se omjer performansi
i kvalitete lako može podešavati rezolucijom kojom generiramo teksturu efekta
kaustike. Manja rezolucija daje brz izračun ali dolazi do pojave aliasinga
koji se može umanjiti nekom od metoda zamućivanja slike. Dodatna ušteda
računalnih resursa se može postići i izgradnjom hijerarhijskih mapa kaustike
\citep{wyman08}. Kod te modifikacije postavi se hijerarhija tekstura kako bi
se lakše odbacili nepotrebni izračuni a točke koje završe u međusobnoj
blizini se zamjenjuju jednom većom točkom. Glavna mana ovog algoritma je
nepreciznost u određivanju točke presjecišta pa uz navedeni aliasing dolazi i
do pogrešnog prikazivanja koje je vidljivo i pri većim rezolucijama.
 
%%% Local Variables:
%%% mode: latex
%%% TeX-master: "../thesis"
%%% End:
