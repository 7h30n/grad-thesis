# Realization of Caustic Visual Effect in Real-time

This is my graduation thesis development repository.
It contains the written part and practical part.

## Thesis topic

The topic of the thesis is rendering caustic effects in real-time.

It is based on the following works:

  * _Caustics Mapping: An Image-space Technique for Real-time Caustics_
    by Shah et al.
  * _Hierarchical Caustic Maps_ by Wyman
  * _High-contrast Computational Caustic Design_ by Schwartzburg et al.
  * _Interactive Screen-Space Accurate Photon Tracing on GPUs_ by Krüger et al.
  * _Interactive Rendering of Caustics using Interpolated Warped Volumes_
    by Ernst et al.

## Repository structure

The repository consists of:

  * `caustics-app` - source code of the realized effect, written in C++ and GL
  * `thesis` - the written part of the thesis with included `.tex` files

## License

The software accompanying the master thesis is licensed under MIT license and
can be found in the `caustic-app` directory.

The master thesis itself as well as the presentation is licensed under Creative
Commons BY (Attribution) license and can be found inside `thesis` directory.

Copyright (c) 2015 Teon Banek
